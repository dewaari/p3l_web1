<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class Pegawai extends CI_Controller
	{
		public function __construct()
		{
			parent::__construct();

			$this->load->model(array('M_Pegawai', 'M_Cabang', 'M_Peran'));
		}

		public function index()
		{
			$data['show'] = $this->M_Pegawai->select_all(); 
			$data['side']='tampil/side'; 
			$data['content']='tampil_pegawai/tampil_pegawai1'; 
		 	$this->load->view('tampil/main',$data); 
		 }

		public function tampil_pegawai()
		{
			$data['side']='tampil/side';
			$data['tbl_cabang'] = $this->M_Cabang->get_all_cabang();
			$data['tbl_peran'] = $this->M_Peran->get_all_peran();
			$data['content']='tampil_pegawai/tambah_pegawai';
			$this->load->view('tampil/main',$data);
		}

		public function simpan_pegawai()
		{
			//$this->form_validation->set_rules('ID_PEGAWAI' , 'ID Pegawai', 'trim | required');
			$this->form_validation->set_rules('ID_CABANG' , 'ID Cabang', 'trim|required');
			$this->form_validation->set_rules('NAMA_PEGAWAI' , 'NAMA PEGAWAI', 'trim|required');
			$this->form_validation->set_rules('EMAIL_PEGAWAI' , 'Email Pegawai', 'trim|required');
			$this->form_validation->set_rules('PASSWORD' , 'Password', 'trim|required');
			$this->form_validation->set_rules('ALAMAT' , 'Alamat', 'trim|required');
			$this->form_validation->set_rules('STATUS_PEGAWAI' , 'Status Pegawai', 'trim|required');
			$this->form_validation->set_rules('JABATAN' , 'Jabatan', 'trim|required');
			$this->form_validation->set_rules('ID_PERAN' , 'ID Peran', 'trim|required');

			if ($this->form_validation->run())
			{
				$data = array(
						'ID_CABANG' => $this->input->post('ID_CABANG'),
						'NAMA_PEGAWAI' => $this->input->post('NAMA_PEGAWAI'),
						'EMAIL_PEGAWAI' => $this->input->post('EMAIL_PEGAWAI'),
						'PASSWORD' => sha1($this->input->post('PASSWORD')),
						'ALAMAT' => $this->input->post('ALAMAT'),
						'STATUS_PEGAWAI' => $this->input->post('STATUS_PEGAWAI'),
						'JABATAN' => $this->input->post('JABATAN'),
						'ID_PERAN' => $this->input->post('ID_PERAN')
				);

				$result = $this->M_Pegawai->tambah_pegawai($data);

				if ($result < 0) {
					$out['status'] = true;
					$out['msg'] ='Data Pegawai Berhasil ditambahkan';
				// jika salah
				} else {
					$out['status'] = false;
					$out['msg'] = 'Data Pegawai Gagal ditambahkan';
				}
			}
			else
			{
				$out['status'] = false;
				$out['msg'] = show_err_msg(validation_errors());
			}

			$this->session->set_flashdata('pesan', $out['msg']);
			if($out['status'])
				redirect("Pegawai/tampil_pegawai");
			else
				redirect("Pegawai");
		}

		public function delete($ID_PEGAWAI)
		{
			$result = $this->M_Pegawai->delete($ID_PEGAWAI);
			redirect('Pegawai');
			if ($result > 0) {
				echo show_succ_msg('Data cabang Berhasil dihapus', '20px');
			} else {
				echo show_err_msg('Data Cabang Gagal dihapus', '20px');
			}
		}

		public function edit_pegawai($ID_PEGAWAI)
		{

			$this->form_validation->set_rules('ID_CABANG' , 'ID Cabang', 'trim|required');
			$this->form_validation->set_rules('NAMA_PEGAWAI' , 'NAMA PEGAWAI', 'trim|required');
			$this->form_validation->set_rules('EMAIL_PEGAWAI' , 'Email Pegawai', 'trim|required');
			$this->form_validation->set_rules('PASSWORD' , 'Password', 'trim|required');
			$this->form_validation->set_rules('ALAMAT' , 'Alamat', 'trim|required');
			$this->form_validation->set_rules('STATUS_PEGAWAI' , 'Status Pegawai', 'trim|required');
			$this->form_validation->set_rules('JABATAN' , 'Jabatan', 'trim|required');
			$this->form_validation->set_rules('ID_PERAN' , 'ID Peran', 'trim|required');


			$data['show'] = $this->M_Pegawai->get_pegawai($ID_PEGAWAI);
			$data['side']='tampil/side';
			$data['tbl_cabang'] = $this->M_Cabang->get_all_cabang();
			$data['tbl_peran'] = $this->M_Peran->get_all_peran();
			$data['content']='tampil_pegawai/edit_pegawai';
			$this->load->view('tampil/main',$data);
		}

		public function update_data_pegawai()
		{
			$this->form_validation->set_rules('ID_CABANG' , 'ID Cabang', 'trim|required');
			$this->form_validation->set_rules('NAMA_PEGAWAI' , 'NAMA PEGAWAI', 'trim|required');
			$this->form_validation->set_rules('EMAIL_PEGAWAI' , 'Email Pegawai', 'trim|required');
			$this->form_validation->set_rules('PASSWORD' , 'Password', 'trim|required');
			$this->form_validation->set_rules('ALAMAT' , 'Alamat', 'trim|required');
			$this->form_validation->set_rules('STATUS_PEGAWAI' , 'Status Pegawai', 'trim|required');
			$this->form_validation->set_rules('JABATAN' , 'Jabatan', 'trim|required');
			$this->form_validation->set_rules('ID_PERAN' , 'ID Peran', 'trim|required');

			$data 	= $this->input->post();
			if ($this->form_validation->run())
			{
				$data = array(
						'ID_PEGAWAI' => $this->input->post('ID_PEGAWAI'),
						'ID_CABANG' => $this->input->post('ID_CABANG'),
						'NAMA_PEGAWAI' => $this->input->post('NAMA_PEGAWAI'),
						'EMAIL_PEGAWAI' => $this->input->post('EMAIL_PEGAWAI'),
						'PASSWORD' => sha1($this->input->post('PASSWORD')),
						'ALAMAT' => $this->input->post('ALAMAT'),
						'STATUS_PEGAWAI' => $this->input->post('STATUS_PEGAWAI'),
						'JABATAN' => $this->input->post('JABATAN'),
						'ID_PERAN' => $this->input->post('ID_PERAN')
				);

				$result = $this->M_Pegawai->update_pegawai($data);

				if ($result < 0) {
					$out['status'] = true;
					$out['msg'] ='Data Pegawai Berhasil ditambahkan';
				} else {
					$out['status'] = false;
					$out['msg'] = 'Data Pegawai Gagal ditambahkan';
				}
			}
			else
			{
				$out['status'] = false;
				$out['msg'] = show_err_msg(validation_errors());
			}

			$this->session->set_flashdata('pesan', $out['msg']);

			if($out['status'])
				redirect("Pegawai/tampil_pegawai");
			else
				redirect("Pegawai");
		}

	}

?>