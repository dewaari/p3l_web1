<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* 
*/
	class Jenis_Kamar extends CI_Controller
	{
		
		public function __construct() {
			parent::__construct();
			$this->load->model('M_Jenis_Kamar');
			
		}

		public function index() 
		{

			$data['show'] = $this->M_Jenis_Kamar->select_all();
			$data['side']='tampil/side';
			$data['content']='tampil_jenis_kamar/tampil_Jenis_Kamar';
		 	$this->load->view('tampil/main',$data);
		}

		public function tambah()
		{
			$this->form_validation->set_rules('JENIS_KAMAR', 'Jenis Kamar', 'trim|required');
			$this->form_validation->set_rules('KAPASITAS', 'Kapasitas', 'trim|required');
			$this->form_validation->set_rules('DESKRIPSI_KAMAR', 'Deskripsi Kamar', 'trim|required');
			$this->form_validation->set_rules('STATUS_KAMAR', 'Status Kamar', 'trim|required');
			$this->form_validation->set_rules('HARGA', 'Harga', 'trim|required');

			$data 	= $this->input->post();
			$data['side']='tampil/side';
		 	$data['content']='tampil_jenis_kamar/tambah_jenis_kamar';
		 	$this->load->view('tampil/main',$data);
			if ($this->form_validation->run() == TRUE) {
				$result = $this->M_Jenis_Kamar->tambah_jenis_kamar($data);
				redirect('Jenis_Kamar');

				if ($result) {
					$out['status'] = '';
					$out['msg'] = show_succ_msg('Data Fasilitas Berhasil ditambahkan', '20px');
				} else {
					$out['status'] = '';
					$out['msg'] = show_err_msg('Data Fasilitas Gagal ditambahkan', '20px');
				}
			} else {
				$out['status'] = 'form';
				$out['msg'] = show_err_msg(validation_errors());
			}
		}

		public function delete($ID_JENIS_KAMAR)
		{
			$result = $this->M_Jenis_Kamar->delete($ID_JENIS_KAMAR);
			redirect('Jenis_Kamar');
			if ($result > 0) {
				echo show_succ_msg('Data cabang Berhasil dihapus', '20px');
			} else {
				echo show_err_msg('Data Cabang Gagal dihapus', '20px');
			}
		}

		public function edit($ID_JENIS_KAMAR)
		{
			$this->form_validation->set_rules('JENIS_KAMAR', 'Jenis Kamar', 'trim|required');
			$this->form_validation->set_rules('KAPASITAS', 'Kapasitas', 'trim|required');
			$this->form_validation->set_rules('DESKRIPSI_KAMAR', 'Deskripsi Kamar', 'trim|required');
			$this->form_validation->set_rules('STATUS_KAMAR', 'Status Kamar', 'trim|required');
			$this->form_validation->set_rules('HARGA', 'Harga', 'trim|required');

			$data['show'] = $this->M_Jenis_Kamar->get_jenis_kamar($ID_JENIS_KAMAR);
			$data['side']='tampil/side';
		 	$data['content']='tampil_jenis_kamar/edit_jenis_kamar';
		 	$this->load->view('tampil/main',$data);
		}

		public function update()
		{
			$this->form_validation->set_rules('JENIS_KAMAR', 'Jenis Kamar', 'trim|required');
			$this->form_validation->set_rules('KAPASITAS', 'Kapasitas', 'trim|required');
			$this->form_validation->set_rules('DESKRIPSI_KAMAR', 'Deskripsi Kamar', 'trim|required');
			$this->form_validation->set_rules('STATUS_KAMAR', 'Status Kamar', 'trim|required');
			$this->form_validation->set_rules('HARGA', 'Harga', 'trim|required');

			$data 	= $this->input->post();
			if ($this->form_validation->run() == TRUE) {
				$result = $this->M_Jenis_Kamar->update($data);

				redirect('Jenis_Kamar');
				if ($result > 0) {
					$out['status'] = '';
					$out['msg'] = show_succ_msg('Data Jenis Kamar Berhasil ditambahkan', '20px');
				} else {
					$out['status'] = '';
					$out['msg'] = show_err_msg('Data Jenis Kamar Gagal ditambahkan', '20px');
				}
			} else {
				$out['status'] = 'form';
				$out['msg'] = show_err_msg(validation_errors());
			}
		}
	}
?>