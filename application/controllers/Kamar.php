<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	/**
	* 
	*/
	class Kamar extends CI_Controller
	{
		
		public function __construct()
		{
			parent::__construct();
			$this->load->model(array('M_Kamar','M_Cabang','M_Jenis_Kamar', 'M_Tempat_Tidur'));
		}

		public function index() 
		{

			$data['show'] = $this->M_Kamar->select_all();
			$data['side']='tampil/side';
			$data['content']='tampil_kamar/tampil_kamar1';
			$this->load->view('tampil/main',$data);
		}

		public function tampil_kamar()
		{
			$data['side']='tampil/side';
			$data['tbl_cabang'] = $this->M_Cabang->get_all_cabang();
			$data['tbl_jenis_kamar'] = $this->M_Jenis_Kamar->get_all_jenis_kamar();
			$data['tbl_tempat_tidur'] = $this->M_Tempat_Tidur->get_all_tempat_tidur();
			$data['content']='tampil_kamar/tambah_kamar';
			$this->load->view('tampil/main',$data);
		}

		public function tambah_kamar()
		{
			// $this->form_validation->set_rules('KODE_KAMAR', 'Kode Kamar', 'trim|required');
			$this->form_validation->set_rules('ID_CABANG', 'ID Cabang', 'trim|required');
			$this->form_validation->set_rules('ID_TEMPAT_TIDUR', 'ID Tempat Tidur', 'trim|required');
			$this->form_validation->set_rules('ID_JENIS_KAMAR', 'ID Jenis Kamar', 'trim|required');
			$this->form_validation->set_rules('LANTAI_KAMAR', 'Lantai Kamar', 'trim|required');
			$this->form_validation->set_rules('NOMOR_KAMAR', 'Nomor Kamar', 'trim|required');
			$this->form_validation->set_rules('STATUS_KAMAR', 'Status Kamar', 'trim|required');
			$this->form_validation->set_rules('DESKRIPSI_KAMAR', 'Deskripsi Kamar', 'trim|required');
			$this->form_validation->set_rules('BEBAS_ROKO', 'Bebas Roko', 'trim|required');

			if ($this->form_validation->run()) 
			{

				$data = array(
					'KODE_KAMAR' => '',
					'ID_CABANG' => $this->input->post('ID_CABANG'),
					'ID_TEMPAT_TIDUR' => $this->input->post('ID_TEMPAT_TIDUR'),
					'ID_JENIS_KAMAR' => $this->input->post('ID_JENIS_KAMAR'),
					'LANTAI_KAMAR' => $this->input->post('LANTAI_KAMAR'),
					'NOMOR_KAMAR' => $this->input->post('NOMOR_KAMAR'),
					'STATUS_KAMAR' => $this->input->post('STATUS_KAMAR'),
					'DESKRIPSI_KAMAR' => $this->input->post('DESKRIPSI_KAMAR'),
					'BEBAS_ROKO' => $this->input->post('BEBAS_ROKO'),
				);

				$Nama_kamar = $this->M_Jenis_Kamar->get_jenis_kamar($data['ID_JENIS_KAMAR']);

				$words = explode(" ", $Nama_kamar[0]->JENIS_KAMAR);

				foreach ($words as $word) {
					$data['KODE_KAMAR'] .= $word[0];
				}

				$data['KODE_KAMAR'] .= '0'.$data['LANTAI_KAMAR'];

				if ($data['NOMOR_KAMAR']/10 == 0) {
					$data['KODE_KAMAR'] .= '00'.$data['NOMOR_KAMAR'];

				}elseif ($data['NOMOR_KAMAR']/100 == 0) {
					$data['KODE_KAMAR'] .= '0'.$data['NOMOR_KAMAR'];
				}else{
					$data['KODE_KAMAR'] .= $data['NOMOR_KAMAR'];
				}

				$result = $this->M_Kamar->tambah_kamar($data);

				
				if ($result < 0) {
					$out['status'] = true;
					$out['msg'] ='Data Pegawai Berhasil ditambahkan';
				} else {
					$out['status'] = false;
					$out['msg'] = 'Data Pegawai Gagal ditambahkan';
				}
			}
			else
			{
				$out['status'] = false;
				$out['msg'] = show_err_msg(validation_errors());
			}

			$this->session->set_flashdata('pesan', $out['msg']);
			if($out['status'])
				redirect("Kamar/tampil_kamar1");
			else
				redirect("Kamar");
		}

		public function delete_kamar($ID_KAMAR)
		{
			$result = $this->M_Kamar->delete($ID_KAMAR);
			redirect('Kamar');
			if ($result > 0) {
				echo show_succ_msg('Data cabang Berhasil dihapus', '20px');
			} else {
				echo show_err_msg('Data Cabang Gagal dihapus', '20px');
			}
		}

		public function edit_kamar($ID_KAMAR)
		{
			$this->form_validation->set_rules('KODE_KAMAR', 'Kode Kamar', 'trim|required');
			$this->form_validation->set_rules('ID_CABANG', 'ID Cabang', 'trim|required');
			$this->form_validation->set_rules('ID_TEMPAT_TIDUR', 'ID Tempat Tidur', 'trim|required');
			$this->form_validation->set_rules('ID_JENIS_KAMAR', 'ID Jenis Kamar', 'trim|required');
			$this->form_validation->set_rules('LANTAI_KAMAR', 'Lantai Kamar', 'trim|required');
			$this->form_validation->set_rules('NOMOR_KAMAR', 'Nomor Kamar', 'trim|required');
			$this->form_validation->set_rules('STATUS_KAMAR', 'Status Kamar', 'trim|required');
			$this->form_validation->set_rules('DESKRIPSI_KAMAR', 'Deskripsi Kamar', 'trim|required');
			$this->form_validation->set_rules('BEBAS_ROKO', 'Bebas Roko', 'trim|required');

			//$data 	= $this->input->post();
			$data['show'] = $this->M_Kamar->get_kamar($ID_KAMAR);
			$data['side']='tampil/side';
			$data['tbl_cabang'] = $this->M_Cabang->get_all_cabang();
			$data['tbl_jenis_kamar'] = $this->M_Jenis_Kamar->get_all_jenis_kamar();
			$data['tbl_tempat_tidur'] = $this->M_Tempat_Tidur->get_all_tempat_tidur();
			$data['content']='tampil_kamar/edit_kamar';
			$this->load->view('tampil/main',$data);
		}

		public function update_kamar()
		{
			// $this->form_validation->set_rules('KODE_KAMAR', 'Kode Kamar', 'trim|required');
			$this->form_validation->set_rules('ID_CABANG', 'ID Cabang', 'trim|required');
			$this->form_validation->set_rules('ID_TEMPAT_TIDUR', 'ID Tempat Tidur', 'trim|required');
			$this->form_validation->set_rules('ID_JENIS_KAMAR', 'ID Jenis Kamar', 'trim|required');
			$this->form_validation->set_rules('LANTAI_KAMAR', 'Lantai Kamar', 'trim|required');
			$this->form_validation->set_rules('NOMOR_KAMAR', 'Nomor Kamar', 'trim|required');
			$this->form_validation->set_rules('STATUS_KAMAR', 'Status Kamar', 'trim|required');
			$this->form_validation->set_rules('DESKRIPSI_KAMAR', 'Deskripsi Kamar', 'trim|required');
			$this->form_validation->set_rules('BEBAS_ROKO', 'Bebas Roko', 'trim|required');


			$data 	= $this->input->post();
			if ($this->form_validation->run() == TRUE) {

				$Nama_kamar = $this->M_Jenis_Kamar->get_jenis_kamar($data['ID_JENIS_KAMAR']);

				$words = explode(" ", $Nama_kamar[0]->JENIS_KAMAR);

				foreach ($words as $word) {
					$data['KODE_KAMAR'] .= $word[0];
				}

				$data['KODE_KAMAR'] .= '0'.$data['LANTAI_KAMAR'];

				if ($data['NOMOR_KAMAR']/10 == 0) {
					$data['KODE_KAMAR'] .= '00'.$data['NOMOR_KAMAR'];

				}elseif ($data['NOMOR_KAMAR']/100 == 0) {
					$data['KODE_KAMAR'] .= '0'.$data['NOMOR_KAMAR'];
				}else{
					$data['KODE_KAMAR'] .= $data['NOMOR_KAMAR'];
				}
				
				$result = $this->M_Kamar->update($data);
				redirect('Kamar');
				if ($result) {
					$out['status'] = '';
					$out['msg'] = show_succ_msg('Data Kamar Berhasil ditambahkan', '20px');
				} else {
					$out['status'] = '';
					$out['msg'] = show_err_msg('Data Kamar Gagal ditambahkan', '20px');
				}
			} else {
				$out['status'] = 'form';
				$out['msg'] = show_err_msg(validation_errors());
			}
		}
	}

	?>