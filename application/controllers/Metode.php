<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class Metode extends CI_Controller
	{
		public function __construct()
		{
			parent::__construct();

			$this->load->model('M_Metode');//load session setiap form di buka
			//$this->load->model(array('M_Kamar','M_Cabang','M_Jenis_Kamar', 'M_Tempat_Tidur'));
		}

		public function index()
		{
			$data['show'] = $this->M_Metode->select_all(); //ngambil data dari function select_all simpan ke variabel $data['show']
			$data['side']='tampil/side'; //tampil/side simpan ke variabel $data['side']
			$data['content']='tampil_metode/tampil_metode1'; //tampil_sesion/tampil_sesion1 simpan ke variabel $data['content']

		 	$this->load->view('tampil/main',$data); //tampilkan view tampil/main lewatkan variabel $data;
		}

		// public function tambah_metode()
		// {
		// 	if(!empty($_FILES['BUKTI_PEMBAYARAN']['name']))
		// 	{
		// 		$config['upload_path'] = './assets/images/';
		// 		$config['allowed_types'] = 'gif|jpg|png';
		// 		$config['max_size']	= '10000'; //in kb
		// 		$config['max_width']  = '1024';
		// 		$config['max_height']  = '768';
		// 		$config['file_name'] = $_FILES['BUKTI_PEMBAYARAN']['name'];

		// 		$this->upload->initialize($config);

		// 		if($this->upload->do_upload('BUKTI_PEMBAYARAN')){
  //                   $uploadData = $this->upload->data();
  //                   $BUKTI_PEMBAYARAN = $uploadData['file_name'];
  //               }else{
  //                   $BUKTI_PEMBAYARAN = '';
  //               }
		// 	} else {
		// 		$BUKTI_PEMBAYARAN = '';
		// 	}

		// 	$data = array(

		// 		'NAMA_PEMILIK' => $this->input->post('NAMA_PEMILIK'),
		// 		'NO_KARTU' => $this->input->post('NO_KARTU'),
		// 		'NAMA_BANK' => $this->input->post('NAMA_BANK'),
		// 		'CVV' => $this->input->post('CVV'),
		// 		'BUKTI_PEMBAYARAN' => $BUKTI_PEMBAYARAN

		// 	);

		// 	$result = $this->M_Metode->insert_metode($data);


		// 	if($result){
  //               $this->session->set_flashdata('success_msg', 'User data have been added successfully.');
  //           }else{
  //               $this->session->set_flashdata('error_msg', 'Some problems occured, please try again.');
  //           }
  //           redirect("Metode/tampil_metode1");
			
		// }


		public function tambah_metode()
		{
			$this->form_validation->set_rules('NAMA_PEMILIK' , 'Nama Pemilik', 'trim|required');
			$this->form_validation->set_rules('NO_KARTU' , 'No Kartu', 'trim|required');
			$this->form_validation->set_rules('NAMA_BANK' , 'Nama Bank', 'trim|required');
			$this->form_validation->set_rules('CVV' , 'CVV', 'trim|required');
			//$this->form_validation->set_rules('BUKTI_PEMBAYARAN' , 'Bukti Pembayaran', 'trim|required');

			$nama = 'file_'.time();
			$config['upload_path'] = './assets/images/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']	= '10000'; //in kb
			$config['max_width']  = '1024';
			$config['max_height']  = '768';
			$config['file_name']	= $nama;

			$this->upload->initialize($config);
			if ($this->form_validation->run() == TRUE && $this->upload->do_upload('foto'))
			{
				$gambar = $this->upload->data();
				$data = array(
						'NAMA_PEMILIK'	=> $this->input->post('NAMA_PEMILIK',TRUE),
						'NO_KARTU'		=> $this->input->post('NO_KARTU',TRUE),
						'NAMA_BANK'		=> $this->input->post('NAMA_BANK',TRUE),
						'CVV'	=> $this->input->post('CVV',TRUE),
						'BUKTI_PEMBAYARAN'		=> $gambar['file_name']

				);
				$this->M_Metode->insert_metode($data);
				redirect("Metode");

			}
			else
			{
				
				$data['foto'] = '';

				if (! $this->upload->do_upload('foto')) 
				{
					$data['foto'] = $this->upload->display_errors();
				}
				$data['side']='tampil/side'; //tampil/side simpan ke variabel $data['side']
				$data['content']='tampil_metode/tambah_metode'; //tampil_sesion/tampil_sesion1 simpan ke variabel $data['content']

		 		$this->load->view('tampil/main',$data);
			}
		}

		public function hapus_metode($ID_METODE)
		{
			$result = $this->M_Metode->delete_metode($ID_METODE);
			redirect('Metode');
			if ($result > 0) {
				echo show_succ_msg('Data cabang Berhasil dihapus', '20px');
			} else {
				echo show_err_msg('Data Cabang Gagal dihapus', '20px');
			}
		}

		public function edit_metode($ID_METODE)
		{

			$this->form_validation->set_rules('NAMA_PEMILIK' , 'Nama Pemilik', 'trim|required');
			$this->form_validation->set_rules('NO_KARTU' , 'No Kartu', 'trim|required');
			$this->form_validation->set_rules('NAMA_BANK' , 'Nama Bank', 'trim|required');
			$this->form_validation->set_rules('CVV' , 'CVV', 'trim|required');


			$data['metode'] = $this->M_Metode->get_metode($ID_METODE); //ngambil data dari function select_all simpan ke variabel $data['show']
			$data['side']='tampil/side'; //tampil/side simpan ke variabel $data['side']
			$data['content']='tampil_metode/edit_metode'; //tampil_sesion/tampil_sesion1 simpan ke variabel $data['content']
		 	$this->load->view('tampil/main',$data);
		}

		public function update_data_metode()
		{
			$this->form_validation->set_rules('NAMA_PEMILIK' , 'Nama Pemilik', 'trim|required');
			$this->form_validation->set_rules('NO_KARTU' , 'No Kartu', 'trim|required');
			$this->form_validation->set_rules('NAMA_BANK' , 'Nama Bank', 'trim|required');
			$this->form_validation->set_rules('CVV' , 'CVV', 'trim|required');
			//$this->form_validation->set_rules('BUKTI_PEMBAYARAN' , 'Bukti Pembayaran', 'trim|required');

			$nama = 'file_'.time();
			$config['upload_path'] = './assets/images/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']	= '10000'; //in kb
			$config['max_width']  = '1024';
			$config['max_height']  = '768';
			$config['file_name']	= $nama;

			$this->upload->initialize($config);
			if ($this->form_validation->run() == TRUE && $this->upload->do_upload('foto'))
			{
				$gambar = $this->upload->data();
				$data = array(
						'NAMA_PEMILIK'	=> $this->input->post('NAMA_PEMILIK',TRUE),
						'NO_KARTU'		=> $this->input->post('NO_KARTU',TRUE),
						'NAMA_BANK'		=> $this->input->post('NAMA_BANK',TRUE),
						'CVV'	=> $this->input->post('CVV',TRUE),
						'BUKTI_PEMBAYARAN'		=> $gambar['file_name']

				);
				$this->M_Metode->update_metode($data);
				redirect("Metode");

			}
			else
			{
				
				$data['foto'] = '';

				if (! $this->upload->do_upload('foto')) 
				{
					$data['foto'] = $this->upload->display_errors();
				}
				//$data['show'] = $this->M_Metode->get_metode($ID_METODE);
				$data['side']='tampil/side'; //tampil/side simpan ke variabel $data['side']
				$data['content']='tampil_metode/edit_metode'; //tampil_sesion/tampil_sesion1 simpan ke variabel $data['content']

		 		// $this->load->view('tampil/main',$data);
			}
		}
	}
?>