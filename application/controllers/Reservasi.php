<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	/**
	* 
	*/
	class Reservasi extends CI_Controller
	{
		
		public function __construct()
		{
			parent::__construct();
			$this->load->model(array('M_Reservasi','M_Cabang','M_Pegawai', 'M_Pelanggan','M_Detail', 'M_Kamar', 'M_Jenis_Kamar'));
		}

		public function index() 
		{

			$data['show'] = $this->M_Reservasi->select_all_reservasi();
			//$data['detail_reservasi_data'] = $this->M_Reservasi->detail_reservasi($ID_RESERVASI)->result();
			$data['side']='tampil/side';
			$data['content']='tampil_reservasi/tampil_reservasi1';
			$this->load->view('tampil/main',$data);
		}

		public function tampil_reservasi()
		{
			$data['side']='tampil/side';
			$data['tbl_pegawai'] = $this->M_Pegawai->get_all_pegawai();
			$data['tbl_pelanggan'] = $this->M_Pelanggan->get_all_pelanggan();
			$data['tbl_cabang'] = $this->M_Cabang->get_all_cabang();
			$data['detail_reservasi'] = $this->M_Detail->get_all_detail();
			$data['tbl_kamar'] = $this->M_Kamar->get_all_Kamar();
			$data['tbl_jenis_kamar'] = $this->M_Jenis_Kamar->get_all_jenis_kamar();
			$data['ID_BOOKING'] = 'P'.date("d").date("m").date("y");
			$data['content']='tampil_reservasi/tambah_reservasi';
			$this->load->view('tampil/main',$data);
		}

		function dateDifference($date_1 , $date_2 , $differenceFormat = '%a' )
		{
			$datetime1 = date_create($date_1);
			$datetime2 = date_create($date_2);

			$interval = date_diff($datetime1, $datetime2);

			return $interval->format($differenceFormat);

		}

		public function simpan_reservasi()
		{
			$this->form_validation->set_rules('ID_BOOKING' , 'ID Booking', 'trim|required');
			//$this->form_validation->set_rules('ID_PEGAWAI' , 'ID Pegawai', 'trim|required');
			$this->form_validation->set_rules('ID_PELANGGAN' , 'ID Pelanggan', 'trim|required');
			$this->form_validation->set_rules('ID_CABANG' , 'ID Cabang', 'trim|required');
			$this->form_validation->set_rules('ID_JENIS_KAMAR' , 'ID Jenis Kamar', 'trim|required');
			$this->form_validation->set_rules('ID_KAMAR' , 'ID Kamar', 'trim|required');
			$this->form_validation->set_rules('TANGGAL_RESERVASI' , 'Tanggal Reservasi', 'trim|required');
			$this->form_validation->set_rules('JUMLAH_DEWASA' , 'Jumlah Dewasa', 'trim|required');
			$this->form_validation->set_rules('JUMLAH_ANAK' , 'Jumlah Anak', 'trim|required');
			$this->form_validation->set_rules('TANGGAL_MASUK' , 'Tanggal Masuk', 'trim|required');
			$this->form_validation->set_rules('TANGGAL_SELESAI' , 'Tanggal Selesai', 'trim|required');
			// $this->form_validation->set_rules('TANGGAL_BAYAR' , 'Tanggal Bayar', 'trim|required');
			// $this->form_validation->set_rules('TOTAL_BIAYA' , 'Total Biaya', 'trim|required');
			$this->form_validation->set_rules('STATUS_RESERVASI' , 'Status Reservasi', 'trim|required');
			//$this->form_validation->set_rules('NAMA_INSTITUSI' , 'Nama Institusi', 'trim|required');
			// $this->form_validation->set_rules('DEPOSIT' , 'Deposit', 'trim|required');
			// $this->form_validation->set_rules('UANG_JAMINAN' , 'Uang Jaminan', 'trim|required');
			$this->form_validation->set_rules('JENIS_TAMU' , 'Jenis Tamu', 'trim|required');


			if ($this->form_validation->run())
			{
				$data = array(
					'ID_BOOKING' => $this->input->post('ID_BOOKING'),
					//'ID_PEGAWAI' => $this->input->post('ID_PEGAWAI'),
					'ID_PELANGGAN' => $this->input->post('ID_PELANGGAN'),
					'ID_CABANG' => $this->input->post('ID_CABANG'),
					'ID_JENIS_KAMAR' => $this->input->post('ID_JENIS_KAMAR'),
					'ID_KAMAR' => $this->input->post('ID_KAMAR'),
					'TANGGAL_RESERVASI' => $this->input->post('TANGGAL_RESERVASI'),
					'JUMLAH_DEWASA' => $this->input->post('JUMLAH_DEWASA'),
					'JUMLAH_ANAK' => $this->input->post('JUMLAH_ANAK'),
					'TANGGAL_MASUK' => $this->input->post('TANGGAL_MASUK'),
					'TANGGAL_SELESAI' => $this->input->post('TANGGAL_SELESAI'),
					// 'TANGGAL_BAYAR' => $this->input->post('TANGGAL_BAYAR'),
					'TOTAL_BIAYA' => 0,
					'STATUS_RESERVASI' => $this->input->post('STATUS_RESERVASI'),
					//'NAMA_INSTITUSI' => $this->input->post('NAMA_INSTITUSI'),
					// 'DEPOSIT' => $this->input->post('DEPOSIT'),
					'UANG_JAMINAN' => 0,
					'JENIS_TAMU' => $this->input->post('JENIS_TAMU'),
				);

				$hargaKamar = $this->M_Jenis_Kamar->get_jenis_kamar($data['ID_JENIS_KAMAR']);
				$hargaKamar = $hargaKamar[0]->HARGA;

				$data['TOTAL_BIAYA'] = $this->dateDifference($data['TANGGAL_MASUK'], $data['TANGGAL_SELESAI'], '%a') * $hargaKamar;
				$data['UANG_JAMINAN'] = $data['TOTAL_BIAYA'];



				$data['ID_RESERVASI'] = $this->M_Reservasi->tambah_reservasi($data);
				$result = $this->M_Reservasi->tambah_detail($data);
				//$result = $this->M_Reservasi->tambah_jenis_kamar($data);

				if ($result < 0) {
					$out['status'] = true;
					$out['msg'] ='Data Reservasi Berhasil ditambahkan';
				} else {
					$out['status'] = false;
					$out['msg'] = 'Data Reservasi Gagal ditambahkan';
				}
			}
			else
			{
				$out['status'] = false;
				$out['msg'] = show_err_msg(validation_errors());
			}

			$this->session->set_flashdata('pesan', $out['msg']);

			if($out['status'])
				redirect("Reservasi/tampil_reservasi");
			else
				redirect("Reservasi");
		}

		public function delete_reservasi($ID_RESERVASI)
		{
			$result = $this->M_Reservasi->delete_reservasi($ID_RESERVASI);
			redirect('Reservasi');
			if ($result > 0) {
				echo show_succ_msg('Data cabang Berhasil dihapus', '20px');
			} else {
				echo show_err_msg('Data Cabang Gagal dihapus', '20px');
			}
		}

		public function detail_data_reservasi($ID_RESERVASI)
		{
			$data['side']='tampil/side';
			//$data['tbl_pegawai'] = $this->M_Pegawai->get_pegawai();
			$data['detail_reservasi'] = $this->M_Reservasi->detail_reservasi($ID_RESERVASI)->result();
			// $data['detail_pelanggan'] = $this->M_Pelanggan->get_pelanggan($ID_PELANGGAN);
			// $data['detail_cabang'] = $this->M_Cabang->get_cabang($ID_CABANG);
			// $data['detail_reservasi'] = $this->M_Detail->get_detail($ID_DETAIL_RESERVASI)->result();
			// $data['detail_kamar'] = $this->M_Kamar->get_Kamar($ID_KAMAR)->result();
			// $data['detail_jenis_kamar'] = $this->M_Jenis_Kamar->get_jenis_kamar($ID_JENIS_KAMAR)->result();
			$data['content']='tampil_reservasi/detail_reservasi';
			$this->load->view('tampil/main',$data);
		}

		public function pembayaran()
		{
			$data['side']='tampil/side';
			$data['reservasi'] = $this->M_Reservasi->view_data_trasnfer()->result();
			$data['content']='tampil_reservasi/list_pembayaran';
			$this->load->view('tampil/main',$data);
		}

		public function pembayaranID()
		{
			$data['side']='tampil/side';
			$data['reservasi'] = $this->M_Reservasi->view_data_trasnferID($ID_RESERVASI)->result();
			$data['content']='tampil_reservasi/list_pembayaran';
			$this->load->view('tampil/main',$data);
		}
	}
	?>
