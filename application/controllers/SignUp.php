<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class SignUp extends CI_Controller
	{
		public function __construct()
		{
			parent::__construct();

			$this->load->model(array('M_Pelanggan','M_Peran'));
		}

		public function index()
		{
			$data['tbl_peran'] = $this->M_Peran->get_all_peran();
		 	$this->load->view('user/signup',$data);
		}

		// public function tampil_pelanggan()
		// {
		// 	$data['side']='tampil/side';
			
		// 	$data['content']='tampil_pelanggan/tambah_pelanggan';
		// 	$this->load->view('tampil/main',$data);
		// }

		public function simpan_pelanggan()
		{
			$this->form_validation->set_rules('NAMA_PELANGGAN', 'Nama Pelanggan', 'trim|required');
			$this->form_validation->set_rules('EMAIL_PELANGGAN', 'Nama Pelanggan', 'trim|required');
			$this->form_validation->set_rules('ALAMAT', 'Nama Pelanggan', 'trim|required');
			$this->form_validation->set_rules('NO_TELEPON_PELANGGAN', 'Nama Pelanggan', 'trim|required');
			$this->form_validation->set_rules('NO_IDENTITAS', 'Nama Pelanggan', 'trim|required');
			$this->form_validation->set_rules('PASSWORD', 'Nama Pelanggan', 'trim|required');
			$this->form_validation->set_rules('TANGGAL_RESERVASI', 'Nama Pelanggan', 'trim|required');
			$this->form_validation->set_rules('ID_PERAN', 'Nama Pelanggan', 'trim|required');

			if($this->form_validation->run())
			{
				$data = array(
						'NAMA_PELANGGAN' => $this->input->post('NAMA_PELANGGAN'),
						'EMAIL_PELANGGAN' => $this->input->post('EMAIL_PELANGGAN'),
						'ALAMAT' => $this->input->post('ALAMAT'),
						'NO_TELEPON_PELANGGAN' => $this->input->post('NO_TELEPON_PELANGGAN'),
						'NO_IDENTITAS' => $this->input->post('NO_IDENTITAS'),
						'PASSWORD' => $this->input->post('PASSWORD'),
						'TANGGAL_RESERVASI' =>date('Y-m-d',strtotime($this->input->post('TANGGAL_RESERVASI'))),
						'ID_PERAN' => $this->input->post('ID_PERAN')

				);

				$result = $this->M_Pelanggan->tambah_pelanggan($data);

				if ($result < 0) {
					$out['status'] = true;
					$out['msg'] ='Data Pelanggan Berhasil ditambahkan';
				} else {
					$out['status'] = false;
					$out['msg'] = 'Data Pelanggan Gagal ditambahkan';
				}
			} else {
				$out['status'] = false;
				$out['msg'] = show_err_msg(validation_errors());
			}
				$this->session->set_flashdata('pesan', $out['msg']);
				if($out['status'])
					redirect("Pelanggan/tampil_pelanggan");
				else
					redirect("Signin");
		}

		public function delete_pelanggan($ID_PELANGGAN)
		{
			$result = $this->M_Pelanggan->delete_pelanggan($ID_PELANGGAN);
			
			if ($result > 0) {
			echo show_succ_msg('Data Pelanggan Berhasil dihapus', '20px');
			} else {
				echo show_err_msg('Data Pelanggan Gagal dihapus', '20px');
			}
			redirect('Pelanggan');
		}

		public function edit_pelanggan($ID_PELANGGAN)
		{

			$this->form_validation->set_rules('NAMA_PELANGGAN', 'Nama Pelanggan', 'trim|required');
			$this->form_validation->set_rules('EMAIL_PELANGGAN', 'Nama Pelanggan', 'trim|required');
			$this->form_validation->set_rules('ALAMAT', 'Nama Pelanggan', 'trim|required');
			$this->form_validation->set_rules('NO_TELEPON_PELANGGAN', 'Nama Pelanggan', 'trim|required');
			$this->form_validation->set_rules('NO_IDENTITAS', 'Nama Pelanggan', 'trim|required');
			$this->form_validation->set_rules('PASSWORD', 'Nama Pelanggan', 'trim|required');
			$this->form_validation->set_rules('TANGGAL_RESERVASI', 'Nama Pelanggan', 'trim|required');
			$this->form_validation->set_rules('ID_PERAN', 'Nama Pelanggan', 'trim|required');

			$data['show'] = $this->M_Pelanggan->get_pelanggan($ID_PELANGGAN);
			$data['side'] = 'tampil/side';
			$data['tbl_peran'] = $this->M_Peran->get_all_peran();
			$data['content']='tampil_pelanggan/edit_pelanggan';
			$this->load->view('tampil/main',$data);

		}

		public function update_data_pelanggan()
		{
			$this->form_validation->set_rules('NAMA_PELANGGAN', 'Nama Pelanggan', 'trim|required');
			$this->form_validation->set_rules('EMAIL_PELANGGAN', 'Nama Pelanggan', 'trim|required');
			$this->form_validation->set_rules('ALAMAT', 'Nama Pelanggan', 'trim|required');
			$this->form_validation->set_rules('NO_TELEPON_PELANGGAN', 'Nama Pelanggan', 'trim|required');
			$this->form_validation->set_rules('NO_IDENTITAS', 'Nama Pelanggan', 'trim|required');
			$this->form_validation->set_rules('PASSWORD', 'Nama Pelanggan', 'trim|required');
			$this->form_validation->set_rules('TANGGAL_RESERVASI', 'Nama Pelanggan', 'trim|required');
			$this->form_validation->set_rules('ID_PERAN', 'Nama Pelanggan', 'trim|required');

			$data = $this->input->post();
			if($this->form_validation->run() == TRUE)
			{
				$result = $this->M_Pelanggan->update_pelanggan($data);

				if ($result < 0) {
					$out['status'] = true;
					$out['msg'] ='Data Pelanggan Berhasil ditambahkan';
				} else {
					$out['status'] = false;
					$out['msg'] = 'Data Pelanggan Gagal ditambahkan';
				}
			} else {
				$out['status'] = false;
				$out['msg'] = show_err_msg(validation_errors());
			}
				$this->session->set_flashdata('pesan', $out['msg']);

				if($out['status'])
					redirect("Pelanggan/tampil_pelanggan");
				else
					redirect("Pelanggan");
		}
	}
?>