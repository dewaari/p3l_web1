<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class Pembayaran extends CI_Controller
	{
		public function __construct()
		{
			parent::__construct();
			$this->load->model(array('M_Detail_Pembayaran','M_Reservasi'));
		}

		public function index()
		{
			$data['show'] = $this->M_Detail_Pembayaran->select_all_pembayaran(); 
			$data['side']='tampil/side'; 
			$data['content']='tampil_pembayaran/tampil_pembayaran1'; 
		 	$this->load->view('tampil/main',$data);
		}

		public function detail_transfer($ID_RESERVASI)
		{
			//$data['show'] = $this->M_Detail_Pembayaran->select_all_pembayaran(); 
			$data['side']='tampil/side'; 
			//$data['tbl_reservasi'] = $this->M_Reservasi->get_all_reservasi();
			$data['content']='tampil_pembayaran/detail_pembayaran';
			$data['show']= $ID_RESERVASI;
		 	$this->load->view('tampil/main',$data);
		}

		public function detail_kartu($ID_RESERVASI)
		{
			//$data['show'] = $this->M_Detail_Pembayaran->select_all_pembayaran(); 
			$data['side']='tampil/side'; 
			//$data['tbl_reservasi'] = $this->M_Reservasi->get_all_reservasi();
			$data['content']='tampil_pembayaran/detail_kartu';
			$data['show']= $ID_RESERVASI;
		 	$this->load->view('tampil/main',$data);
		}

		public function bayar_transfer($ID_PEMBAYARAN)
		{
			$data['side']='tampil/side';
			$data['bayar_transfer'] = $this->M_Detail_Pembayaran->select_data_transfer($ID_PEMABAYARAN)->result();
			$data['content']='tampil_pembayaran/tampil_pembayaran1';
			$this->load->view('tampil/main',$data);
		}

		public function tambah_transfer()
		{
			$this->form_validation->set_rules('ID_RESERVASI' , 'ID Booking', 'trim|required');
			$this->form_validation->set_rules('NAMA_PEMILIK' , 'ID Booking', 'trim|required');
			$this->form_validation->set_rules('NAMA_BANK' , 'ID Booking', 'trim|required');
			$this->form_validation->set_rules('TANGGAL_PEMBAYARAN' , 'ID Booking', 'trim|required');
			$this->form_validation->set_rules('TOTAL_PEMBAYARAN_AWAL' , 'ID Booking', 'trim|required');

			$nama = 'file_'.time();
			$config['upload_path'] = './assets/images/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']	= '10000'; //in kb
			$config['max_width']  = '1024';
			$config['max_height']  = '768';
			$config['file_name']	= $nama;

			$this->upload->initialize($config);
			if ($this->form_validation->run() == TRUE && $this->upload->do_upload('foto'))
			{
				$gambar = $this->upload->data();
				$data = array(
						'ID_RESERVASI'	=> $this->input->post('ID_RESERVASI',TRUE),
						'NAMA_PEMILIK'		=> $this->input->post('NAMA_PEMILIK',TRUE),
						'NAMA_BANK'		=> $this->input->post('NAMA_BANK',TRUE),
						'TANGGAL_PEMBAYARAN'	=> $this->input->post('TANGGAL_PEMBAYARAN',TRUE),
						'TOTAL_PEMBAYARAN_AWAL'	=> $this->input->post('TOTAL_PEMBAYARAN_AWAL',TRUE),
						'BUKTI_PEMBAYARAN'		=> $gambar['file_name']

				);
				//$this->M_Reservasi->get_reservasi($ID_RESERVASI);
				$this->M_Detail_Pembayaran->tambah_bayar_transfer($data);
				redirect("Pembayaran");

			}
			else
			{
				
				$data['foto'] = '';

				if (! $this->upload->do_upload('foto')) 
				{
					$data['foto'] = $this->upload->display_errors();
				}else{
					return TRUE;
				}
				
			}
		}

		public function tambah_kartu()
		{
			$this->form_validation->set_rules('ID_RESERVASI' , 'ID Booking', 'trim|required');
			$this->form_validation->set_rules('NAMA_PEMILIK' , 'ID Booking', 'trim|required');
			$this->form_validation->set_rules('NAMA_BANK' , 'ID Booking', 'trim|required');
			$this->form_validation->set_rules('NO_KARTU' , 'No Kartu', 'trim|required');
			$this->form_validation->set_rules('TANGGAL_PEMBAYARAN' , 'ID Booking', 'trim|required');
			$this->form_validation->set_rules('TOTAL_PEMBAYARAN_AWAL' , 'ID Booking', 'trim|required');

			if($this->form_validation->run())
			{
				$data = array(
						'ID_RESERVASI'	=> $this->input->post('ID_RESERVASI',TRUE),
						'NAMA_PEMILIK'		=> $this->input->post('NAMA_PEMILIK',TRUE),
						'NAMA_BANK'		=> $this->input->post('NAMA_BANK',TRUE),
						'NO_KARTU'		=> $this->input->post('NO_KARTU',TRUE),
						'TANGGAL_PEMBAYARAN'	=> $this->input->post('TANGGAL_PEMBAYARAN',TRUE),
						'TOTAL_PEMBAYARAN_AWAL'	=> $this->input->post('TOTAL_PEMBAYARAN_AWAL',TRUE)
				);

				$result = $this->M_Detail_Pembayaran->tambah_bayar_kartu($data);

				if ($result < 0) {
					$out['status'] = true;
					$out['msg'] ='Data Pelanggan Berhasil ditambahkan';
				} else {
					$out['status'] = false;
					$out['msg'] = 'Data Pelanggan Gagal ditambahkan';
				}

			} else {
				$out['status'] = false;
				$out['msg'] = show_err_msg(validation_errors());
			}
			$this->session->set_flashdata('pesan', $out['msg']);
			if($out['status'])
				redirect("Pembayaran/tampil_pembayaran1");
			else
				redirect("Pembayaran");
		}
	}
?>