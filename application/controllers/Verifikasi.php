<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
	class Verifikasi extends CI_Controller 
	{
		public function __construct()
		{
			parent::__construct();
			$this->load->model(array('M_Reservasi','M_Detail_Pembayaran'));
			
		}

		public function index($ID_PEMBAYARAN) {

			$data['detailnota'] = $this->M_Detail_Pembayaran->detail_data_nota($ID_PEMBAYARAN)->result();
			$this->load->library('pdf_report');
			$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

			// set document information
			$pdf->SetCreator(PDF_CREATOR);
			$pdf->SetAuthor('Nicola Asuni');
			$pdf->SetTitle('TCPDF Example 005');
			$pdf->SetSubject('TCPDF Tutorial');
			$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

			// set default header data
			$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 005', PDF_HEADER_STRING);

			// set header and footer fonts
			$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
			$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

			// set default monospaced font
			$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

			// set margins
			$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
			$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
			$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

			// set auto page breaks
			$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

			// set image scale factor
			$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

			// set some language-dependent strings (optional)
			if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
				require_once(dirname(__FILE__).'/lang/eng.php');
				$pdf->setLanguageArray($l);
			}

			// ---------------------------------------------------------

			// set font
			$pdf->SetFont('times', '', 10);

			// add a page
			$pdf->AddPage();

			// set cell padding
			$pdf->setCellPaddings(1, 1, 1, 1);

			// set cell margins
			$pdf->setCellMargins(1, 1, 1, 1);

			// set color for background
			$pdf->SetFillColor(255, 255, 127);

			$html = $this->load->view('tampil_pembayaran/Verifikasi_Pembayaran', $data, true);

			$pdf->WriteHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, 'C', true);  
    		$pdf->Output('bulletin_de_paie.pdf', 'I'); 


		}
	}
?>