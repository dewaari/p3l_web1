<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* 
*/
	class Tempat_Tidur extends CI_Controller
	{
		
		public function __construct() {
			parent::__construct();
			$this->load->model('M_Tempat_Tidur');
			
		}

		public function index() 
		{

			$data['show'] = $this->M_Tempat_Tidur->select_all();
			$data['side']='tampil/side';
			$data['content']='tampil_tempat_tidur/tampil_tempat_tidur1';
		 	$this->load->view('tampil/main',$data);
		}

		public function tampil_tempat_tidur()
		{
			$data 	= $this->input->post();
			$data['side']='tampil/side';	
		 	$data['content']='tampil_tempat_tidur/tambah_tempat_tidur';
		 	$this->load->view('tampil/main',$data);
		}

		public function simpan_tempat()
		{
			$this->form_validation->set_rules('JENIS_TEMPAT_TIDUR', 'Jenis Tempat Tidur', 'trim|required');
			$this->form_validation->set_rules('HARGA', 'Hargas', 'trim|required');
			
			if ($this->form_validation->run()) 
			{
				$data = array(
					'JENIS_TEMPAT_TIDUR' => $this->input->post('JENIS_TEMPAT_TIDUR'),
					'HARGA' => $this->input->post('HARGA'),
					
				);

				$result = $this->M_Tempat_Tidur->tambah_tempat_tidur($data);

				if ($result < 0) {
					$out['status'] = true;
					$out['msg'] ='Data Pegawai Berhasil ditambahkan';
				// jika salah
				} else {
					$out['status'] = false;
					$out['msg'] = 'Data Pegawai Gagal ditambahkan';
				}
			} 
			else
			{
				$out['status'] = false;
				$out['msg'] = show_err_msg(validation_errors());
			}

			$this->session->set_flashdata('pesan', $out['msg']);

			//jika out status true maka buka index session (tampilkan data)
			if($out['status'])
				redirect("Tempat_Tidur/tampil_tempat_tidur");
			//jika gagal maka redirect atau tampilkan session/tambah
			else
				redirect("Tempat_Tidur");

		}

		public function delete($ID_TEMPAT_TIDUR)
		{
			$result = $this->M_Tempat_Tidur->delete($ID_TEMPAT_TIDUR);
			redirect('Tempat_Tidur');
			if ($result > 0) {
				echo show_succ_msg('Data cabang Berhasil dihapus', '20px');
			} else {
				echo show_err_msg('Data Cabang Gagal dihapus', '20px');
			}
		}


		public function edit_tempat($ID_TEMPAT_TIDUR)
		{

			$this->form_validation->set_rules('JENIS_TEMPAT_TIDUR', 'Jenis Tempat Tidur', 'trim|required');
			$this->form_validation->set_rules('HARGA', 'Hargas', 'trim|required');

			$data['show'] = $this->M_Tempat_Tidur->get_tempat_tidur($ID_TEMPAT_TIDUR);
			$data['side']='tampil/side';	
		 	$data['content']='tampil_tempat_tidur/edit_tempat_tidur';
		 	$this->load->view('tampil/main',$data);
		}


		public function update_tempat()
		{
			$this->form_validation->set_rules('JENIS_TEMPAT_TIDUR', 'Jenis Tempat Tidur', 'trim|required');
			$this->form_validation->set_rules('HARGA', 'Hargas', 'trim|required');
			
			$data 	= $this->input->post();
			if ($this->form_validation->run() == TRUE) {
				$result = $this->M_Tempat_Tidur->update_tempat_tidur($data);
				redirect('Tempat_Tidur');
				if ($result > 0) {
					$out['status'] = '';
					$out['msg'] = show_succ_msg('Data Fasilitas Berhasil ditambahkan', '20px');
				} else {
					$out['status'] = '';
					$out['msg'] = show_err_msg('Data Fasilitas Gagal ditambahkan', '20px');
				}
			} else {
				$out['status'] = 'form';
				$out['msg'] = show_err_msg(validation_errors());
			}
		}

	}
?>
