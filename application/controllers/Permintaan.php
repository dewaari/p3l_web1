<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class Permintaan extends CI_Controller
	{
		public function __construct()
		{
			parent::__construct();

			$this->load->model(array('M_Permintaan', 'M_Reservasi'));
			$this->load->library('session');
		}

		public function index()
		{
			$data['show'] = $this->M_Permintaan->select_all(); 
			$data['side']='tampil/side'; 
			$data['content']='tampil_permintaan/tampil_permintaan1'; 
		 	$this->load->view('tampil/main',$data); 
		 }

		 public function tampil_permintaan()
		{
			$data['side']='tampil/side';
			$data['tbl_reservasi'] = $this->M_Reservasi->get_all_reservasi();
		 	$data['content']='tampil_permintaan/tambah_permintaan';
		 	$this->load->view('tampil/main',$data);
		}

		public function tambah_permintaan()
		{
			$this->form_validation->set_rules('ID_RESERVASI', 'ID Reservasi', 'trim|required');
			$this->form_validation->set_rules('NAMA_PERMINTAAN', 'Nama Permintaan', 'trim|required');
			$this->form_validation->set_rules('JUMLAH_PERMINTAAN', 'Jumlah Permintaan', 'trim|required');

			if ($this->form_validation->run()) 
			{

				$data = array(
						'ID_RESERVASI' => $this->input->post('ID_RESERVASI'),
						'NAMA_PERMINTAAN' => $this->input->post('NAMA_PERMINTAAN'),
						'JUMLAH_PERMINTAAN' => $this->input->post('JUMLAH_PERMINTAAN'),
				);

				$result = $this->M_Permintaan->tambah_permintaan($data);

				
				if ($result < 0) {
					$out['status'] = true;
					$out['msg'] ='Data Permintaan Berhasil ditambahkan';
				} else {
					$out['status'] = false;
					$out['msg'] = 'Data Permintaan Gagal ditambahkan';
				}
			}
			else
			{
				$out['status'] = false;
				$out['msg'] = show_err_msg(validation_errors());
			}

			$this->session->set_flashdata('pesan', $out['msg']);
			if($out['status'])
				redirect("Permintaan/tampil_permintaan1");
			else
				redirect("Permintaan");
		}

		public function delete_permintaan($ID_PERMINTAAN)
		{
			$result = $this->M_Permintaan->delete($ID_PERMINTAAN);
			redirect('Permintaan');
			if ($result > 0) {
				echo show_succ_msg('Data cabang Berhasil dihapus', '20px');
			} else {
				echo show_err_msg('Data Cabang Gagal dihapus', '20px');
			}
		}

		public function edit_permintaan($ID_PERMINTAAN)
		{

			$this->form_validation->set_rules('ID_RESERVASI', 'ID Reservasi', 'trim|required');
			$this->form_validation->set_rules('NAMA_PERMINTAAN', 'Nama Permintaan', 'trim|required');
			$this->form_validation->set_rules('JUMLAH_PERMINTAAN', 'Jumlah Permintaan', 'trim|required');

			$data['show'] = $this->M_Permintaan->get_permintaan($ID_PERMINTAAN);
			$data['side'] = 'tampil/side';
			$data['tbl_reservasi'] = $this->M_Reservasi->get_all_reservasi();
			$data['content']='tampil_permintaan/edit_permintaan';
			$this->load->view('tampil/main',$data);

		}

		public function update_data_permintaan()
		{
			$this->form_validation->set_rules('ID_RESERVASI', 'ID Reservasi', 'trim|required');
			$this->form_validation->set_rules('NAMA_PERMINTAAN', 'Nama Permintaan', 'trim|required');
			$this->form_validation->set_rules('JUMLAH_PERMINTAAN', 'Jumlah Permintaan', 'trim|required');


			$data = $this->input->post();
			if($this->form_validation->run() == TRUE)
			{
				$result = $this->M_Permintaan->update_permintaan($data);

				if ($result < 0) {
					$out['status'] = true;
					$out['msg'] ='Data Permintaan Berhasil ditambahkan';
				} else {
					$out['status'] = false;
					$out['msg'] = 'Data Permintaan Gagal ditambahkan';
				}
			} else {
				$out['status'] = false;
				$out['msg'] = show_err_msg(validation_errors());
			}
				$this->session->set_flashdata('pesan', $out['msg']);

				if($out['status'])
					redirect("Permintaan/tampil_permintaan");
				else
					redirect("Permintaan");
		}


	}
?>