<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();

		$this->load->model('M_Login'); //load session setiap form di buka
	//	$this->load->model(array('M_Kamar','M_Cabang','M_Jenis_Kamar', 'M_Tempat_Tidur'));
	}

	public function login_user()
	{
		$e = $this->input->post('EMAIL_PEGAWAI');
		$p = $this->input->post('PASSWORD');
	//var_dump($p);
		$this->M_Login->login($e,$p);
	}

	public function index()
	{
//$data['show'] = $this->M_Metode->select_all(); //ngambil data dari function select_all simpan ke variabel $data['show']
// $data['side']='tampil/side'; //tampil/side simpan ke variabel $data['side']
		$this->load->view('Login_Tampil/login_tampil');
//$this->load->view('Login_Tampil/ubah_password'); //tampil_sesion/tampil_sesion1 simpan ke variabel $data['content']
//$this->load->view('tampil/main',$data);
//$this->load->view('login/login_tampil'); //tampilkan view tampil/main lewatkan variabel $data;
	}

	public function ubah_password()
	{
		$this->load->view('Login_Tampil/ubah_password');
	}

	public function change_password()
	{
		if ($this->input->post('PASSWORD_BARU') === $this->input->post('PASSWORD_BARU_2'))
		{		
			$data['EMAIL_PEGAWAI'] = $this->input->post('EMAIL_PEGAWAI');
			$data['PASSWORD_LAMA'] = $this->input->post('PASSWORD_LAMA');
			$data['PASSWORD_BARU'] = $this->input->post('PASSWORD_BARU');

			$changed = $this->M_Login->change_password($data);
			if ($changed) {
				redirect('Login');
			}
			else
			{
				redirect('Login/ubah_password');
			}
		}
		else
		{
			redirect('Login/ubah_password');
		}
	}
}