<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class Sesion extends CI_Controller
	{

		public function __construct()
		{
			parent::__construct();

			$this->load->model(array('M_Sesion', 'M_Jenis_Kamar'));
		}

		public function index()
		{
			$data['show'] = $this->M_Sesion->select_all(); 
			$data['side']='tampil/side'; 
			$data['content']='tampil_sesion/tampil_sesion1'; 

		 	$this->load->view('tampil/main',$data); 
		}

		public function tampil_sesion()
		{

			$data['side']='tampil/side';
			$data['tbl_jenis_kamar'] = $this->M_Jenis_Kamar->get_all_jenis_kamar();
			$data['content']='tampil_sesion/tambah_sesion';
			$this->load->view('tampil/main',$data);

		}

		public function simpan_sesion()
		{
			
			$this->form_validation->set_rules('ID_JENIS_KAMAR', 'ID Jenis Kamar', 'trim|required');
			$this->form_validation->set_rules('NAMA_SESION', 'Nama Sesion', 'trim|required');
			$this->form_validation->set_rules('TANGGAL_MULAI', 'Tanggal Mulai', 'trim|required');
			$this->form_validation->set_rules('TANGGAL_SELESAI', 'Tanggal Selesai', 'trim|required');
			$this->form_validation->set_rules('HARGA_SESION', 'Harga Sesion', 'trim|required');
			$this->form_validation->set_rules('STATUS_SESION', 'Status Sesion', 'trim|required');

			if ($this->form_validation->run()) {
				$data= array(
					'ID_JENIS_KAMAR' =>$this->input->post('ID_JENIS_KAMAR'),
					'NAMA_SESION' =>$this->input->post('NAMA_SESION'),
					'TANGGAL_MULAI' =>date('Y-m-d',strtotime($this->input->post('TANGGAL_MULAI'))),
					'TANGGAL_SELESAI' =>date('Y-m-d',strtotime($this->input->post('TANGGAL_SELESAI'))),
					'HARGA_SESION' =>$this->input->post('HARGA_SESION'),
					'STATUS_SESION' =>$this->input->post('STATUS_SESION')
				);
				
				$result = $this->M_Sesion->tambah_sesion($data);

				if ($result < 0) {
					$out['status'] = true;
					$out['msg'] ='Data Fasilitas Berhasil ditambahkan';
				} else {
					$out['status'] = false;
					$out['msg'] = 'Data Fasilitas Gagal ditambahkan';
				}
			} else {
				$out['status'] = false;
				$out['msg'] = show_err_msg(validation_errors());
			}

			$this->session->set_flashdata('pesan', $out['msg']);

			if($out['status'])
				redirect("Sesion/tampil_sesion");
			else
				redirect("Sesion");
		}

		public function delete_sesion($ID_SESION)
		{
			$result = $this->M_Sesion->delete($ID_SESION);
			
			if ($result > 0) {
			echo show_succ_msg('Data Pelanggan Berhasil dihapus', '20px');
			} else {
				echo show_err_msg('Data Pelanggan Gagal dihapus', '20px');
			}
			redirect('Sesion');
		}

		public function edit_sesion($ID_SESION)
		{

			$this->form_validation->set_rules('ID_JENIS_KAMAR', 'ID Jenis Kamar', 'trim|required');
			$this->form_validation->set_rules('NAMA_SESION', 'Nama Sesion', 'trim|required');
			$this->form_validation->set_rules('TANGGAL_MULAI', 'Tanggal Mulai', 'trim|required');
			$this->form_validation->set_rules('TANGGAL_SELESAI', 'Tanggal Selesai', 'trim|required');
			$this->form_validation->set_rules('HARGA_SESION', 'Harga Sesion', 'trim|required');
			$this->form_validation->set_rules('STATUS_SESION', 'Status Sesion', 'trim|required');

			$data['show'] = $this->M_Sesion->get_sesion($ID_SESION);
			$data['side']='tampil/side';
			$data['tbl_jenis_kamar'] = $this->M_Jenis_Kamar->get_all_jenis_kamar();
			$data['content']='tampil_sesion/edit_sesion';
			$this->load->view('tampil/main',$data);
		}

		public function update_data_sesion()
		{
			$this->form_validation->set_rules('ID_JENIS_KAMAR', 'ID Jenis Kamar', 'trim|required');
			$this->form_validation->set_rules('NAMA_SESION', 'Nama Sesion', 'trim|required');
			$this->form_validation->set_rules('TANGGAL_MULAI', 'Tanggal Mulai', 'trim|required');
			$this->form_validation->set_rules('TANGGAL_SELESAI', 'Tanggal Selesai', 'trim|required');
			$this->form_validation->set_rules('HARGA_SESION', 'Harga Sesion', 'trim|required');
			$this->form_validation->set_rules('STATUS_SESION', 'Status Sesion', 'trim|required');

			$data 	= $this->input->post();
			if ($this->form_validation->run()) 
			{

				// $data= array(
				// 	'ID_JENIS_KAMAR' =>$this->input->post('ID_JENIS_KAMAR'),
				// 	'NAMA_SESION' =>$this->input->post('NAMA_SESION'),
				// 	'TANGGAL_MULAI' =>date('Y-m-d',strtotime($this->input->post('TANGGAL_MULAI'))),
				// 	'TANGGAL_SELESAI' =>date('Y-m-d',strtotime($this->input->post('TANGGAL_SELESAI'))),
				// 	'HARGA_SESION' =>$this->input->post('HARGA_SESION'),
				// 	'STATUS_SESION' =>$this->input->post('STATUS_SESION')
				// );
				
				$result = $this->M_Sesion->update_sesion($data);

				if ($result < 0) {
					$out['status'] = true;
					$out['msg'] ='Data Fasilitas Berhasil ditambahkan';
				} else {
					$out['status'] = false;
					$out['msg'] = 'Data Fasilitas Gagal ditambahkan';
				}
			} else {
				$out['status'] = false;
				$out['msg'] = show_err_msg(validation_errors());
			}

			$this->session->set_flashdata('pesan', $out['msg']);

			if($out['status'])
				redirect("Sesion/tampil_sesion");
			else
				redirect("Sesion");
		}



	}

?>