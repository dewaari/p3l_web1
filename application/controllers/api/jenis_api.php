<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
//use Restserver\Libraries\REST_Controller;

class jenis_api extends REST_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('M_Jenis_Kamar');
	}

	public function index_get()
	{
		$response['error'] = FALSE;
        $response['message'] = "";
        $response['data'] = $this->M_Jenis_Kamar->select_all();
        $this->response($response, REST_Controller::HTTP_OK); 
	}

	// public function cabang_post()
	// {
	// 	$data = array(
	// 				'id_cabang'			=>$this->post('id_cabang'),
	// 				'nama_kota'			=>$this->post('nama_kota'),
	// 				'alamat'			=>$this->post('alamat'),
	// 				'No_Telp_Cabang'	=>$this->post('No_Telp_Cabang'));

	// 	$insert = $this->cabang_model->create_cabang($data);

	// 	if($insert)
	// 	{
	// 		$response['error'] = FALSE;
	//         $response['message'] = "Failed Insert data";
	//         //$response['data'] = $this->cabang_model->get_cabang();
	//         $this->response($response, REST_Controller::HTTP_CREATED); 
	// 	}
	// 	else
	// 	{
	// 		$response['error'] = FALSE;
	//         $response['message'] = "";
	//         //$response['data'] = $this->cabang_model->get_cabang();
	//         $this->response($response, REST_Controller::HTTP_BAD_REQUEST); 
	// 	}
	// }

	// public function cabang_edit()
	// {
	// 	$id_cabang = $this->put('id_cabang');
	// 	$data = array(
	// 				'id_cabang'			=>$this->put('id_cabang'),
	// 				'nama_kota'			=>$this->put('nama_kota'),
	// 				'alamat'			=>$this->put('alamat'),
	// 				'No_Telp_Cabang'	=>$this->put('No_Telp_Cabang'));

	// 	$update = $this->cabang_model->update_cabang($data);

	// 	if($update)
	// 	{
	// 		$response['error'] = FALSE;
	//         $response['message'] = "Failed Update data";
	//         //$response['data'] = $this->cabang_model->get_cabang();
	//         $this->response($response, REST_Controller::HTTP_CREATED); 
	// 	}
	// 	else
	// 	{
	// 		$response['error'] = FALSE;
	//         $response['message'] = "";
	//         //$response['data'] = $this->cabang_model->get_cabang();
	//         $this->response($response, REST_Controller::HTTP_BAD_REQUEST); 
	// 	}
	// }

}


/* End of file Cabang.php */
/* Location: ./application/controllers/Cabang.php */