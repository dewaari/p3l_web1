<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
//use Restserver\Libraries\REST_Controller;

class Api_Kamar extends REST_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('M_Kamar');
	}

	public function index_get()
	{
		$response['error'] = FALSE;
        $response['message'] = "";
        $response['data'] = $this->M_Kamar->select_all();
        $this->response($response, REST_Controller::HTTP_OK); 
	}
}
?>