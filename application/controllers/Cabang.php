<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
	class Cabang extends CI_Controller 
	{
	 	
	 	public function __construct() 
	 	{
			parent::__construct();
			$this->load->model('M_Cabang');
			$this->load->library('session');
		}

		public function index() {
			$data['show'] = $this->M_Cabang->select_all();
			$data['side']='tampil/side';
			$data['content']='tampil_cabang/tampil_cabang';
		 	$this->load->view('tampil/main',$data);
		}

		public function tambah()
		{
			$this->form_validation->set_rules('NAMA_KOTA', 'Nama_Kota', 'trim|required');
			$this->form_validation->set_rules('ALAMAT', 'Alamat', 'trim|required');
			$this->form_validation->set_rules('NO_TELP_CABANG', 'No Telepon', 'trim|required');

			$data 	= $this->input->post();
			$data['side']='tampil/side';
		 	$data['content']='tampil_cabang/tambah_cabang';
		 	$this->load->view('tampil/main',$data);
			if ($this->form_validation->run() == TRUE) {
				$result = $this->M_Cabang->insert($data);
				redirect('Cabang');
				if ($result > 0) {
					$out['status'] = '';
					$out['msg'] = show_succ_msg('Data Cabang Berhasil ditambahkan', '20px');
				} else {
					$out['status'] = '';
					$out['msg'] = show_err_msg('Data Cabang Gagal ditambahkan', '20px');
				}
			} else {
				$out['status'] = 'form';
				$out['msg'] = show_err_msg(validation_errors());
			}
		}

		

		public function delete($ID_CABANG) 
		{
			$result = $this->M_Cabang->delete($ID_CABANG);
			redirect('Cabang');
			if ($result > 0) {
				echo show_succ_msg('Data cabang Berhasil dihapus', '20px');
			} else {
				echo show_err_msg('Data Cabang Gagal dihapus', '20px');
			}
		}

		public function edit($ID_CABANG)
		{
			$this->form_validation->set_rules('NAMA_KOTA', 'Nama_Kota', 'trim|required');
			$this->form_validation->set_rules('ALAMAT', 'Alamat', 'trim|required');
			$this->form_validation->set_rules('NO_TELP_CABANG', 'No Telepon', 'trim|required');

			$data['show'] = $this->M_Cabang->get_cabang($ID_CABANG);
			$data['side']='tampil/side';
		 	$data['content']='tampil_cabang/edit_cabang';
		 	$this->load->view('tampil/main',$data);
		}

		public function update()
		{
			$this->form_validation->set_rules('NAMA_KOTA', 'Nama_Kota', 'trim|required');
			$this->form_validation->set_rules('ALAMAT', 'Alamat', 'trim|required');
			$this->form_validation->set_rules('NO_TELP_CABANG', 'No Telepon', 'trim|required');

			$data 	= $this->input->post();
			if ($this->form_validation->run() == TRUE) {
				$result = $this->M_Cabang->update($data);
				redirect('Cabang');
				if ($result > 0) {
					$out['status'] = '';
					$out['msg'] = show_succ_msg('Data Cabang Berhasil ditambahkan', '20px');
				} else {
					$out['status'] = '';
					$out['msg'] = show_err_msg('Data Cabang Gagal ditambahkan', '20px');
				}
			} else {
				$out['status'] = 'form';
				$out['msg'] = show_err_msg(validation_errors());
			}

		}
	}
?>