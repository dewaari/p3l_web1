<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* 
*/
	class Fasilitas extends CI_Controller
	{
		
		public function __construct()
		{
			parent::__construct();
			$this->load->model(array('M_Fasilitas', 'M_Cabang'));
			$this->load->library('session');
		}

		public function index() {
			$data['show'] = $this->M_Fasilitas->select_all();
			$data['side']='tampil/side';
			$data['content']='tampil_fasilitas/tampil_fasilitas';
		 	$this->load->view('tampil/main',$data);
		}

		public function tampil_fasilitas()
		{
			$data 	= $this->input->post();
			$data['side']='tampil/side';
			$data['tbl_cabang'] = $this->M_Cabang->get_all_cabang();
		 	$data['content']='tampil_fasilitas/tambah_fasilitas';
		 	$this->load->view('tampil/main',$data);
		}

		public function tambah()
		{
			$this->form_validation->set_rules('ID_CABANG', 'ID Cabang', 'trim|required');
			$this->form_validation->set_rules('NAMA_FASILITAS', 'Nama Fasilitas', 'trim|required');
			$this->form_validation->set_rules('HARGA_FASILITAS', 'Harga Fasilitas', 'trim|required');

			if ($this->form_validation->run()) 
			{
				$data = array(
					'ID_CABANG' => $this->input->post('ID_CABANG'),
					'NAMA_FASILITAS' => $this->input->post('NAMA_FASILITAS'),
					'HARGA_FASILITAS' => $this->input->post('HARGA_FASILITAS')
				);

				$result = $this->M_Fasilitas->insert($data);

				if ($result < 0) {
					$out['status'] = true;
					$out['msg'] ='Data Pegawai Berhasil ditambahkan';
				// jika salah
				} else {
					$out['status'] = false;
					$out['msg'] = 'Data Pegawai Gagal ditambahkan';
				}
			} 
			else
			{
				$out['status'] = false;
				$out['msg'] = show_err_msg(validation_errors());
			}

			$this->session->set_flashdata('pesan', $out['msg']);

			//jika out status true maka buka index session (tampilkan data)
			if($out['status'])
				redirect("Fasilitas/tampil_fasilitas");
			//jika gagal maka redirect atau tampilkan session/tambah
			else
				redirect("Fasilitas");

		}

		public function delete($ID_FASILITAS)
		{
			$result = $this->M_Fasilitas->delete($ID_FASILITAS);
			redirect('Fasilitas');
			if ($result > 0) {
				echo show_succ_msg('Data cabang Berhasil dihapus', '20px');
			} else {
				echo show_err_msg('Data Cabang Gagal dihapus', '20px');
			}
		}

		public function edit($ID_FASILITAS)
		{
			$this->form_validation->set_rules('ID_CABANG', 'ID Cabang', 'trim|required');
			$this->form_validation->set_rules('NAMA_FASILITAS', 'Nama Fasilitas', 'trim|required');
			$this->form_validation->set_rules('HARGA_FASILITAS', 'Harga Fasilitas', 'trim|required');

			$data['show'] = $this->M_Fasilitas->get_fasilitas($ID_FASILITAS);
			$data['tbl_cabang'] = $this->M_Cabang->get_all_cabang();
			$data['side'] = 'tampil/side';
			$data['content'] = 'tampil_fasilitas/edit_fasilitas';
			$this->load->view('tampil/main', $data);
		}

		public function update_data_fasilitas()
		{
			$this->form_validation->set_rules('ID_CABANG', 'ID Cabang', 'trim|required');
			$this->form_validation->set_rules('NAMA_FASILITAS', 'Nama Fasilitas', 'trim|required');
			$this->form_validation->set_rules('HARGA_FASILITAS', 'Harga Fasilitas', 'trim|required');

			$data 	= $this->input->post();
			if ($this->form_validation->run() == TRUE) 
			{
				$result = $this->M_Fasilitas->update_fasilitas($data);
				redirect('Fasilitas');
				if ($result > 0) {
					$out['status'] = '';
					$out['msg'] = show_succ_msg('Data Fasilitas Berhasil ditambahkan', '20px');
				} else {
					$out['status'] = '';
					$out['msg'] = show_err_msg('Data Fasilitas Gagal ditambahkan', '20px');
				}
			} else {
				$out['status'] = 'form';
				$out['msg'] = show_err_msg(validation_errors());
			}
		}

	}
?>