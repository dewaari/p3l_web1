<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Signin extends CI_Controller {

	function __construct(){
		parent::__construct();

		// $this->load->model('m_kontenweb');
		// $this->load->model('user');
		// $this->load->helper('url');
		$this->load->model('M_Signin');
	}

  public function index()
  {
		
			$this->load->view('user/signin');
		
  }

  public function cek_login()
  {
  	$EMAIl_PELANGGAN = $this->input->post('EMAIL_PELANGGAN');
  	$PASSWORD = $this->input->post('PASSWORD');
  	$this->M_Signin->signin($EMAIl_PELANGGAN, $PASSWORD);
  }
}
?>