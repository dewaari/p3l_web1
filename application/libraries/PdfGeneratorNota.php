<?php defined('BASEPATH') OR exit('No direct script access allowed');

//use Dompdf\Dompdf;
//define('DOMPDF_ENABLE_AUTOLOAD', false);
require_once("dompdf/autoload.inc.pdf");
use Dompdf\Dompdf;

class PdfGeneratorNota
{
  public function generate($html,$filename)
  {
    

    $dompdf = new DOMPDF();
    $dompdf->load_html($html);
    $dompdf->set_paper('A4', 'Portrait');
    $dompdf->render();
    $dompdf->stream($filename.'.pdf',array("Attachment"=>0));
  }
}