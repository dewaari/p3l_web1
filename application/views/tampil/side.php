<!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="../../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Dewa Ari</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->

      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-database"></i> <span>Master Data</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <?php if ($this->session->userdata('ADMIN')): ?>
              <li><a href="<?php echo base_url('index.php/Cabang') ?>"><i class="fa fa-circle-o"></i> Cabang Master</a></li>
             <li><a href="<?php echo base_url('index.php/Fasilitas') ?>"><i class="fa fa-circle-o"></i> Fasilitas Master</a></li>
             <li><a href="<?php echo base_url('index.php/Jenis_Kamar') ?>"><i class="fa fa-circle-o"></i> Jenis Kamar Master</a></li>
             <li><a href="<?php echo base_url('index.php/Kamar') ?>"><i class="fa fa-circle-o"></i> Kamar Master</a></li>
             <li><a href="<?php echo base_url('index.php/Sesion') ?>"><i class="fa fa-circle-o"></i> Sesion Master</a></li>
             <li><a href="<?php echo base_url('index.php/Pegawai') ?>"><i class="fa fa-circle-o"></i> Pegawai Master</a></li>
             <li><a href="<?php echo base_url('index.php/Pelanggan') ?>"><i class="fa fa-circle-o"></i> Pelanggan Master</a></li>
             <li><a href="<?php echo base_url('index.php/Tempat_Tidur') ?>"><i class="fa fa-circle-o"></i> Tempat Tidur Master</a></li>
             <li><a href="<?php echo base_url('index.php/Permintaan') ?>"><i class="fa fa-circle-o"></i> Permintaan Khusus Master</a></li>
             <li><a href="<?php echo base_url('index.php/Pembayaran') ?>"><i class="fa fa-circle-o"></i>Pembayaran Transfer Master</a></li>
            <li><a href="<?php echo base_url('index.php/Pembayaran_Kartu') ?>"><i class="fa fa-circle-o"></i>Pembayaran Kartu Kredit Master</a></li>

            
             
             <!-- <li><a href="<?php echo base_url('index.php/Login') ?>"><i class="fa fa-circle-o"></i> Login Master</a></li> -->
          </ul>
        </li>

        <li>
          <a href="#">
            <i class="fa fa-bookmark"></i> <span>Reservasi</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
               <li><a href="<?php echo base_url('index.php/Reservasi/tampil_reservasi') ?>"><i class="fa fa-circle-o"></i> Tambah Reservasi</a></li>
                <li><a href="<?php echo base_url('index.php/Reservasi/index') ?>"><i class="fa fa-circle-o"></i> Tampil Reservasi</a></li>
          </ul>
        </li>

         <li>
          <a href="#">
            <i class="fa fa-credit-card"></i> <span>Pembayaran</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
               <li><a href="<?php echo base_url('index.php/Reservasi/pembayaran') ?>"><i class="fa fa-circle-o"></i>List Pembayaran</a></li>
          </ul>
        </li>
        <?php endif ?>
            
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>