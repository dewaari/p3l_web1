<div class="col-md-6">
  <div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Form Add</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <form method="post" action="<?php echo base_url('index.php/Permintaan/tambah_permintaan') ?>">
      <div class="box-body">

        <div class="form-group">
          <label for="exampleInputPassword1">Nama Permintaan</label>
          <input type="text" pattern="[A-Z a-z]*" title="Inputan Harus Text" class="form-control" id="NAMA_PERMINTAAN" name="NAMA_PERMINTAAN" placeholder="Nama Permintaan" required="/required">
        </div>

        <div class="form-group">
         <label for="exampleInputEmail1">Kode Booking</label>
         <select class="form-control" id="ID_RESERVASI", name="ID_RESERVASI" required="/required">
          <option value="0" selected="selected" disabled="disabled">Kode Booking</option>
            <?php foreach ($tbl_reservasi as $c){ ?>
              <option value='<?php echo $c->ID_RESERVASI?>'><?php echo $c->ID_BOOKING?></option><?php }?>
        </select>
      </div>

      <div class="form-group">
        <label for="exampleInputPassword2">Jumlah Permintaan</label>
        <input type="number" class="form-control" id="JUMLAH_PERMINTAAN" name="JUMLAH_PERMINTAAN" placeholder="Jumlah Permintaan" required="/required">
      </div>

    </div>
    <!-- /.box-body -->

   <div class="box-footer">
        <button type="submit" class="btn btn-success">Submit</button>
          <a href="<?php echo base_url('Permintaan/index'); ?>"><input type="button" value="Back To View" class="btn btn-success"></a>
        <input type="reset" name="reset" value="Cancel" class="btn btn-success">
      </div>
    
  </form>
</div>
</div>


