  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title"><b>Data Jenis Kamar</b></h3>
          <a href="<?php echo base_url('index.php/Permintaan/tampil_permintaan') ?>" class="btn btn-success pull-right">Tambah Data</a>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="example2" class="table table-bordered table-hover">
            <thead>
              <tr>
                <th>NOMOR</th>
                <th>KODE_BOOKING</th>
                <th>NAMA_PERMINTAAN</th>
                <th>JUMLAH_PERMINTAAN</th>
              </tr>
            </thead>
            <tbody>
              <?php
              $no = 1;
              foreach ($show as $i) {
                ?>
                <tr>
                  <td><?php echo $no++."."; ?></td>
                  <td><?php echo $i->ID_BOOKING; ?></td>
                  <td><?php echo $i->NAMA_PERMINTAAN; ?></td>
                  <td><?php echo $i->JUMLAH_PERMINTAAN; ?></td>
                  <td>
                    <a href="#" class="btn btn-info btn-xs" onclick="updatejs('<?php echo $i->ID_PERMINTAAN; ?>')">edit</a>
                    <a href="#" class="btn btn-danger btn-xs" onclick="deleted('<?php echo $i->ID_PERMINTAAN; ?>')">delete</a>
                  </td>
                </tr>

                <?php } ?>
              </tbody>
            </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->


      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
    <script type="text/javascript">
      function updatejs(param){
       document.location='<?php echo base_url(); ?>index.php/Permintaan/edit_permintaan/'+param;
     }

     function deleted(param){
      var proc = window.confirm('Anda Yakin Akan Menghapus data ini?');
      if(proc){
        document.location='<?php echo base_url(); ?>index.php/Permintaan/delete_permintaan/'+param;
      }
    }
  </script>
