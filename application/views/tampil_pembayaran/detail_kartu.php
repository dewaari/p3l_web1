<div class="col-xs-12">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title">Form Metode Pembayaran</h3>
		</div>
		<!-- /.box-header -->
		<!-- form start -->
		<form method="post" action="<?php echo base_url('index.php/Pembayaran/tambah_kartu') ?>" role="form" enctype="multipart/form-data">
			<div class="box-body">

				<div id="id_booking" class="box-body col-lg-12" style="margin-bottom: 10px;">
					<input type="text" name="ID_RESERVASI" class="form-control" placeholder="Id Booking" readonly  value="<?=$show?>" />
				</div>

				<div id="nama_pemilik" class="box-body col-lg-12" style="margin-bottom: 10px;">
					<input type="text" class="form-control" name="NAMA_PEMILIK" placeholder="Nama Pemilik">
				</div>

				<div id="nama_bank" class="box-body col-lg-12" style="margin-bottom: 10px;">
					<input type="text" class="form-control" name="NAMA_BANK" placeholder="Nama Bank">
				</div>

				<div id="bukti" class="box-body col-lg-12" style="margin-bottom: 10px;">
					<input type="text" name="NO_KARTU" class="form-control"/>
				</div>

				<div id="tanggal_pembayaran" class="box-body col-lg-12" style="margin-bottom: 10px;">
					<input type="text" name="TANGGAL_PEMBAYARAN" class="form-control" value="<?php echo date("Y-m-d") ?>">
				</div>

				<div id="total" class="box-body col-lg-12" style="margin-bottom: 10px;">
					<input type="text" name="TOTAL_PEMBAYARAN_AWAL" class="form-control" placeholder="Total Biaya Awal" />
				</div> 
			</div>

				<!-- /.box-body -->

				<div class="box-footer">
					<button type="submit" class="btn btn-success">Submit</button>
					<a href="<?php echo base_url('Reservasi/index'); ?>"><input type="button" value="Back To View" class="btn btn-success"></a>
					<input type="reset" name="reset" value="Cancel" class="btn btn-success">
				</div>
			</div>
		</form>
	</div>
</div>