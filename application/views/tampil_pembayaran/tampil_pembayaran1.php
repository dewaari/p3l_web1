  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title"><b>Data Pembayaran</b></h3>
          <!-- <a href="<?php echo base_url('index.php/Pembayaran/tambah') ?>" class="btn btn-success pull-right">Tambah Data</a> -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="example2" class="table table-bordered table-hover">
            <thead>
            <tr>
              <th>NOMOR</th>
              <th>ID BOOKING</th>
              <th>NAMA PEMILIK</th>
              <th>NAMA BANK</th>
              <th>NOMOR KARTU</th>
              <th>TANGGAL PEMBAYARAN</th>
              <th>TOTAL PEMBAYARAN</th>
              <th>BUKTI PEMBAYARAN</th>
            </tr>
            </thead>
            <tbody>
                <?php
                $no = 1;
                  foreach ($show as $i) {
                ?>
                <tr>
                    <td><?php echo $no++."."; ?></td>
                    <td><?php echo $i->ID_BOOKING; ?></td>
                    <td><?php echo $i->NAMA_PEMILIK; ?></td>
                    <td><?php echo $i->NAMA_BANK; ?></td>
                    <td><?php echo $i->NO_KARTU; ?></td>
                    <td><?php echo $i->TANGGAL_PEMBAYARAN; ?></td>
                    <td><?php echo $i->TOTAL_PEMBAYARAN_AWAL; ?></td>
                    <td>
                      <center>
                        <img class="thumbnail" style="height: 100px; width: 100px;" src="<?php echo base_url() ?>assets/images/<?php echo $i->BUKTI_PEMBAYARAN ?>">
                      </center>
                    </td>

                    <td>
                      <a href="#" class="btn btn-info btn-xs" onclick="updatejs('<?php echo $i->ID_PEMBAYARAN; ?>')">edit</a>
                      <a href="#" class="btn btn-danger btn-xs" onclick="deleted('<?php echo $i->ID_PEMBAYARAN; ?>')">delete</a>
                      <a href="#" class="btn btn-danger btn-xs" onclick="verif('<?php echo $i->ID_PEMBAYARAN; ?>')">Verifikasi</a>
                    </td>
                </tr>

              <?php } ?>
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->

      
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
<script type="text/javascript">
  function updatejs(param){
   document.location='<?php echo base_url(); ?>index.php/Pembayaran/edit/'+param;
  }

  function verif(param){
   document.location='<?php echo base_url(); ?>index.php/Verifikasi/index/'+param;
  }
 
  function deleted(param){
    var proc = window.confirm('Anda Yakin Akan Menghapus data ini?');
    if(proc){
      document.location='<?php echo base_url(); ?>index.php/Pembayaran/delete/'+param;
    }
  }
</script>
