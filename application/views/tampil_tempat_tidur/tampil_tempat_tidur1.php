  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title"><b>Data Tempat Tidur</b></h3>
          <a href="<?php echo base_url('index.php/Tempat_Tidur/tampil_tempat_tidur') ?>" class="btn btn-success pull-right">Tambah Data</a>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="example2" class="table table-bordered table-hover">
            <thead>
            <tr>
              <th>NOMOR</th>
              <th>JENIS TEMPAT TIDUR</th>
              <th>HARGA</th>
              <th>AKSI</th>
            </tr>
            </thead>
            <tbody>
                <?php
                $no = 1;
                foreach ($show as $i) {

                     $angka = $i->HARGA;
                     $angka_format = number_format($angka,2,",",".");
                ?>
                <tr>
                    <td><?php echo $no++."."; ?></td>
                    <td><?php echo $i->JENIS_TEMPAT_TIDUR; ?></td>
                    <td>Rp. <?php echo $angka_format; ?></td>
                    <td>
                      <a href="#" class="btn btn-info btn-xs" onclick="updatejs('<?php echo $i->ID_TEMPAT_TIDUR; ?>')">edit</a>
                      <a href="#" class="btn btn-danger btn-xs" onclick="deleted('<?php echo $i->ID_TEMPAT_TIDUR; ?>')">delete</a>
                    </td>
                </tr>

              <?php } ?>
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->

      
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
<script type="text/javascript">
  function updatejs(param){
   document.location='<?php echo base_url(); ?>index.php/Tempat_Tidur/edit_tempat/'+param;
  }
 
  function deleted(param){
    var proc = window.confirm('Anda Yakin Akan Menghapus data ini?');
    if(proc){
      document.location='<?php echo base_url(); ?>index.php/Tempat_Tidur/delete/'+param;
    }
  }
</script>
