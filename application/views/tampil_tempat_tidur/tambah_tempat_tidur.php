<div class="col-md-6">
<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Form Add</h3>
  </div>
  <!-- /.box-header -->
  <!-- form start -->
  <form method="post" action="<?php echo base_url('index.php/Tempat_Tidur/simpan_tempat') ?>">
    <div class="box-body">

      <div class="form-group">
        <label for="exampleInputPassword1">Jenis Tempat Tidur</label>
        <input type="text" pattern="[A-Z a-z]*" title="Inputan Harus Text" class="form-control" id="JENIS_TEMPAT_TIDUR" name="JENIS_TEMPAT_TIDUR" placeholder="Input Tempat_Tidur" required="">
      </div>

      <div class="form-group">
        <label for="exampleInputPassword2">Harga Tempat Tidur</label>
        <input type="text" class="form-control" id="HARGA" name="HARGA" placeholder="Input Harga" required="">
      </div>

    <!-- /.box-body -->
 
      <div class="box-footer">
        <button type="submit" class="btn btn-success">Submit</button>
          <a href="<?php echo base_url('Tempat_Tidur/index'); ?>"><input type="button" value="Back To View" class="btn btn-success"></a>
        <input type="reset" name="reset" value="Cancel" class="btn btn-success">
      </div>

  </form>
</div>
</div>

<script type="text/javascript">
    function validasi(){
      var hargas = $('#HARGA').val().replace(/[^,\d]/g, '').toString();
      var number = /^[0-9]+$/;
      if(!hargas.match(number))
      {
        alert('Harga Harus Angka');
        return false;
      }
    }

    function formatRupiah(angka, prefix)
    {
      var number_string = angka.replace(/[^,\d]/g, '').toString(),
        split = number_string.split(','),
        sisa  = split[0].length % 3,
        rupiah  = split[0].substr(0, sisa),
        ribuan  = split[0].substr(sisa).match(/\d{3}/gi);

      if (ribuan) {
        separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
      }

      rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
      return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }

    var tanpa_tunai = document.getElementById('HARGA');
    tanpa_tunai.addEventListener('keyup', function(e)
    {
      tanpa_tunai.value = formatRupiah(this.value);
    });
  </script>