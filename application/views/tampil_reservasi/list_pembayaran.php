  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title"><b>Data Pembayaran</b></h3>
          <a href="<?php echo base_url('index.php/Pembayaran/tambah') ?>" class="btn btn-success pull-right">Tambah Data</a>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="example2" class="table table-bordered table-hover">
            <thead>
            <tr>
              <th>NOMOR</th>
              <th>NAMA PELANGGAN</th>
              <th>KODE BOOKING</th>
              <th>TANGGAL MASUK</th>
              <th>TANGGAL SELESAI</th>
              <th>STATUS RESERVASI</th>
              <th>TOTAL BAYAR</th>
              <th>UANG JAMINAN</th>
              <!-- <th>PAJAK</th> -->
            </tr>
            </thead>
            <tbody>
                <?php
                $no = 1;
                foreach ($reservasi as $i) {
                      $tanggal1 =  $i->TANGGAL_MASUK;
                      $tanggal2 =  $i->TANGGAL_SELESAI;
                      $format1 = date('d F Y', strtotime($tanggal1 ));
                      $format2 = date('d F Y', strtotime($tanggal2 ));
                      // $tot1 = $r->TOTAL_BIAYA;
                      // $tot2 = $r->UANG_JAMINAN;

                      $angka = $i->TOTAL_BIAYA;
                      $angka_format = number_format($angka,2,",",".");
                      
                      $angka2 = $i->UANG_JAMINAN;
                      $angka_format2 = number_format($angka2,2,",",".");

                ?>
                <tr>
                    <td><?php echo $no++."."; ?></td>
                    <td><?php echo $i->NAMA_PELANGGAN; ?></td>
                    <td><?php echo $i->ID_BOOKING; ?></td>
                    <td><?php echo $tanggal1; ?></td>
                    <td><?php echo $tanggal2; ?></td>
                    <td><?php echo $i->STATUS_RESERVASI; ?></td>
                    <td><?php echo $angka_format; ?></td>
                    <td><?php echo $angka_format2; ?></td>
                    <td>
                      <a href="#" class="btn btn-primary btn-sm" onclick="detailjs('<?php echo $i->ID_RESERVASI; ?>')"><i class="glyphicon glyphicon-list-alt"></i>&nbsp; Detail</a>
                      <a href="#" class="btn btn-success btn-sm" onclick="bayarjs('<?php echo $i->ID_RESERVASI; ?>')"><i class="glyphicon glyphicon-credit-card"></i>&nbsp; Transfer</a>
                      <a href="#" class="btn btn-success btn-sm" onclick="bayarkartujs('<?php echo $i->ID_RESERVASI; ?>')"><i class="glyphicon glyphicon-credit-card"></i>&nbsp; Kartu Kredit</a>
                    </td>
                </tr>

              <?php } ?>
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->

      
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
<script type="text/javascript">
  function updatejs(param){
   document.location='<?php echo base_url(); ?>index.php/Pembayaran/edit/'+param;
  }

   function detailjs(param){
   document.location='<?php echo base_url(); ?>index.php/Reservasi/detail_data_reservasi/'+param;
  }

  function bayarjs(param){
   document.location='<?php echo base_url(); ?>index.php/Pembayaran/detail_transfer/'+param;
  }

  function bayarkartujs(param){
   document.location='<?php echo base_url(); ?>index.php/Pembayaran/detail_kartu/'+param;
  }
 
  function deleted(param){
    var proc = window.confirm('Anda Yakin Akan Menghapus data ini?');
    if(proc){
      document.location='<?php echo base_url(); ?>index.php/Pembayaran/delete/'+param;
    }
  }
</script>
