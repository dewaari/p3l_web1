  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title"><b>Data Reservasi</b></h3>
          <!-- <a href="<?php echo base_url('index.php/Reservasi/tampil_reservasi') ?>" class="glyphicon-plus btn btn-success pull-right">Tambah Data</a> -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
              <?php
              foreach($detail_reservasi as $dr){
                $tanggal1 =  $dr->TANGGAL_MASUK;
                $tanggal2 =  $dr->TANGGAL_SELESAI;
                $format1 = date('d F Y', strtotime($tanggal1 ));
                $format2 = date('d F Y', strtotime($tanggal2 ));

                $angka = $dr->TOTAL_BIAYA;
                $angkajaminan = $dr->UANG_JAMINAN;
                $angka_format = number_format($angka,2,",",".");
                $angka_formatjaminan = number_format($angkajaminan,2,",",".");
              ?>
              <table>
                <tr>
                  <td height="30"><b>Kode Booking</td>
                  <td>&nbsp;&nbsp; : </td>
                  <td>&nbsp;&nbsp; <?php echo $dr->ID_BOOKING ?> </td>
                  <td width="30%"> </td>
                  <td height="30"><b>Jumlah Dewasa</td>
                  <td>&nbsp;&nbsp; : </td>
                  <td>&nbsp;&nbsp; <?php echo $dr->jdewasa ?> Orang  </td>
                </tr>
                <tr>
                  <td height="30"><b>Nama Pelanggan</td>
                  <td>&nbsp;&nbsp; : </td>
                  <td>&nbsp;&nbsp; <?php echo $dr->NAMA_PELANGGAN ?> </td>
                  <td width="30%"> </td>
                  <td height="30"><b>Jumlah Anak</td>
                  <td>&nbsp;&nbsp; : </td>
                  <td>&nbsp;&nbsp; <?php echo $dr->janak ?>  Orang </td>
                </tr>
                <tr>
                  <td height="30"><b>Nama Kota</td>
                  <td>&nbsp;&nbsp; : </td>
                  <td>&nbsp;&nbsp; <?php echo $dr->NAMA_KOTA ?> </td>
                  <td width="30%"> </td>
                  <td height="30"><b>Total Biaya</td>
                  <td>&nbsp;&nbsp; : </td>
                  <td>&nbsp;&nbsp; Rp <?php echo $angka_format ?> </td>
                </tr>
                <tr>
                  <td height="30"><b>Nama Kamar</td>
                  <td>&nbsp;&nbsp; : </td>
                  <td>&nbsp;&nbsp; <?php echo $dr->JENIS_KAMAR ?> </td>
                  <td width="30%"> </td>
                  <td height="30"><b>Uanga Jaminan</td>
                  <td>&nbsp;&nbsp; : </td>
                  <td>&nbsp;&nbsp; Rp <?php echo $angka_format ?> </td>
                </tr>
                <tr>
                  <td height="30"><b>Kode Kamar</td>
                  <td>&nbsp;&nbsp; : </td>
                  <td>&nbsp;&nbsp; <?php echo $dr->KODE_KAMAR ?></td>
                  <td width="30%"> </td>
                  <td height="30"><b>Status Reservasi</td>
                  <td>&nbsp;&nbsp; : </td>
                  <td>&nbsp;&nbsp; <?php echo $dr->STATUS_RESERVASI ?> </td>
                </tr>
                <tr>
                  <td height="30"><b>TANGGAL_MASUK</td>
                  <td>&nbsp;&nbsp; : </td>
                  <td>&nbsp;&nbsp; <?php echo $format1 ?> </td>
                  <td width="30%"> </td>
                  <td height="30"><b>Jenis Tamu</td>
                  <td>&nbsp;&nbsp; : </td>
                  <td>&nbsp;&nbsp; <?php echo $dr->JENIS_TAMU ?> </td>
                </tr>
                <tr>
                  <td height="30"><b>TANGGAL_SELESAI</td>
                  <td>&nbsp;&nbsp; : </td>
                  <td>&nbsp;&nbsp; <?php echo $format2 ?> </td>
                  <td width="30%"> </td>
                  <!-- <td height="30"><b>Permintaan Khusus</td>
                  <td>&nbsp;&nbsp; : </td>
                  <td>&nbsp;&nbsp; <?php echo $dr->permintaan_khusus ?> </td> -->
                </tr>
                
              </table>
              <?php } ?>
              <!-- <br>
              <table id="exampleupdate" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Ruangan</th>
                  <th>Kamar</th>
                  <th>Nama Tamu</th>
                  <th>Jenis Kelamin</th>
                </tr>
                </thead>
                <tbody>
                <?php
                    $no = 1;
                    foreach ($detil_tamu as $dt) {
                ?>
                <tr>
                  <td><?php echo $no++ ?></td>
                  <td><?php echo $dt->nama_ruang ?></td>
                  <td><?php echo $dt->nama_kamar ?></td>
                  <td><?php echo $dt->nama_tamu ?></td>
                  <td><?php echo $dt->jenis_kelamin ?></td>
                </tr>
                <?php } ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>No</th>
                  <th>Ruangan</th>
                  <th>Kamar</th>
                  <th>Nama Tamu</th>
                  <th>Jenis Kelamin</th>
                </tr>
                </tfoot>
              </table>
              <br><br> -->
              <a href="<?php echo base_url('Reservasi/index'); ?>"><input type="button" value="Back To View" class="btn btn-success"></a>
              <!-- <input type="button" name="print" value="Print" class="btn btn-success" onClick="window.print();"> -->
              </div>
            <!-- /.box-body -->
          </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->

      
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
<script type="text/javascript">
  function updatejs(param){
   document.location='<?php echo base_url(); ?>index.php/Pelanggan/edit_reservasi/'+param;
  }

  function detailjs(param){
   document.location='<?php echo base_url(); ?>index.php/Reservasi/detail_data_reservasi/'+param;
  }
 
  function deleted(param){
    var proc = window.confirm('Anda Yakin Akan Menghapus data ini?');
    if(proc){
      document.location='<?php echo base_url(); ?>index.php/Reservasi/delete_reservasi/'+param;
    }
  }
</script>
