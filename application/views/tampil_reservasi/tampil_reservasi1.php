  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title"><b>Data Reservasi</b></h3>
          <!-- <a href="<?php echo base_url('index.php/Reservasi/tampil_reservasi') ?>" class="glyphicon-plus btn btn-success pull-right">Tambah Data</a> -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="example2" class="table table-bordered table-hover">
            <thead>
            <tr>
              <th>NOMOR</th>
              <th>KODE BOOKING</th>
              <!-- <th>NAMA PEGAWAI</th> -->
              <th>NAMA PELANGGAN</th>
              <th>NAMA KOTA</th>
               <th>NAMA KAMAR</th> 
             <th>KODE KAMAR</th> 
              <th>TANGGAL RESERVASI</th>
              <th>JUMLAH DEWASA</th>
              <th>JUMLAH ANAK</th>
              <th>TANGGAL MASUK</th>
              <th>TANGGAL SELESAI</th> 
              <!-- <th>TANGGAL BAYAR</th> -->
              <th>TOTAL_BIAYA</th>
              <th>STATUS RESERVASI</th>
              <!-- <th>NAMA INSTITUSI</th> -->
              <!-- <th>DEPOSIT</th> -->
              <th>UANG JAMINAN</th>
              <th>JENIS TAMU</th>
            </tr>
            </thead>
            <tbody>
                <?php
                $no = 1;
                foreach ($show as $i) {

                    $angka = $i->TOTAL_BIAYA;
                    $angka = $i->UANG_JAMINAN;
                    $angka_format = number_format($angka,2,",",".");
                ?>
                <tr>
                    <td><?php echo $no++."."; ?></td>
                    <td><?php echo $i->ID_BOOKING; ?></td>
                   <!--  <td><?php echo $i->NAMA_PEGAWAI; ?></td> -->
                    <td><?php echo $i->NAMA_PELANGGAN; ?></td>
                    <td><?php echo $i->NAMA_KOTA; ?></td>
                    <td><?php echo $i->JENIS_KAMAR; ?></td>
                    <td><?php echo $i->KODE_KAMAR; ?></td>
                    <td><?php echo $i->TANGGAL_RESERVASI; ?></td>
                    <td><?php echo $i->JUMLAH_DEWASA; ?></td>
                    <td><?php echo $i->JUMLAH_ANAK; ?></td>
                    <td><?php echo $i->TANGGAL_MASUK; ?></td>
                    <td><?php echo $i->TANGGAL_SELESAI; ?></td> 
                   <!--  <td><?php echo $i->TANGGAL_BAYAR; ?></td> -->
                    <td>Rp. <?php echo $angka_format; ?></td>
                    <td><?php echo $i->STATUS_RESERVASI; ?></td>
                   <!--  <td><?php echo $i->NAMA_INSTITUSI; ?></td> -->
                    <!-- <td><?php echo $i->DEPOSIT; ?></td> -->
                    <td>Rp. <?php echo $angka_format; ?></td>
                    <td><?php echo $i->JENIS_TAMU; ?></td>
                    <td>
                      <a href="#" class="glyphicon glyphicon-edit btn btn-info btn-sm" onclick="updatejs('<?php echo $i->ID_RESERVASI; ?>')">Update</a>
                      <a href="#" class="glyphicon glyphicon-remove btn btn-danger btn-sm" onclick="deleted('<?php echo $i->ID_RESERVASI; ?>')">delete</a>
                      <a href="#" class="glyphicon glyphicon-edit btn btn-info btn-sm" onclick="detailjs('<?php echo $i->ID_RESERVASI; ?>')">detail</a>
                    </td>
                </tr>

              <?php } ?>
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->

      
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
<script type="text/javascript">
  function updatejs(param){
   document.location='<?php echo base_url(); ?>index.php/Pelanggan/edit_reservasi/'+param;
  }

  function detailjs(param){
   document.location='<?php echo base_url(); ?>index.php/Reservasi/detail_data_reservasi/'+param;
  }
 
  function deleted(param){
    var proc = window.confirm('Anda Yakin Akan Menghapus data ini?');
    if(proc){
      document.location='<?php echo base_url(); ?>index.php/Reservasi/delete_reservasi/'+param;
    }
  }
</script>
