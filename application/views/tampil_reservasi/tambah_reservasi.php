<div class="col-xs-12">
<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Form Add</h3>
  </div>
  <!-- /.box-header -->
  <!-- form start -->
  <form method="post" action="<?php echo base_url('index.php/Reservasi/simpan_reservasi') ?>">
    <div class="box-body">

      <div class="box-body col-lg-6">
        <div class="form-group">
          <label>ID Booking</label>
          <div class="input-group date">
            <div class="input-group-addon">
              <i class="glyphicon glyphicon-bed" aria-hidden="true"></i>
            </div>
              <input type="text" class="form-control" id="ID_BOOKING" name="ID_BOOKING" value="<?=$ID_BOOKING?>" readonly>
          </div>
        </div>
      </div>

       <div class="box-body col-lg-6">
        <div class="form-group">
          <label>Nama Kota</label>
          <div class="input-group date">
            <div class="input-group-addon">
              <i class="glyphicon glyphicon-bed" aria-hidden="true"></i>
            </div>
             <select class="form-control" id="ID_CABANG" name="ID_CABANG">
                <option value="none" selected="selected">--------------Nama Kota-------------</option>
                    <?php foreach ($tbl_cabang as $c):?>
                      <option value="<?php echo $c->ID_CABANG?>"><?php echo $c->NAMA_KOTA?></option>
                    <?php endforeach;?>
              </select> 
          </div>
        </div>
      </div>

     <div class="box-body col-lg-6">
        <div class="form-group">
          <label>Jenis Kamar</label>
          <div class="input-group date">
            <div class="input-group-addon">
              <i class="glyphicon glyphicon-bed" aria-hidden="true"></i>
            </div>
             <select class="form-control" id="ID_JENIS_KAMAR" name="ID_JENIS_KAMAR">
                <option value="none" selected="selected">--------------Jenis Kamar-------------</option>
                    <?php foreach ($tbl_jenis_kamar as $jk):?>
                      <option value="<?php echo $jk->ID_JENIS_KAMAR?>"><?php echo $jk->JENIS_KAMAR?></option>
                    <?php endforeach;?>
              </select> 
          </div>
        </div>
      </div>

      <!--  <input type="radio" name="alamat" value="berbeda" class="detail"> Tambah kamar<br>
       <input type="radio" name="alamat" value="sama" class="detail"> Tidak<br> -->

      <div class="box-body col-lg-6">
        <div class="form-group">
          <label>Kode Kamar</label>
          <div class="input-group date">
            <div class="input-group-addon">
              <i class="glyphicon glyphicon-bed" aria-hidden="true"></i>
            </div>
               <select class="form-control" id="ID_KAMAR" name="ID_KAMAR">
                  <option value="none" selected="selected">--------------Kode Kamar-------------</option>
                      <?php foreach ($tbl_kamar as $k):?>
                        <option value="<?php echo $k->ID_KAMAR?>"><?php echo $k->KODE_KAMAR?></option>
                      <?php endforeach;?>
                </select> 
          </div>
        </div>
      </div>

       
          <div class="box-body col-lg-12">
            <div class="form-group">
              <label>Nama Pelanggang</label>
              <div class="input-group date">
                <div class="input-group-addon">
                  <i class="glyphicon glyphicon-user" aria-hidden="true"></i>
                </div>
                   <select class="form-control" id="ID_PELANGGAN" name="ID_PELANGGAN">
                      <option value="none" selected="selected">Nama Pelanggan</option>
                          <?php foreach ($tbl_pelanggan as $pl):?>
                            <option value="<?php echo $pl->ID_PELANGGAN?>"><?php echo $pl->NAMA_PELANGGAN?></option>
                          <?php endforeach;?>
                    </select> 
              </div>
            </div>
          </div>
        
      <div class="box-body col-lg-6">
        <div class="form-group">
          <label>Tanggal Reservasi</label>
          <div class="input-group date">
            <div class="input-group-addon">
              <i class="fa fa-calendar"></i>
            </div>
              <input type="date" class="form-control" id="TANGGAL_RESERVASI" name="TANGGAL_RESERVASI" value="<?php echo date("Y-m-d") ?>" readonly>
          </div>
        </div>
      </div>

     <!--  <div class="form-group">
        <label for="exampleInputPassword1">Jumlah Dewasa</label>
        <input type="text" class="form-control" id="JUMLAH_DEWASA" name="JUMLAH_DEWASA" placeholder="Your Alamat">
      </div> -->

      <div class="box-body col-lg-6">
        <div class="form-group">
          <label>Jumlah Dewasa</label>
          <div class="input-group date">
            <div class="input-group-addon">
              <i class="glyphicon glyphicon-user" aria-hidden="true"></i>
            </div>
            <select class="form-control" id="JUMLAH_DEWASA" name="JUMLAH_DEWASA">
              <option value="none" selected="selected">Jumlah Anak</option>
              <option>1</option>
              <option>2</option>
              <option>3</option>
              <option>4</option>
              <option>5</option>
              <option>6</option>
              <option>7</option>
              <option>8</option>
              <option>9</option>
              <option>10</option>
            </select>
          </div>
        </div>
      </div>

      <div class="box-body col-lg-6">
        <div class="form-group">
          <label>Jumlah Anak</label>
          <div class="input-group date">
            <div class="input-group-addon">
              <i class="glyphicon glyphicon-user" aria-hidden="true"></i>
            </div>
            <select class="form-control" id="JUMLAH_ANAK" name="JUMLAH_ANAK">
              <option value="none" selected="selected">Jumlah Anak</option>
              <option>1</option>
              <option>2</option>
              <option>3</option>
              <option>4</option>
              <option>5</option>
            </select>
          </div>
        </div>
      </div>

      <div class="box-body col-lg-6">
        <div class="form-group">
          <label>Tanggal Mulai</label>
          <div class="input-group date">
            <div class="input-group-addon">
              <i class="fa fa-calendar"></i>
            </div>
            <input type="text" name="TANGGAL_MASUK" class="form-control pull-right field-check-form1 tgl_checkin" id="datepicker1a" placeholder="Tanggal Mulai... " required>
          </div>
        </div>
      </div>

      <div class="box-body col-lg-6">
        <div class="form-group">
          <label>Tanggal Selesai</label>
          <div class="input-group date">
            <div class="input-group-addon">
              <i class="fa fa-calendar"></i>
            </div>
            <input type="text" name="TANGGAL_SELESAI" class="form-control pull-right field-check-form1 tgl_checkout" id="datepicker2a" placeholder="Tanggal Selesai ... " required>
          </div>
        </div>
      </div>


     <div class="box-body col-lg-6">
        <div class="form-group">
          <label>Total Biaya</label>
          <div class="input-group date">
            <div class="input-group-addon">
              <i class="fa fa-money"></i>
            </div>
              <input type="text" class="form-control" id="TOTAL_BIAYA" name="TOTAL_BIAYA" placeholder="Your Alamat">
          </div>
        </div>
      </div>

      <div class="box-body col-lg-6">
          <div class="form-group">
            <label>Uang Jaminan</label>
            <div class="input-group date">
              <div class="input-group-addon">
                <i class="fa fa-money"></i>
              </div>
                <input type="text" class="form-control" id="UANG_JAMINAN" name="UANG_JAMINAN" placeholder="Your Alamat">
            </div>
          </div>
      </div>
      

      <div class="box-body col-lg-6">
        <div class="form-group">
          <label>Status Reservasi</label>
          <div class="input-group date">
            <div class="input-group-addon">
              <i class="fa fa-calendar"></i>
            </div>
              <select class="form-control" id="STATUS_RESERVASI" name="STATUS_RESERVASI">
                <option value="none" selected="selected">--------------Status Reservasi-------------</option>
                <option>Aktive</option>
                <option>Tidak Aktive</option>
              </select>
          </div>
        </div>
      </div>
      

     <div class="box-body col-lg-6">
        <div class="form-group">
          <label>Jenis Tamu</label>
          <div class="input-group date">
            <div class="input-group-addon">
              <i class="fa fa-calendar"></i>
            </div>
            <select class="form-control" id="JENIS_TAMU" name="JENIS_TAMU">
              <option value="none" selected="selected">--------------Jenis Tamu-------------</option>
              <option>Personal</option>
              <option>Group</option>
            </select>
          </div>
        </div>
      </div>
      
    </div>
    <!-- /.box-body -->
 
    <div class="box-footer">
        <button type="submit" class="btn btn-success">Submit</button>
          <a href="<?php echo base_url('Reservasi/index'); ?>"><input type="button" value="Back To View" class="btn btn-success"></a>
        <input type="reset" name="reset" value="Cancel" class="btn btn-success">
      </div>
  </form>
</div>
</div>

<script type="text/javascript">
    $(function () {
        $('#datepicker1a').datepicker();
        $('#datepicker2a').datepicker({
            useCurrent: false //Important! See issue #1075
        });
        $("#datepicker1a").on("dp.change", function (e) {
            $('#datepicker2a').data("DatePicker").minDate(e.date);
        });
        $("#datepicker2a").on("dp.change", function (e) {
            $('#datepicker1a').data("DatePicker").maxDate(e.date);
        });
    });
</script>




<!-- <script>
$(document).ready(function(){
$("#form-input").css("display","none"); //Menghilangkan form-input ketika pertama kali dijalankan
$(".detail").click(function(){ //Memberikan even ketika class detail di klik (class detail ialah class radio button)
if ($("input[name='alamat']:checked").val() == "berbeda" ) { //Jika radio button "berbeda" dipilih maka tampilkan form-inputan
$("#form-input").slideDown("fast"); //Efek Slide Down (Menampilkan Form Input)
} else {
$("#form-input").slideUp("fast"); //Efek Slide Up (Menghilangkan Form Input)
}
});
});
</script> -->


