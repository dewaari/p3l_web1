<div class="col-md-6">
  <div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Form Add</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <form method="post" action="<?php echo base_url('index.php/Sesion/update_data_sesion') ?>">
      <div class="box-body">
        <?php foreach ($show as $key ) { ?>

        <div class="form-group">
          <label for="exampleInputEmail1">ID Sesion</label>
          <input type="text" class="form-control" id="ID_SESION" name="ID_SESION" value="<?php echo $key->ID_SESION; ?>" readonly>
        </div>

        <div class="form-group">
          <label for="exampleInputEmail1">Jenis Kamar</label>
          <select class="form-control" id="ID_JENIS_KAMAR" name="ID_JENIS_KAMAR">
            <option value="0" selected="selected" disabled="disabled">Jenis Kamar</option>
            <?php foreach ($tbl_jenis_kamar as $j):?>
              <option value="<?php echo $j->ID_JENIS_KAMAR?>" <?= $key->ID_JENIS_KAMAR == $j->ID_JENIS_KAMAR ? 'selected' : null ?>><?php echo $j->JENIS_KAMAR?></option>
            <?php endforeach;?>
          </select> 
        </div>

        <div class="form-group">
          <label class="control_label" for="NAMA_SESION">Nama Sesion</label>
          <select class="form-control" id="NAMA_SESION" name="NAMA_SESION">
            <option value="0" selected="selected" disabled="disabled">Nama Sesion</option>
            <option>Promo</option>
            <option>High Sesion</option>
          </select>
        </div>

        <div class="box-body col-lg-6">
        <div class="form-group">
          <label>Tanggal Mulai</label>
          <div class="input-group date">
            <div class="input-group-addon">
              <i class="fa fa-calendar"></i>
            </div>
            <input type="text" name="TANGGAL_MULAI" class="form-control pull-right field-check-form1 tgl_checkin" id="datepicker1" placeholder="Tanggal Mulai... " required>
          </div>
        </div>
      </div>

      <div class="box-body col-lg-6">
        <div class="form-group">
          <label>Tanggal Selesai</label>
          <div class="input-group date">
            <div class="input-group-addon">
              <i class="fa fa-calendar"></i>
            </div>
            <input type="text" name="TANGGAL_SELESAI" class="form-control pull-right field-check-form1 tgl_checkout" id="datepicker2" placeholder="Tanggal Selesai ... " required>
          </div>
        </div>
      </div>

       
      <!-- <div class="form-group">
        <label for="exampleInputPassword1">Tanggal Mulai</label>
        <input type="date" class="form-control" id="datepicker1" name="TANGGAL_MULAI" value="<?php echo isset($itemOutData->TANGGAL_MULAI) ? set_value('TANGGAL_MULAI', date('Y-m-d', strtotime($this->input->post('TANGGAL_MULAI')))) : set_value('TANGGAL_MULAI'); ?>">
      </div>

      <div class="form-group">
        <label for="exampleInputPassword1">Tanggal Selesai</label>
        <input type="date" class="form-control" id="datepicker2" name="TANGGAL_SELESAI" value="<?php echo isset($itemOutData->TANGGAL_SELESAI) ? set_value('TANGGAL_SELESAI', date('Y-m-d', strtotime($this->input->post('TANGGAL_SELESAI')))) : set_value('TANGGAL_SELESAI'); ?>">
      </div> -->

      <div class="form-group">
        <label for="exampleInputPassword1">Harga Sesion</label>
        <input type="text" class="form-control" id="HARGA_SESION" name="HARGA_SESION" placeholder="Your Address" value="<?php echo $key->HARGA_SESION; ?>">
      </div>

      <div class="form-group">
        <label class="control_label" for="STATUS_SESION">STATUS</label>
        <select class="form-control" id="STATUS_SESION" name="STATUS_SESION">
          <option value="none" selected="selected">--------------Status Sesion-------------</option>
          <option>Aktive</option>
          <option>Tidak Aktive</option>
        </select>
      </div>

    <?php } ?>
  </div>
  <!-- /.box-body -->

  <div class="box-footer">
    <button type="submit" class="btn btn-success">Submit</button>
    <a href="<?php echo base_url('Sesion/index'); ?>"><input type="button" value="Back To View" class="btn btn-success"></a>
    <input type="reset" name="reset" value="Cancel" class="btn btn-success">
  </div>

</form>
</div>
</div>

<script type="text/javascript">
    $(function () {
        $('#datepicker1').datepicker();
        $('#datepicker2').datepicker({
            useCurrent: false //Important! See issue #1075
        });
        $("#datepicker1").on("dp.change", function (e) {
            $('#datepicker2').data("DatePicker").minDate(e.date);
        });
        $("#datepicker2").on("dp.change", function (e) {
            $('#datepicker1').data("DatePicker").maxDate(e.date);
        });
    });
</script>

<script type="text/javascript">
    function validasi(){
      var hargas = $('#HARGA_SESION').val().replace(/[^,\d]/g, '').toString();
      var number = /^[0-9]+$/;
      if(!hargas.match(number))
      {
        alert('Harga Harus Angka');
        return false;
      }
    }

    function formatRupiah(angka, prefix)
    {
      var number_string = angka.replace(/[^,\d]/g, '').toString(),
        split = number_string.split(','),
        sisa  = split[0].length % 3,
        rupiah  = split[0].substr(0, sisa),
        ribuan  = split[0].substr(sisa).match(/\d{3}/gi);

      if (ribuan) {
        separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
      }

      rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
      return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }

    var tanpa_tunai = document.getElementById('HARGA_SESION');
    tanpa_tunai.addEventListener('keyup', function(e)
    {
      tanpa_tunai.value = formatRupiah(this.value);
    });
  </script>

