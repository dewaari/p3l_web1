  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title"><b>Data Sesion</b></h3>
          <a href="<?php echo base_url('index.php/Sesion/tampil_sesion') ?>" class="btn btn-success pull-right">Tambah Data</a>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="example2" class="table table-bordered table-hover">
            <thead>
            <tr>
              <th>NOMOR</th>
              <th>JENIS KAMAR</th>
              <th>NAMA SESION</th>
              <th>TANGGAL MULAI</th>
              <th>TANGGAL SELESAI</th>
              <th>HARGA SESION</th>
              <th>STATUS SESION</th>
              <th>AKSI</th>

            </tr>
            </thead>
            <tbody>
                <?php
                $no = 1;
                foreach ($show as $i) {

                    $angka = $i->HARGA_SESION;
                    $angka_format = number_format($angka,2,",",".");
                ?>
                <tr>
                    <td><?php echo $no++."."; ?></td>
                    <td><?php echo $i->JENIS_KAMAR; ?></td>
                    <td><?php echo $i->NAMA_SESION; ?></td>
                    <td><?php echo $i->TANGGAL_MULAI; ?></td>
                    <td><?php echo $i->TANGGAL_SELESAI; ?></td>
                    <td>Rp. <?php echo $angka_format; ?></td>
                    <td><?php echo $i->STATUS_SESION; ?></td>
                    <td>
                      <a href="#" class="btn btn-info btn-xs" onclick="updatejs('<?php echo $i->ID_SESION; ?>')">edit</a>
                      <a href="#" class="btn btn-danger btn-xs" onclick="deleted('<?php echo $i->ID_SESION; ?>')">delete</a>
                    </td>
                </tr>

              <?php } ?>
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->

      
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
<script type="text/javascript">
  function updatejs(param){
   document.location='<?php echo base_url(); ?>index.php/Sesion/edit_sesion/'+param;
  }
 
  function deleted(param){
    var proc = window.confirm('Anda Yakin Akan Menghapus data ini?');
    if(proc){
      document.location='<?php echo base_url(); ?>index.php/Sesion/delete_sesion/'+param;
    }
  }
</script>
