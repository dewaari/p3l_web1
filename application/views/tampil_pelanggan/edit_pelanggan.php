<div class="col-md-6">
  <div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Form Add</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <form method="post" action="<?php echo base_url('index.php/Pelanggan/update_data_pelanggan') ?>">
      <div class="box-body">
        <?php foreach ($show as $key ) { ?>

              <div class="form-group">
                <label for="exampleInputEmail1">ID Pelanggan</label>
                <input type="text" class="form-control" id="ID_PELANGGAN" name="ID_PELANGGAN" value="<?php echo $key->ID_PELANGGAN; ?>" readonly>
              </div> 

              <div class="form-group">
                <label for="exampleInputPassword1">Nama Pelanggan</label>
                <input type="text" pattern="[A-Z a-z]*" title="Inputan Nama Kota Harus Text" class="form-control" id="NAMA_PELANGGAN" name="NAMA_PELANGGAN" placeholder="Nama Pelanggan" value="<?php echo $key->NAMA_PELANGGAN; ?>" required="/required" >
              </div>

              <div class="form-group">
                <label for="exampleInputPassword1">Email Pelanggan</label>
                <input type="email" class="form-control" id="EMAIL_PELANGGAN" name="EMAIL_PELANGGAN" placeholder="Email Pelanggan" value="<?php echo $key->EMAIL_PELANGGAN; ?>" required="/required" >
              </div>

              <div class="form-group">
                <label for="exampleInputPassword1">Password</label>
                <input type="password" class="form-control" id="PASSWORD" name="PASSWORD" placeholder="Password" value="<?php echo $key->PASSWORD; ?>" required="/required">
              </div>

               <div class="form-group">
                  <label for="exampleFormControlTextarea1">Alamat Pelanggan</label>
                  <textarea type="text" value="<?php echo $key->ALAMAT; ?>" placeholder="Alamat Pelanggan" class="form-control" id="ALAMAT" name="ALAMAT" rows="3" required="/required"></textarea>
               </div>

              <div class="form-group">
                <label for="exampleInputPassword1">No Telepon</label>
                <input type="text" class="form-control" id="NO_TELEPON_PELANGGAN" name="NO_TELEPON_PELANGGAN" placeholder="Nomor Telepon" value="<?php echo $key->NO_TELEPON_PELANGGAN; ?>" required="/required">
              </div>

              <div class="form-group">
                <label for="exampleInputPassword1">No Identitas</label>
                <input type="text" class="form-control" id="NO_IDENTITAS" name="NO_IDENTITAS" placeholder="No Identitas" value="<?php echo $key->NO_IDENTITAS; ?>" required="/required">
              </div>

              <div class="form-group">
                <label for="exampleInputPassword1">Tanggal Regristrasi</label>
                <input type="date" class="form-control" id="TANGGAL_PENDAFTARAN" name="TANGGAL_PENDAFTARAN" value="<?php echo date("Y-m-d");?>" readonly>
              </div> 

              <div class="form-group">
                <label for="exampleInputEmail1">Nama Peran</label>
                 <select class="form-control" id="ID_PERAN" name="ID_PERAN">
                    <option value="none" selected="selected" disabled="disabled">Pilih Peran</option>
                        <?php foreach ($tbl_peran as $p):?>
                          <option value="<?php echo $p->ID_PERAN?>" <?= $key->ID_PERAN == $p->ID_PERAN ? 'selected' : null ?> ><?php echo $p->NAMA_PERAN?></option>
                        <?php endforeach;?>
                  </select> 
              </div>
         <?php } ?>
      </div>
      <!-- /.box-body -->

       <div class="box-footer">
        <button type="submit" class="btn btn-success">Submit</button>
          <a href="<?php echo base_url('Pelanggan/index'); ?>"><input type="button" value="Back To View" class="btn btn-success"></a>
        <input type="reset" name="reset" value="Cancel" class="btn btn-success">
      </div>

    </form>
  </div>
</div>


<script type="text/javascript">
  function validasi(){
    var notelepon = document.getElementById('NO_TELEPON_PELANGGAN').value;
    var noidentitas = document.getElementById('NO_IDENTITAS').value;
    var mail = document.getElementById('EMAIL_PELANGGAN').value;
    var number = /^[0-9]+$/;
    var re = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

    if(!mail.match(re))
    {
      alert('Email Tidak Valid');
      return false;
    }

    if(!notelepon.match(number))
    {
      alert('No Telphone Harus Angka');
      return false;
    }

    if(notelepon.length >= 13)
    {
      alert('No Telphone Harus 12 digit');
      return false;
    }

    if(!noidentitas.match(number))
    {
      alert('No Identitas Harus Angka');
      return false;
    }

    if(noidentitas.length >= 17)
    {
      alert('No Identitas Harus 12 digit');
      return false;
    }

    return true;
  }
</script>