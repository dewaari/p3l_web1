  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title"><b>Data Pelanggan</b></h3>
          <a href="<?php echo base_url('index.php/Pelanggan/tampil_pelanggan') ?>" class="btn btn-success pull-right">Tambah Data</a>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="example2" class="table table-bordered table-hover">
            <thead>
            <tr>
              <th>NOMOR</th>
              <th>NAMA PELANGGAN</th>
              <th>EMAIL PELANGGAN</th>
              <th>ALAMAT</th>
              <th>NO TELEPON PELANGGAN</th>
              <th>NO IDENTITAS PELANGGAN</th>
              <th>PASSWORD</th>
              <th>TANGGAL REGRISTRASI</th>
              <th>NAMA PERAN</th>
            </tr>
            </thead>
            <tbody>
                <?php
                $no = 1;
                foreach ($show as $i) {
                ?>
                <tr>
                    <td><?php echo $no++."."; ?></td>
                    <td><?php echo $i->NAMA_PELANGGAN; ?></td>
                    <td><?php echo $i->EMAIL_PELANGGAN; ?></td>
                    <td><?php echo $i->ALAMAT; ?></td>
                    <td><?php echo $i->NO_TELEPON_PELANGGAN; ?></td>
                    <td><?php echo $i->NO_IDENTITAS; ?></td>
                    <td><?php echo $i->PASSWORD; ?></td>
                    <td><?php echo $i->TANGGAL_PENDAFTARAN; ?></td>
                    <td><?php echo $i->NAMA_PERAN; ?></td>
                    <td>
                      <a href="#" class="btn btn-info btn-xs" onclick="updatejs('<?php echo $i->ID_PELANGGAN; ?>')">edit</a>
                      <a href="#" class="btn btn-danger btn-xs" onclick="deleted('<?php echo $i->ID_PELANGGAN; ?>')">delete</a>
                    </td>
                </tr>

              <?php } ?>
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->

      
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
<script type="text/javascript">
  function updatejs(param){
   document.location='<?php echo base_url(); ?>index.php/Pelanggan/edit_pelanggan/'+param;
  }
 
  function deleted(param){
    var proc = window.confirm('Anda Yakin Akan Menghapus data ini?');
    if(proc){
      document.location='<?php echo base_url(); ?>index.php/Pelanggan/delete_pelanggan/'+param;
    }
  }
</script>
