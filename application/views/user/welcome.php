<?php include "main-header.php" ;?>

  	<header id="fh5co-header" class="fh5co-cover" role="banner" style="background-image: url(assets/frontend/images/tugu.jpg);">
  		<div class="overlay"></div>
  		<div class="container">
  			<div class="row">
  				<div class="col-md-9 text-left">
  					<div class="display-t">
  						<div class="display-tc animate-box" data-animate-effect="fadeInUp">
  							<h1 class="mb30">Selamat Datang Di Grand Atma Hotels</h1>
  							<p>
  								<a data-toggle="modal" data-target=".bs-example-modal-lg" class="btn btn-primary">Booking Now</a>
  							</p>
  						</div>
  					</div>
  				</div>
  			</div>
  		</div>
  	</header>
  	<div id="fh5co-project">
  		<div class="container">
  			<div class="row row-pb-md">
  				<div class="col-md-12 text-center fh5co-heading  animate-box">
  					<h2 align="center">Grand Atma Hotels</h2>
  					<hr>
  					<p style="font-size: 14px;">
              
              <br><br>
              Visi :<br>
              Rumah Retret RPCB Syantikara merupakan tempat pembinaan dan tempat doa yang memberikan pelayanan<br>
              dalam semangat kasih, persaudaraan, keramahan, kesiapsediaan dan kesederhanaan.<br><br>

              Misi :<br>
              Melayani pribadi/kelompok yang membutuhkan sarana rohani seperti retret, rekoleksi dan ziarah.<br>
              Melayani kelompok-kelompok yang melaksanakan kegiatan pembinaan dan penyegaran.<br>
              Melayani pribadi/kelompok yang membutuhkan tempat dan suasana yang nyaman untuk pembuatan program dan penyelenggaraan raker.<br>
  					</p>
  				</div>
  			</div>
  		</div>
  	</div>
  	<!-- <div id="fh5co-blog" class="fh5co-bg-section">
  		<div class="container" style="margin-top: -50px;">
  			<div class="row animate-box row-pb-md" data-animate-effect="fadeInUp">
  				<div class="col-md-12 text-center fh5co-heading">
  					<h2 align="center">NEWS UPDATE</h2>
  					<hr>
  					<p style="font-size: 14px;">The most updated news, activities, article, nor information from Yayasan RPCB Syantikara is here. Check It Out.</p>
  				</div>
  			</div>
  			<div class="row">
          <?php
              foreach ($berita as $b)
              {
                $tanggal1 =  $b->tgl_publish;
                $format1 = date('d F Y', strtotime($tanggal1 ));
          ?>
  				<div class="col-md-4 col-sm-4 animate-box" data-animate-effect="fadeInUp">
  					<div class="fh5co-post">
  						<span class="fh5co-date"><?php echo $format1; ?></span>
  						<h3><a href="<?php echo base_url('news/detail/'.$b->id_news); ?>"><?php echo $b->title; ?></a></h3>
  						<p style="font-size:13px;"><?php echo $b->deskripsi_singkat; ?></p>
  						<p class="author"><img src="assets/dashboard/dist/img/<?php echo $b->imgpp; ?>"> <cite> Posted By : <?php echo $b->name; ?></cite></p>
  					</div>
  				</div>
  				<?php } ?>
  			</div>
        <p align="center"><a href="<?php echo base_url('news'); ?>" class="btn btn-primary btn-flat" style="padding-top: 10px; margin-bottom: -100px;">Show All The News</a></p>
  		</div>
  	</div> -->
  	<div id="fh5co-counter">
  		<div class="container">
  			<div class="row animate-box" data-animate-effect="fadeInUp">
  				<div class="col-md-12 text-center fh5co-heading">
  					<h2 align="center">Facts</h2>
  					<hr>
  					<p style="font-size: 14px;">Some facts occurred in Yayasan RPCB Syantikara. Check It out.</p>
  				</div>
  			</div>
  			<div class="row">
  				<div class="col-md-3 col-sm-6 animate-box" data-animate-effect="fadeInLeft">
  					<div class="feature-center">
  						<span class="icon">
  							<i class="ti-download"></i>
  						</span>
  						<span class="counter"><span class="js-counter" data-from="0" data-to="15" data-speed="1500" data-refresh-interval="50">1</span>+</span>
  						<span class="counter-label">Mitra</span>

  					</div>
  				</div>
  				<div class="col-md-3 col-sm-6 animate-box" data-animate-effect="fadeInLeft">
  					<div class="feature-center">
  							<span class="icon">
  								<i class="ti-face-smile"></i>
  							</span>
  							<span class="counter"><span class="js-counter" data-from="0" data-to="500" data-speed="1500" data-refresh-interval="50">1</span>+</span>
  							<span class="counter-label">Happy Clients</span>
  						</div>
  					</div>
  					<div class="col-md-3 col-sm-6 animate-box" data-animate-effect="fadeInLeft">
  						<div class="feature-center">
  							<span class="icon">
  								<i class="ti-briefcase"></i>
  							</span>
  							<span class="counter"><span class=" js-counter" data-from="0" data-to="90" data-speed="1500" data-refresh-interval="50">1</span>+</span>
  							<span class="counter-label">Workers</span>
  						</div>
  					</div>
  					<div class="col-md-3 col-sm-6 animate-box" data-animate-effect="fadeInLeft">
  						<div class="feature-center">
  							<span class="icon">
  								<i class="ti-time"></i>
  							</span>
  							<span class="counter"><span class="js-counter" data-from="0" data-to="150" data-speed="1500" data-refresh-interval="50">1</span>+</span>
  							<span class="counter-label">Reservation</span>

  						</div>
  					</div>

  				</div>
  		  </div>
  	</div>
  	<div id="fh5co-testimonial" class="fh5co-bg-section" >
  		<div class="container" style="margin-top: -50px;">
  			<div class="row animate-box" data-animate-effect="fadeInUp">
  				<div class="col-md-12 text-center fh5co-heading">
  					<h2 align="center">Room And Rates</h2>
  					<hr>
  					<p style="font-size: 14px;">Some Testimonial from Syantikara's Mitra and Syantikara's Customers. Check It Out.</p>
  				</div>
  			</div>
  				<div class="priceing-table-main">
				 <div class="col-md-3 price-grid">
					<div class="price-block agile">
						<div class="price-gd-top">
              <div class="content">
                <img src="assets/frontend/images/r1.jpg" alt=" " class="img-responsive" />
              <h4>Deluxe Room</h4>
              </div>
						
						</div>
						<!-- <div class="price-gd-bottom">
							   <div class="price-list">
									<ul>
											<li><i class="fa fa-star" aria-hidden="true"></i></li>
											<li><i class="fa fa-star" aria-hidden="true"></i></li>
											<li><i class="fa fa-star" aria-hidden="true"></i></li>
											<li><i class="fa fa-star" aria-hidden="true"></i></li>
											<li><i class="fa fa-star-o" aria-hidden="true"></i></li>
									
								     </ul>
							</div> -->
							<div class="price-selet">	
								<h3><span>Rp</span>320</h3>						
								<p><a data-toggle="modal" data-target=".bs-example-modal-lg" class="btn btn-primary" style="padding-top: 15px;">Booking Now</a></p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-3 price-grid ">
					<div class="price-block agile">
						<div class="price-gd-top">
						<img src="assets/frontend/images/r2.jpg" alt=" " class="img-responsive" />
							<h4>Luxury Room</h4>
						</div>
						<div class="price-gd-bottom">
							<!-- <div class="price-list">
									<ul>
									<li><i class="fa fa-star" aria-hidden="true"></i></li>
									<li><i class="fa fa-star" aria-hidden="true"></i></li>
									<li><i class="fa fa-star" aria-hidden="true"></i></li>
									<li><i class="fa fa-star" aria-hidden="true"></i></li>
									<li><i class="fa fa-star-o" aria-hidden="true"></i></li>
								</ul>
							</div> -->
							<div class="price-selet">
								<h3><span>Rp</span>220</h3>
								<p><a data-toggle="modal" data-target=".bs-example-modal-lg" class="btn btn-primary" style="padding-top: 15px;">Booking Now</a></p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-3 price-grid lost">
					<div class="price-block agile">
						<div class="price-gd-top">
						<img src="assets/frontend/images/r3.jpg" alt=" " class="img-responsive" />
							<h4>Guest House</h4>
						</div>
						<div class="price-gd-bottom">
							<!-- <div class="price-list">
								<ul>
									<li><i class="fa fa-star" aria-hidden="true"></i></li>
									<li><i class="fa fa-star" aria-hidden="true"></i></li>
									<li><i class="fa fa-star" aria-hidden="true"></i></li>
									<li><i class="fa fa-star-o" aria-hidden="true"></i></li>
									<li><i class="fa fa-star-o" aria-hidden="true"></i></li>
								</ul>
							</div> -->
							<div class="price-selet">
								<h3><span>Rp</span>180</h3>
								<p><a data-toggle="modal" data-target=".bs-example-modal-lg" class="btn btn-primary" style="padding-top: 15px;">Booking Now</a></p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-3 price-grid wthree lost">
					<div class="price-block agile">
						<div class="price-gd-top ">
							<img src="assets/frontend/images/r4.jpg" alt=" " class="img-responsive" />
							<h4>Single Room</h4>
						</div>
						<div class="price-gd-bottom">
							<!-- <div class="price-list">
								<ul>
									<li><i class="fa fa-star" aria-hidden="true"></i></li>
									<li><i class="fa fa-star" aria-hidden="true"></i></li>
									<li><i class="fa fa-star-o" aria-hidden="true"></i></li>
									<li><i class="fa fa-star-o" aria-hidden="true"></i></li>
									<li><i class="fa fa-star-o" aria-hidden="true"></i></li>
								</ul>
							</div> -->
							<div class="price-selet">
								<h3><span>Rp</span> 150</h3>
								<p><a data-toggle="modal" data-target=".bs-example-modal-lg" class="btn btn-primary" style="padding-top: 15px;">Booking Now</a></p>
							</div>
						</div>
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
  		</div>
  	</div>
  	<div id="fh5co-started">
  		<div class="container">
  			<div class="row animate-box">
  				<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
  					<h2>Are You Interesting ?</h2>
  					<p>Order now before they run out and get the special price for active member or mitra.</p>
  					<p><a data-toggle="modal" data-target=".bs-example-modal-lg" class="btn btn-primary" style="padding-top: 15px;">Booking Now</a></p>
  				</div>
  			</div>
  		</div>
  	</div>



  <?php include "main-footer.php"; ?>
