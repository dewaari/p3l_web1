
<!-------------------------------------------------------------------------------------------------------------------->

<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
<div class="modal-dialog modal-lg" role="document">
  <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel">BOOK GRAND ATMA HOTELS</h4>
      </div>
      <div class="modal-body">
      <p style="margin-bottom:20px;"></p>
      <form>
          <table>
            <tr height="100px">
              <td width="450px">
                <label for="recipient-name" class="control-label">Check In:</label>
                <input type="text" class="form-control" id="recipient-name">
              </td>
              <td width="100px">
              </td>
              <td width="450px">
                <label for="recipient-name" class="control-label">Check Out:</label>
                <input type="text" class="form-control" id="recipient-name">
              </td>
            </tr>
            <tr>
              <td width="450px">
                <label for="recipient-name" class="control-label">Rooms:</label>
                  <input type="number" name="room" id="room" class="form-control" value="0" required>
              </td>
              <td width="100px">
              </td>
              <td width="450px">
                <label for="recipient-name" class="control-label">Guest:</label>
                <input type="number" name="guest" id="guest" class="form-control" value="0" required>
              </td>
            </tr>
          </table>
        </form>
        <p style="margin-bottom:50px;"></p>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Book Now</button>
        </div>
      </div>
  </div>
</div>
</div>

<!-------------------------------------------------------------------------------------------------------------------->
<script>
      $('#myModal').on('shown.bs.modal', function () {
          $('#myInput').focus()
      })
</script>
<!-- jQuery -->
<script src="assets/frontend/js/jquery.min.js"></script>
<!-- jQuery Easing -->
<script src="assets/frontend/js/jquery.easing.1.3.js"></script>
<!-- Bootstrap -->
<script src="assets/frontend/js/bootstrap.min.js"></script>
<!-- Waypoints -->
<script src="assets/frontend/js/jquery.waypoints.min.js"></script>
<!-- countTo -->
<script src="assets/frontend/js/jquery.countTo.js"></script>
<!-- Magnific Popup -->
<script src="assets/frontend/js/jquery.magnific-popup.min.js"></script>
<script src="assets/frontend/js/magnific-popup-options.js"></script>
<!-- Stellar -->
<script src="assets/frontend/js/jquery.stellar.min.js"></script>
<!-- Main -->
<script src="assets/frontend/js/main.js"></script>
<!-- Lightbox -->
<script src="assets/frontend/lightbox/dist/js/lightbox.js"></script>

<!-- DataTables -->
<script src="assets/dashboard/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="assets/dashboard/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(function () {
  //  $("#example1").DataTable();
    $('#example1').DataTable({
      "responsive": true,
      "paging": false,
      "lengthChange": true,
      "searching": true,
      "ordering": false,
      "info": false,
      "autoWidth": false
    });
  });
</script>
</body>
</html>
