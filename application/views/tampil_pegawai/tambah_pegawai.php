<div class="col-md-6">
<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Form Add</h3>
  </div>
  <!-- /.box-header -->
  <!-- form start -->
  <form method="post" action="<?php echo base_url('index.php/Pegawai/simpan_pegawai') ?>">
    <div class="box-body">

      <div class="form-group">
        <label for="exampleInputEmail1">Nama Kota</label>
         <select class="form-control" id="ID_CABANG" name="ID_CABANG">
            <option value="0" selected="selected" disabled="disabled">Nama Kota</option>
                <?php foreach ($tbl_cabang as $c):?>
                  <option value="<?php echo $c->ID_CABANG?>"><?php echo $c->NAMA_KOTA?></option>
                <?php endforeach;?>
          </select> 
      </div>

      <div class="form-group">
        <label for="exampleInputPassword1">Nama Pegawai</label>
        <input type="text" pattern="[A-Z a-z]*" title="Inputan Nama Pegawai Harus Text" class="form-control" id="NAMA_PEGAWAI" name="NAMA_PEGAWAI" placeholder="Input Nama Pegawai" required="/required">
      </div>

      <div class="form-group">
        <label for="exampleInputPassword2">Email Pegawai</label>
        <input type="email" class="form-control" id="EMAIL_PEGAWAI" name="EMAIL_PEGAWAI" placeholder="Input Email Pegawai" required="required">
      </div>

      <div class="form-group">
        <label for="exampleInputPassword1">Password</label>
        <input type="password" class="form-control" id="PASSWORD" name="PASSWORD" placeholder="Input Password" required="/required">
      </div>

      <div class="form-group">
          <label for="exampleFormControlTextarea1">Alamat</label>
          <textarea type="text" class="form-control" id="ALAMAT" name="ALAMAT" rows="3" required="/required"></textarea>
        </div>

      <div class="form-group">
        <label class="control_label" for="STATUS_PEGAWAI">STATUS</label>
        <select class="form-control" id="STATUS_PEGAWAI" name="STATUS_PEGAWAI">
          <option value="0" selected="selected" disabled="disabled">Status Pegawai</option>
          <option>Aktive</option>
          <option>Tidak Aktive</option>
        </select>
      </div>

      <div class="form-group">
        <label class="control_label" for="JABATAN">JABATAN</label>
        <select class="form-control" id="JABATAN" name="JABATAN">
          <option value="0" selected="selected" disabled="/disabled">JABATAN</option>
          <option>Owner</option>
          <option>Manajer</option>
          <option>PIC</option>
          <option>Marketing</option>
        </select>
      </div>

      <div class="form-group">
        <label for="exampleInputEmail1">Nama Peran</label>
         <select class="form-control" id="ID_PERAN" name="ID_PERAN">
            <option value="none" selected="selected" disabled="disabled">Pilih Peran</option>
                <?php foreach ($tbl_peran as $p):?>
                  <option value="<?php echo $p->ID_PERAN?>"><?php echo $p->NAMA_PERAN?></option>
                <?php endforeach;?>
          </select> 
      </div>
      
    </div>
    <!-- /.box-body -->
 
      <div class="box-footer">
        <button type="submit" class="btn btn-success">Submit</button>
          <a href="<?php echo base_url('Pegawai/index'); ?>"><input type="button" value="Back To View" class="btn btn-success"></a>
        <input type="reset" name="reset" value="Cancel" class="btn btn-success">
      </div>

  </form>
</div>
</div>
