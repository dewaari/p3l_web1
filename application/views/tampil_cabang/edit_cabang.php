<div class="col-md-6">
  <div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Form Add</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <form method="post" action="<?php echo base_url('index.php/Cabang/update') ?>">
      <div class="box-body">
        <?php foreach ($show as $key ) { ?>

        <div class="form-group">
          <label for="exampleInputEmail1">ID_CABANG</label>
          <input type="text" class="form-control" id="ID_CABANG" name="ID_CABANG" value="<?php echo $key->ID_CABANG; ?>" readonly>
        </div>

        <div class="form-group">
          <label for="exampleInputEmail1">NAMA KOTA</label>
          <input type="text" pattern="[A-Z a-z]*" title="Inputan Nama Kota Harus Text" class="form-control" id="NAMA_KOTA" name="NAMA_KOTA" placeholder="Your Name" value="<?php echo $key->NAMA_KOTA; ?>" required="/required">
        </div>

        <div class="form-group">
          <label for="exampleInputPassword1">ALAMAT</label>
          <input type="text" class="form-control" id="ALAMAT" name="ALAMAT" placeholder="Your Address" value="<?php echo $key->ALAMAT; ?>" required="/required">
        </div>

        <div class="form-group">
          <label for="exampleInputPassword1">NO TELEPON CABANG</label>
          <input type="number" min="1" max="089110492890" class="form-control" id="NO_TELP_CABANG" name="NO_TELP_CABANG" placeholder="Your Address" value="<?php echo $key->NO_TELP_CABANG; ?>" required="/required">
        </div>
        
        <?php } ?>
      </div>
      <!-- /.box-body -->

       <div class="box-footer">
        <button type="submit" class="btn btn-success">Submit</button>
        <a href="<?php echo base_url('Cabang/index'); ?>"><input type="button" value="Back To View" class="btn btn-success"></a>
        <input type="reset" name="reset" value="Cancel" class="btn btn-success">
      </div>
    </form>
  </div>
</div>