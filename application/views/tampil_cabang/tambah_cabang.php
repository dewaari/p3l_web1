<div class="col-md-6">
  <div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Form Add</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <form method="post" action="<?php echo base_url('index.php/Cabang/tambah') ?>">
      <div class="box-body">

        <div class="form-group">
          <label for="exampleInputEmail1">Nama Kota</label>
          <input type="text" pattern="[A-Z a-z]*" title="Inputan Harus Text" class="form-control" id="NAMA_KOTA" name="NAMA_KOTA" placeholder="Nama Kota" required="/required">
        </div>

        <div class="form-group">
          <label for="exampleInputPassword1">Alamat</label>
          <input type="text" class="form-control" id="ALAMAT" name="ALAMAT" placeholder="Alamat" required="/required">
        </div>

        <div class="form-group">
          <label for="exampleInputPassword2">NO Telepon Cabang</label>
          <input type="number"  type="number" min="1" max="089110492890" title="12 numeric characters only" class="form-control" id="NO_TELP_CABANG" name="NO_TELP_CABANG" placeholder="No Telepon" required="/required">
        </div>

      </div>
      <!-- /.box-body -->

      <div class="box-footer">
        <button type="submit" class="btn btn-success">Submit</button>
        <a href="<?php echo base_url('Cabang/index'); ?>"><input type="button" value="Back To View" class="btn btn-success"></a>
        <input type="reset" name="reset" value="Cancel" class="btn btn-success">
      </div>

    </form>
  </div>
</div>

