  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title"><b>Data Cabang</b></h3>
          <a href="<?php echo base_url('index.php/Cabang/tambah') ?>" class="glyphicon-plus btn btn-success pull-right">Tambah Data</a>
        </div>
        <!-- /.box-header -->
        <!-- <div class="box-body"> -->
          <table id="datatable" class="table table-hover table-striped">
            <thead>
              <tr>
                <th>NOMOR</th>
                <th>NAMA KOTA</th>
                <th>ALAMAT</th>
                <th>NO TELEPON</th>
                <th>AKSI</th>

              </tr>
            </thead>
            <tbody>
              <?php
              $no = 1;
              foreach ($show as $i) {
                ?>
                <tr>
                  <td><?php echo $no++."."; ?></td>
                  <td><?php echo $i->NAMA_KOTA; ?></td>
                  <td><?php echo $i->ALAMAT; ?></td>
                  <td><?php echo $i->NO_TELP_CABANG; ?></td>
                  <td>
                    <a href="#" class="glyphicon glyphicon-edit btn btn-info btn-sm" onclick="updatejs('<?php echo $i->ID_CABANG; ?>')">edit</a>
                    <a href="#" class="glyphicon glyphicon-remove btn btn-danger btn-sm" onclick="deleted('<?php echo $i->ID_CABANG; ?>')">delete</a>
                  </td>
                </tr>

                <?php } ?>
              </tbody>
            </table>
          </div>
          <!-- /.box-body -->
          <!-- </div> -->
          <!-- /.box -->


        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      <script type="text/javascript">
          function updatejs(param){
           document.location='<?php echo base_url(); ?>index.php/Cabang/edit/'+param;
         }

         function deleted(param){
          var proc = window.confirm('Anda Yakin Akan Menghapus data ini?');
          if(proc){
            document.location='<?php echo base_url(); ?>index.php/Cabang/delete/'+param;
          }
        }
      </script>
