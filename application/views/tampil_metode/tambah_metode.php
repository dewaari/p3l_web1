<div class="col-md-6">
<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Form Add</h3>
  </div>
  <!-- /.box-header -->
  <!-- form start -->
  <form method="post" action="<?php echo base_url('index.php/Metode/tambah_metode') ?>" role="form" enctype="multipart/form-data">
    <div class="box-body">

      <div class="form-group">
        <label for="exampleInputPassword1">NAMA_PEMILIK</label>
        <input type="text" class="form-control" name="NAMA_PEMILIK" placeholder="Nama Pemilik">
      </div>

       <div class="form-group">
        <label for="exampleInputPassword1">NO Kartu</label>
        <input type="text" class="form-control" name="NO_KARTU" placeholder="No Kartu">
      </div>

       <div class="form-group">
        <label for="exampleInputPassword1">Nama Bank</label>
        <input type="text" class="form-control" name="NAMA_BANK" placeholder="Nama Bank">
      </div>

       <div class="form-group">
        <label for="exampleInputPassword1">CVV</label>
        <input type="text" class="form-control"  name="CVV" placeholder="CVV">
      </div>

      <div class="box-body col-lg-6">
        <div class="form-group">
          <label>Image</label>
          <input type="file" name="foto" class="form-control" />
          <!-- <input type="text" name="filefoto" class="form-control" required> -->
        </div>
      </div>
      </div>
    <!-- /.box-body -->
 
      <div class="box-footer">
        <button type="submit" class="btn btn-success">Submit</button>
          <a href="<?php echo base_url('Metode/index'); ?>"><input type="button" value="Back To View" class="btn btn-success"></a>
        <input type="reset" name="reset" value="Cancel" class="btn btn-success">
      </div>
    
  </form>
</div>
</div>