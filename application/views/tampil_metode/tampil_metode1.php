  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title"><b>Data Metode</b></h3>
          <a href="<?php echo base_url('index.php/Metode/tambah_metode') ?>" class="btn btn-success pull-right">Tambah Data</a>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="example2" class="table table-bordered table-hover">
            <thead>
            <tr>
              <th>NOMOR</th>
              <th>NAMA PEMILIK</th>
              <th>NO_KARTU</th>
              <th>NAMA BANK</th>
              <th>CCV</th>
              <th>BUKTI PEMBAYARAN</th>
            </tr>
            </thead>
            <tbody>
                <?php
                $no = 1;
                foreach ($show as $i) {
                ?>
                <tr>
                    <td><?php echo $no++."."; ?></td>
                    <td><?php echo $i->NAMA_PEMILIK; ?></td>
                    <td><?php echo $i->NO_KARTU; ?></td>
                    <td><?php echo $i->NAMA_BANK; ?></td>
                    <td><?php echo $i->CVV; ?></td>
                    <td>
                      <center>
                        <img class="thumbnail" style="height: 100px; width: 100px;" src="<?php echo base_url() ?>assets/images/<?php echo $i->BUKTI_PEMBAYARAN ?>">
                      </center>
                      
                    </td>
                    <td>
                      <a href="#" class="btn btn-info btn-xs" onclick="updatejs('<?php echo $i->ID_METODE; ?>')">edit</a>
                      <a href="#" class="btn btn-danger btn-xs" onclick="deleted('<?php echo $i->ID_METODE; ?>')">delete</a>
                    </td>
                </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->

      
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
<script type="text/javascript">
  function updatejs(param){
   document.location='<?php echo base_url(); ?>index.php/Metode/edit_metode/'+param;
  }
 
  function deleted(param){
    var proc = window.confirm('Anda Yakin Akan Menghapus data ini?');
    if(proc){
      document.location='<?php echo base_url(); ?>index.php/Metode/hapus_metode/'+param;
    }
  }
</script>
