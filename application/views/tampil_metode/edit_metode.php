<div class="col-md-6">
  <div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Form Add</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <form method="post" action="<?php echo base_url('index.php/Metode/update_data_metode') ?>">
      <div class="box-body">
        <?php foreach ($metode as $key ) { ?>
              <div class="form-group">
                <label for="exampleInputEmail1">ID METODE</label>
                <input type="text" class="form-control" id="ID_METODE" name="ID_METODE" value="<?php echo $key->ID_METODE; ?>" readonly>
              </div>

              <div class="form-group">
                <label for="exampleInputPassword1">Nama Pemilik</label>
                <input type="text" pattern="[A-Z a-z]*" title="Inputan Nama Fasilitas Harus Text" class="form-control" id="NAMA_PEMILIK" name="NAMA_PEMILIK" placeholder="Nama Pemilik Kartu" value="<?php echo $key->NAMA_PEMILIK; ?>" required="/required" >
              </div>

               <div class="form-group">
                <label for="exampleInputPassword1">No Kartu</label>
                <input type="text" class="form-control" id="NO_KARTU" name="NO_KARTU" placeholder="Nama Pemilik Kartu" value="<?php echo $key->NO_KARTU; ?>" required="/required" >
              </div>

               <div class="form-group">
                <label for="exampleInputPassword1">Nama Bank</label>
                <input type="text" pattern="[A-Z a-z]*" title="Inputan Nama Fasilitas Harus Text" class="form-control" id="NAMA_BANK" name="NAMA_BANK" placeholder="Nama Pemilik Kartu" value="<?php echo $key->NAMA_BANK; ?>" required="/required" >
              </div>

               <div class="form-group">
                <label for="exampleInputPassword1">CVV</label>
                <input type="text" class="form-control" id="CVV" name="CVV" placeholder="Nama Pemilik Kartu" value="<?php echo $key->CVV; ?>" required="/required" >
              </div>

               <div class="box-body col-lg-6">
                  <div class="form-group">
                    <label>Image</label>
                    <input type="file" name="foto" class="form-control" />
                    <!-- <input type="text" name="filefoto" class="form-control" required> -->
                  </div>
               </div>

        <?php } ?>
      </div>
      <!-- /.box-body -->

      <div class="box-footer">
        <button type="submit" class="btn btn-success">Submit</button>
          <a href="<?php echo base_url('Metode/index'); ?>"><input type="button" value="Back To View" class="btn btn-success"></a>
        <input type="reset" name="reset" value="Cancel" class="btn btn-success">
      </div>

    </form>
  </div>
</div>