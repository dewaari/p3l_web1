<div class="col-md-6">
  <div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Form Add</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <form method="post" action="<?php echo base_url('index.php/Fasilitas/update_data_fasilitas') ?>">
      <div class="box-body">
        <?php foreach ($show as $key ) { ?>
        <div class="form-group">
          <label for="exampleInputEmail1">ID_FASILITAS</label>
          <input type="text" class="form-control" id="ID_FASILITAS" name="ID_FASILITAS" value="<?php echo $key->ID_FASILITAS; ?>" readonly>
        </div>

        <div class="form-group">
          <label for="exampleInputEmail1">Nama Kota</label>
          <select class="form-control" id="ID_CABANG" name="ID_CABANG">
            <option value="0" selected="selected" disabled="disabled">Nama Kota</option>
            <?php foreach ($tbl_cabang as $c):?>
              <option value="<?php echo $c->ID_CABANG?>" <?= $key->ID_CABANG == $c->ID_CABANG ? 'selected' : null ?>><?php echo $c->NAMA_KOTA?></option>
            <?php endforeach;?>
          </select> 
        </div>

        <div class="form-group">
          <label for="exampleInputPassword1">Nama Fasilitas</label>
          <input type="text" pattern="[A-Z a-z /]*" title="Inputan Nama Fasilitas Harus Text" class="form-control" id="NAMA_FASILITAS" name="NAMA_FASILITAS" placeholder="Nama Fasilitas" value="<?php echo $key->NAMA_FASILITAS; ?>" required="/required" >
        </div>

        <div class="form-group">
          <label for="exampleInputPassword1">Harga Fasilitas</label>
          <input type="text" class="form-control" id="HARGA_FASILITAS" name="HARGA_FASILITAS" placeholder="Harga Fasilitas" value="<?php echo $key->HARGA_FASILITAS; ?>" required="/required" >
        </div>

        <?php } ?>
      </div>
      <!-- /.box-body -->

       <div class="box-footer">
        <button type="submit" class="btn btn-success">Submit</button>
        <a href="<?php echo base_url('Fasilitas/index'); ?>"><input type="button" value="Back To View" class="btn btn-success"></a>
        <input type="reset" name="reset" value="Cancel" class="btn btn-success">
      </div>
    </form>
  </div>
</div>

<script type="text/javascript">
    function validasi(){
      var hargas = $('#HARGA_FASILITAS').val().replace(/[^,\d]/g, '').toString();
      var number = /^[0-9]+$/;
      if(!hargas.match(number))
      {
        alert('Harga Harus Angka');
        return false;
      }
    }

    function formatRupiah(angka, prefix)
    {
      var number_string = angka.replace(/[^,\d]/g, '').toString(),
        split = number_string.split(','),
        sisa  = split[0].length % 3,
        rupiah  = split[0].substr(0, sisa),
        ribuan  = split[0].substr(sisa).match(/\d{3}/gi);

      if (ribuan) {
        separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
      }

      rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
      return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }

    var tanpa_tunai = document.getElementById('HARGA_FASILITAS');
    tanpa_tunai.addEventListener('keyup', function(e)
    {
      tanpa_tunai.value = formatRupiah(this.value);
    });
  </script>

