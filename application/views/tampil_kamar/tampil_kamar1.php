  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title"><b>Data Kamar</b></h3>
          <a href="<?php echo base_url('index.php/Kamar/tampil_kamar') ?>" class="btn btn-success pull-right">Tambah Data</a>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="example2" class="table table-bordered table-hover">
            <thead>
            <tr>
              <th>NOMOR</th>
              <th>KODE KAMAR</th>
              <th>NAMA KOTA</th>
              <th>JENIS TEMPAT TIDUR</th>
              <th>JENIS KAMAR</th>
              <th>LANTAI KAMAR</th>
              <th>NOMOR KAMAR</th>
              <th>STATUS KAMAR</th>
              <th>DESKRIPSI KAMAR</th>
              <th>BEBAS ROKO</th>              
              <th>AKSI</th>

            </tr>
            </thead>
            <tbody>
                <?php
                $no = 1;
                foreach ($show as $i) {
                ?>
                <tr>
                    <td><?php echo $no++."."; ?></td>
                    <td><?php echo $i->KODE_KAMAR; ?></td>
                    <td><?php echo $i->NAMA_KOTA; ?></td>
                    <td><?php echo $i->JENIS_TEMPAT_TIDUR; ?></td>
                    <td><?php echo $i->JENIS_KAMAR; ?></td>
                    <td><?php echo $i->LANTAI_KAMAR; ?></td>
                    <td><?php echo $i->NOMOR_KAMAR; ?></td>
                    <td><?php echo $i->STATUS_KAMAR; ?></td>
                    <td><?php echo $i->DESKRIPSI_KAMAR; ?></td>
                    <td><?php echo $i->BEBAS_ROKO; ?></td>
                    <td>
                      <a href="#" class="btn btn-info btn-xs" onclick="updatejs('<?php echo $i->ID_KAMAR; ?>')">edit</a>
                      <a href="#" class="btn btn-danger btn-xs" onclick="deleted('<?php echo $i->ID_KAMAR; ?>')">delete</a>
                    </td>
                </tr>

              <?php } ?>
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->

      
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
<script type="text/javascript">
  function updatejs(param){
   document.location='<?php echo base_url(); ?>index.php/Kamar/edit_kamar/'+param;
  }
 
  function deleted(param){
    var proc = window.confirm('Anda Yakin Akan Menghapus data ini?');
    if(proc){
      document.location='<?php echo base_url(); ?>index.php/Kamar/delete_kamar/'+param;
    }
  }
</script>
