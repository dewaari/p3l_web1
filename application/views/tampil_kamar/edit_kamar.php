<div class="col-md-6">
  <div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Form Add</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <form method="post" action="<?php echo base_url('index.php/Kamar/update_kamar') ?>">
      <div class="box-body">
        <?php foreach ($show as $key ) { ?>

        <div class="form-group">
          <label for="exampleInputEmail1">ID_KAMAR</label>
          <input type="text" class="form-control" id="ID_KAMAR" name="ID_KAMAR" value="<?php echo $key->ID_KAMAR; ?>" readonly>
        </div>

        <div class="form-group">
          <label for="exampleInputPassword1">KODE KAMAR</label>
          <input type="text" class="form-control" id="KODE_KAMAR" name="KODE_KAMAR" placeholder="Your Address" value="<?php echo $key->KODE_KAMAR; ?>">
        </div>

        <div class="form-group">
          <label for="exampleInputEmail1">Nama Kota</label>
          <select class="form-control" id="ID_CABANG", name="ID_CABANG">
            <option value="none" selected="selected">--------------Nama Kota-------------</option>
            <?php foreach ($tbl_cabang as $c){ ?>
            <option value='<?php echo $c->ID_CABANG?>'><?php echo $c->NAMA_KOTA?></option><?php }?>
          </select>
        </div>

        <div class="form-group">
          <label for="exampleInputEmail1">Jenis Tempat Tidur</label>
          <select class="form-control" id="ID_TEMPAT_TIDUR", name="ID_TEMPAT_TIDUR">
            <option value="none" selected="selected">--------------Jenis Tempat Tidur-------------</option>
            <?php foreach ($tbl_tempat_tidur as $t){ ?>
            <option value='<?php echo $t->ID_TEMPAT_TIDUR?>'><?php echo $t->JENIS_TEMPAT_TIDUR?></option><?php }?>
          </select>
        </div>

        <div class="form-group">
          <label for="exampleInputEmail1">Jenis Kamar</label>
          <select class="form-control" id="ID_JENIS_KAMAR", name="ID_JENIS_KAMAR">
            <option value="none" selected="selected">--------------Jenis Kamar-------------</option>
            <?php foreach ($tbl_jenis_kamar as $j){ ?>
            <option value='<?php echo $j->ID_JENIS_KAMAR?>'><?php echo $j->JENIS_KAMAR?></option><?php }?>
          </select>
        </div>

        <div class="form-group">
          <label for="exampleInputPassword1">Lantai Kamar</label>
          <input type="text" class="form-control" id="LANTAI_KAMAR" name="LANTAI_KAMAR" placeholder="Your Address" value="<?php echo $key->LANTAI_KAMAR; ?>">
        </div>

        <div class="form-group">
          <label for="exampleInputPassword1">Nomor Kamar</label>
          <input type="text" class="form-control" id="NOMOR_KAMAR" name="NOMOR_KAMAR" placeholder="Your Address" value="<?php echo $key->NOMOR_KAMAR; ?>">
        </div>

        <div class="form-group">
          <label class="control_label" for="STATUS_KAMAR">STATUS</label>
          <select class="form-control" id="STATUS_KAMAR" name="STATUS_KAMAR">
            <option value="none" selected="selected">--------------Status Kamar-------------</option>
            <option>Aktive</option>
            <option>Tidak Aktive</option>
          </select>
        </div>

        <div class="form-group">
          <label for="exampleFormControlTextarea1">Deskripsi Kamar</label>
          <textarea class="form-control" id="DESKRIPSI_KAMAR" name="DESKRIPSI_KAMAR" rows="3"></textarea>
        </div>

        <div class="form-group">
          <label class="control_label" for="BEBAS_ROKO">BEBAS ROKO</label>
          <select class="form-control" id="BEBAS_ROKO" name="BEBAS_ROKO">
            <option value="none" selected="selected">--------------Bebas Roko-------------</option>
            <option>Bebas</option>
            <option>Tidak Bebas</option>
          </select>
        </div>
        <?php } ?>
      </div>
      <!-- /.box-body -->

       <div class="box-footer">
        <button type="submit" class="btn btn-success">Submit</button>
          <a href="<?php echo base_url('Kamar/index'); ?>"><input type="button" value="Back To View" class="btn btn-success"></a>
        <input type="reset" name="reset" value="Cancel" class="btn btn-success">
      </div>
      
    </form>
  </div>
</div>