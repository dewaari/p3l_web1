<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class M_Metode extends CI_Model
{

	public function select_all()
	{
		$this->db->select('*');
		$this->db->from('tbl_metode_pembayaran');
		$data = $this->db->get();
		return $data->result();
	}

	public function get_metode($ID_METODE)
	{
		$data = $this->db->get_where('tbl_metode_pembayaran', array('ID_METODE' => $ID_METODE));
		return $data->result();
	}

	public function get_all_metode()
	{
		$data = $this->db->get('tbl_metode_pembayaran');
		return $data->result();
	}

	public function insert_metode($data)
	{
		// $data = array(
		// 	'ID_METODE' => '',
		// 	'NAMA_PEMILIK'	=> $data['NAMA_PEMILIK'],
		// 	'NO_KARTU'	=> $data['NO_KARTU'],
		// 	'NAMA_BANK'	=> $data['NAMA_BANK'],
		// 	'CVV'	=> $data['CVV'],
		// 	'BUKTI_PEMBAYARAN'	=> $data['BUKTI_PEMBAYARAN'],
		// );

		// $this->db->insert('tbl_metode_pembayaran', $data);
		// return TRUE;

		$this->db->insert('tbl_metode_pembayaran', $data);
	}

	public function delete_metode($ID_METODE) 
	{
		$sql = "DELETE FROM tbl_metode_pembayaran WHERE ID_METODE='" .$ID_METODE ."'";

		$this->db->query($sql);

		return $this->db->affected_rows();
	}

	public function update_metode($data)
	{
		$this->db->where('ID_METODE', $data['ID_METODE']);
		return $this->db->update('tbl_metode_pembayaran', $data);
	}

}
?>