<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* 
*/
class M_Kamar extends CI_Model
{
	
	public function select_all()
	{
		$this->db->select('*');
		$this->db->from('tbl_kamar');
		$this->db->join('tbl_cabang', 'tbl_kamar.ID_CABANG = tbl_cabang.ID_CABANG');
		$this->db->join('tbl_tempat_tidur', 'tbl_kamar.ID_TEMPAT_TIDUR = tbl_tempat_tidur.ID_TEMPAT_TIDUR');
		$this->db->join('tbl_jenis_kamar' , 'tbl_kamar.ID_JENIS_KAMAR = tbl_jenis_kamar.ID_JENIS_KAMAR');
		$data = $this->db->get();
		return $data->result();
	}

	public function get_kamar($ID_KAMAR)
	{
		$data = $this->db->get_where('tbl_kamar', array('ID_KAMAR' => $ID_KAMAR));
		return $data->result();
	}

	public function get_all_Kamar()
	{
		$data = $this->db->get('tbl_kamar');
		return $data->result();
	}

	public function tambah_kamar($data)
	{
		$data = array(
			'ID_KAMAR' => '',
			'KODE_KAMAR' => $data['KODE_KAMAR'],
			'ID_CABANG' => $data['ID_CABANG'],
			'ID_TEMPAT_TIDUR' => $data['ID_TEMPAT_TIDUR'],
			'ID_JENIS_KAMAR' => $data['ID_JENIS_KAMAR'],
			'LANTAI_KAMAR' => $data['LANTAI_KAMAR'],
			'NOMOR_KAMAR' => $data['NOMOR_KAMAR'],
			'STATUS_KAMAR' => $data['STATUS_KAMAR'],
			'DESKRIPSI_KAMAR' => $data['DESKRIPSI_KAMAR'],
			'BEBAS_ROKO' => $data['BEBAS_ROKO']
		);
		$this->db->insert('tbl_kamar', $data);
		return TRUE;
	}

	public function insert_batch()
	{
		$this->db->insert_batch('tbl_kamar', $data);

		return $this->db->affected_rows();
	}

	public function delete($ID_KAMAR) 
	{
		$sql = "DELETE FROM tbl_kamar WHERE ID_KAMAR='" .$ID_KAMAR ."'";

		$this->db->query($sql);

		return $this->db->affected_rows();
	}

	public function update($data)
	{

		$this->db->where('ID_KAMAR' , $data['ID_KAMAR']);
		return $this->db->update('tbl_kamar', $data);

		// $sql = "UPDATE tbl_kamar SET KODE_KAMAR='" .$data['KODE_KAMAR'] ."' , 
		// 					ID_CABANG='" .$data['ID_CABANG'] ."' , 
		// 					ID_TEMPAT_TIDUR='" .$data['ID_TEMPAT_TIDUR'] ."' , 
		// 					ID_JENIS_KAMAR='" .$data['ID_JENIS_KAMAR'] ."' , 
		// 					LANTAI_KAMAR='" .$data['LANTAI_KAMAR'] ."' , 
		// 					NOMOR_KAMAR='" .$data['NOMOR_KAMAR'] ."' ,
		// 					STATUS_KAMAR='" .$data['STATUS_KAMAR'] ."',
		// 					DESKRIPSI_KAMAR='" .$data['DESKRIPSI_KAMAR'] ."',
		// 					BEBAS_ROKO='" .$data['BEBAS_ROKO'] ."'
		// 					WHERE ID_KAMAR='" .$data['ID_KAMAR'] ."'";
		// $this->db->query($sql);

		// return $this->db->affected_rows();
	}
}

?>