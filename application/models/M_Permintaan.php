<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class M_Permintaan extends CI_Model 
{

	public function select_all() 
	{
		$this->db->select('*');
		$this->db->from('tbl_permintaan_khusus');
		$this->db->join('tbl_reservasi', 'tbl_permintaan_khusus.ID_RESERVASI = tbl_reservasi.ID_RESERVASI');

		$data = $this->db->get();

		return $data->result();
	}	

	public function get_permintaan($ID_PERMINTAAN)
	{
		$data = $this->db->get_where('tbl_permintaan_khusus', array('ID_PERMINTAAN' => $ID_PERMINTAAN));
		return $data->result();
	}

	public function get_all_permintaan()
	{
		$data = $this->db->get('tbl_permintaan_khusus');
		return $data->result();
	}

	public function tambah_permintaan($data) 
	{
		$data = array(
			'ID_PERMINTAAN' => '',
			'ID_RESERVASI'	=> $data['ID_RESERVASI'],
			'NAMA_PERMINTAAN'	=> $data['NAMA_PERMINTAAN'],
			'JUMLAH_PERMINTAAN'	=> $data['JUMLAH_PERMINTAAN']
		);

		$this->db->insert('tbl_permintaan_khusus', $data);
		return TRUE;
	}

	public function insert_batch($data)
	{
		$this->db->insert_batch('tbl_permintaan_khusus', $data);

		return $this->db->affected_rows();
	}

	public function delete($ID_PERMINTAAN) 
	{
		$sql = "DELETE FROM tbl_permintaan_khusus WHERE ID_PERMINTAAN='" .$ID_PERMINTAAN ."'";

		$this->db->query($sql);

		return $this->db->affected_rows();
	}

	public function update_permintaan($data)
	{
		$this->db->where('ID_PERMINTAAN', $data['ID_PERMINTAAN']);
		return $this->db->update('tbl_permintaan_khusus', $data);
	}


}
?>