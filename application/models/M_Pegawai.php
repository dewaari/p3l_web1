<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class M_Pegawai extends CI_Model
{

	public function select_all()
	{
		$this->db->select('*');
		$this->db->from('tbl_pegawai');
		$this->db->join('tbl_cabang', 'tbl_pegawai.ID_CABANG = tbl_cabang.ID_CABANG' );
		$this->db->join('tbl_peran', 'tbl_pegawai.ID_PERAN = tbl_peran.ID_PERAN');
		$data = $this->db->get();
		return $data->result();
	}

	public function get_pegawai($ID_PEGAWAI)
	{
		$data = $this->db->get_where('tbl_pegawai', array('ID_PEGAWAI' => $ID_PEGAWAI));
		return $data->result();
	}

	public function get_all_pegawai()
	{
		$data = $this->db->get('tbl_pegawai');
		return $data->result();
	}

	public function tambah_pegawai($data) 
	{
		$data = array(
			'ID_PEGAWAI' => '',
			'ID_CABANG'	=> $data['ID_CABANG'],
			'NAMA_PEGAWAI'	=> $data['NAMA_PEGAWAI'],
			'EMAIL_PEGAWAI'	=> $data['EMAIL_PEGAWAI'],
			'PASSWORD'	=> $data['PASSWORD'],
			'ALAMAT'	=> $data['ALAMAT'],
			'STATUS_PEGAWAI' => $data['STATUS_PEGAWAI'],
			'JABATAN' => $data['JABATAN'],
			'ID_PERAN'	=> $data['ID_PERAN']

		);

		$this->db->insert('tbl_pegawai', $data);
		return TRUE;
	}

	public function insert_batch($data)
	{
		$this->db->insert_batch('tbl_pegawai', $data);

		return $this->db->affected_rows();
	}

	public function delete($ID_PEGAWAI) 
	{
		$sql = "DELETE FROM tbl_pegawai WHERE ID_PEGAWAI='" .$ID_PEGAWAI ."'";

		$this->db->query($sql);

		return $this->db->affected_rows();
	}

	public function update_pegawai($data)
	{
		$this->db->where('ID_PEGAWAI', $data['ID_PEGAWAI']);
		return $this->db->update('tbl_pegawai', $data);


		// $sql = "UPDATE tbl_pegawai SET ID_CABANG='" .$data['ID_CABANG'] ."' ,
		// 							NAMA_PEGAWAI='" .$data['NAMA_PEGAWAI'] ."' ,
		// 							EMAIL_PEGAWAI='" .$data['EMAIL_PEGAWAI'] ."' ,
		// 							PASSWORD='" .$data['PASSWORD'] ."' ,
		// 							ALAMAT='" .$data['ALAMAT'] ."' ,
		// 							STATUS_PEGAWAI='" .$data['STATUS_PEGAWAI'] ."' ,
		// 							JABATAN='" .$data['JABATAN'] ."' ,
		// 							ID_PERAN='" .$data['ID_PERAN'] ."'
		// 							WHERE ID_PEGAWAI='" .$data['ID_PEGAWAI'] ."'";
		// $this->db->query($sql);

		// return $this->db->affected_rows();
	}
}


?>