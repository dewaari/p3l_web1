<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

/**
* 
*/
class M_Jenis_Kamar extends CI_Model
{
	
	public function select_all()
	{
		$this->db->select('*');
		$this->db->from('tbl_jenis_kamar');
		$data = $this->db->get();
		return $data->result();
	}

	public function get_all_jenis_kamar()
	{
		$data = $this->db->get('tbl_jenis_kamar');
		return $data->result();
	}

	public function get_jenis_kamar($ID_JENIS_KAMAR)
	{
		$data = $this->db->get_where('tbl_jenis_kamar', array('ID_JENIS_KAMAR' => $ID_JENIS_KAMAR));
		return $data->result();
	}

	public function tambah_jenis_kamar($data)
	{
		$data = array(
			'ID_JENIS_KAMAR' => '',
			'JENIS_KAMAR' => $data['JENIS_KAMAR'],
			'KAPASITAS' => $data['KAPASITAS'],
			'DESKRIPSI_KAMAR' => $data['DESKRIPSI_KAMAR'],
			'STATUS_KAMAR' => $data['STATUS_KAMAR'],
			'HARGA' => $data['HARGA']
		);
		$this->db->insert('tbl_jenis_kamar', $data);
		return TRUE;
	}

	public function insert_batch()
	{
		$this->db->insert_batch('tbl_jenis_kamar', $data);

		return $this->db->affected_rows();
	}

	public function delete($ID_JENIS_KAMAR) {
		$sql = "DELETE FROM tbl_jenis_kamar WHERE ID_JENIS_KAMAR='" .$ID_JENIS_KAMAR ."'";

		$this->db->query($sql);

		return $this->db->affected_rows();
	}

	public function update($data)
	{
		$sql = "UPDATE tbl_jenis_kamar SET JENIS_KAMAR='" .$data['JENIS_KAMAR'] ."' , 
							KAPASITAS='" .$data['KAPASITAS'] ."', 
							DESKRIPSI_KAMAR='" .$data['DESKRIPSI_KAMAR'] ."', 
							STATUS_KAMAR='" .$data['STATUS_KAMAR'] ."', 
							HARGA='" .$data['HARGA'] ."' 
							WHERE ID_JENIS_KAMAR='" .$data['ID_JENIS_KAMAR'] ."'";

		$this->db->query($sql);

		return $this->db->affected_rows();
	}
}
?>