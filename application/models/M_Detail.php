 <?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class M_Detail extends CI_Model 
{

	public function select_all() 
	{
		$this->db->select('*');
		$this->db->from('detail_reservasi');

		$data = $this->db->get();

		return $data->result();
	}	

	public function get_detail($ID_DETAIL_RESERVASI)
	{
		$data = $this->db->get_where('detail_reservasi', array('ID_DETAIL_RESERVASI' => $ID_DETAIL_RESERVASI));
		return $data->result();
	}

	public function get_all_detail()
	{
		$data = $this->db->get('detail_reservasi');
		return $data->result();
	}
}
?>