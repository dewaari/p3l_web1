<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class M_Detail_Pembayaran extends CI_Model
{

	public function select_all_pembayaran()
	{
		$this->db->select('*');
		$this->db->from('tbl_detail_pembayaran');
		$this->db->join('tbl_reservasi', 'tbl_detail_pembayaran.ID_RESERVASI = tbl_reservasi.ID_RESERVASI');
		//$this->db->join('tbl_metode_pembayaran' , 'tbl_detail_pembayaran.ID_METODE = tbl_metode_pembayaran.ID_METODE');
		$data = $this->db->get();
		return $data->result();
	}

	public function get_pembayaran($ID_PEMBAYARAN)
	{
		$data = $this->db->get_where('tbl_detail_pembayaran', array('ID_PEMABAYARAN' => $ID_PEMABAYARAN));
		return $data->result();
	}

	public function get_all_pembayaran()
	{
		$data = $this->db->get('tbl_detail_pembayaran');
		return $data->result();
	}

	public function select_data_transfer($ID_PEMBAYARAN)
	{
		$this->db->select('tbl_detail_pembayaran.ID_PEMBAYARAN, tbl_detail_pembayaran.ID_RESERVASI, tbl_detail_pembayaran.NAMA_PEMILIK, tbl_detail_pembayaran.NAMA_BANK, tbl_detail_pembayaran.BUKTI_PEMBAYARAN, tbl_detail_pembayaran.TANGGAL_PEMBAYARAN, tbl_detail_pembayaran.TOTAL_PEMBAYARAN_AWAL');
		$this->db->from('tbl_detail_pembayaran');
		$this->db->join('tbl_reservasi', 'tbl_detail_pembayaran.ID_RESERVASI = tbl_reservasi.ID_RESERVASI');

		$query = $this->db->get();
		return $query;
	}

	public function select_data_kartu($ID_PEMBAYARAN)
	{
		$this->db->select('tbl_detail_pembayaran.ID_PEMBAYARAN, tbl_detail_pembayaran.ID_RESERVASI tbl_detail_pembayaran.NAMA_PEMILIK, tbl_detail_pembayaran.NAMA_BANK, tbl_detail_pembayaran.NO_KARTU, tbl_detail_pembayaran.TANGGAL_PEMBAYARAN, tbl_detail_pembayaran.TOTAL_PEMBAYARAN_AWAL');
		$this->db->from('tbl_detail_pembayaran');
		$this->db->join('tbl_reservasi', 'tbl_detail_pembayaran.ID_RESERVASI = tbl_reservasi.ID_RESERVASI');

		$query = $this->db->get();
		return $query;
	}

	public function tambah_bayar_transfer($data)
	{
		$data = array(
				'ID_PEMBAYARAN' => '',
				'ID_RESERVASI' => $data['ID_RESERVASI'],
				'NAMA_PEMILIK'	=> $data['NAMA_PEMILIK'],
				'NAMA_BANK'		=> $data['NAMA_BANK'],
				'BUKTI_PEMBAYARAN'	=> $data['BUKTI_PEMBAYARAN'],
				'TANGGAL_PEMBAYARAN'	=> $data['TANGGAL_PEMBAYARAN'],
				'TOTAL_PEMBAYARAN_AWAL'	=> $data['TOTAL_PEMBAYARAN_AWAL'],
		);

		$this->db->insert('tbl_detail_pembayaran', $data);
		return TRUE;

		//$this->db->insert('tbl_detail_pembayaran', $data);
	}

	public function insert_batch($data)
	{
		$this->db->insert_batch('tbl_detail_pembayaran', $data);

		return $this->db->affected_rows();
	}

	public function tambah_bayar_kartu($data)
	{
		$data = array(
				'ID_PEMBAYARAN' => '',
				'ID_RESERVASI' => $data['ID_RESERVASI'],
				'NAMA_PEMILIK'	=> $data['NAMA_PEMILIK'],
				'NAMA_BANK'		=> $data['NAMA_BANK'],
				'NO_KARTU'	=> $data['NO_KARTU'],
				'TANGGAL_PEMBAYARAN'	=> $data['TANGGAL_PEMBAYARAN'],
				'TOTAL_PEMBAYARAN_AWAL'	=> $data['TOTAL_PEMBAYARAN_AWAL'],
		);

		$this->db->insert('tbl_detail_pembayaran', $data);
		return TRUE;

		//$this->db->insert('tbl_detail_pembayaran', $data);
	}

	public function insert_batch_kartu($data)
	{
		$this->db->insert_batch('tbl_detail_pembayaran', $data);

		return $this->db->affected_rows();
	}

	public function detail_data_nota($ID_PEMBAYARAN)
	{
		$this->db->select('tbl_detail_pembayaran.ID_PEMBAYARAN, tbl_reservasi.ID_BOOKING, tbl_reservasi.ID_PELANGGAN, tbl_pelanggan.ID_PELANGGAN, tbl_pelanggan.NAMA_PELANGGAN, tbl_pelanggan.ALAMAT');
		$this->db->from('tbl_detail_pembayaran');
		$this->db->join('tbl_reservasi', 'tbl_detail_pembayaran.ID_RESERVASI = tbl_reservasi.ID_RESERVASI');
		$this->db->join('tbl_pelanggan', 'tbl_reservasi.ID_PELANGGAN = tbl_pelanggan.ID_PELANGGAN');
		$this->db->where("tbl_detail_pembayaran.ID_PEMBAYARAN",$ID_PEMBAYARAN);
		$query = $this->db->get();
		return $query;
	}




}
?>