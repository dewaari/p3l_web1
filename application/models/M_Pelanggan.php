<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class M_Pelanggan extends CI_Model
{

	public function select_all()
	{
		$this->db->select('*');
		$this->db->from('tbl_pelanggan');
		$this->db->join('tbl_peran', 'tbl_pelanggan.ID_PERAN = tbl_peran.ID_PERAN');
		$data = $this->db->get();
		return $data->result();
	}

	public function get_pelanggan($ID_PELANGGAN)
	{
		$data = $this->db->get_where('tbl_pelanggan', array('ID_PELANGGAN' => $ID_PELANGGAN));
		return $data->result();
	}

	public function get_all_pelanggan()
	{
		$data = $this->db->get('tbl_pelanggan');
		return $data->result();
	}

	public function tambah_pelanggan($data) 
	{
		$data = array(
			'ID_PELANGGAN' => '',
			'NAMA_PELANGGAN'	=> $data['NAMA_PELANGGAN'],
			'EMAIL_PELANGGAN'	=> $data['EMAIL_PELANGGAN'],
			'ALAMAT'	=> $data['ALAMAT'],
			'NO_TELEPON_PELANGGAN'	=> $data['NO_TELEPON_PELANGGAN'],
			'NO_IDENTITAS'	=> $data['NO_IDENTITAS'],
			'PASSWORD' => $data['PASSWORD'],
			'TANGGAL_PENDAFTARAN' => $data['TANGGAL_PENDAFTARAN'],
			'ID_PERAN'	=> $data['ID_PERAN']

		);

		$this->db->insert('tbl_pelanggan', $data);
		return TRUE;
	}

	public function insert_batch($data)
	{
		$this->db->insert_batch('tbl_pelanggan', $data);

		return $this->db->affected_rows();
	}

	public function delete_pelanggan($ID_PELANGGAN) 
	{
		$sql = "DELETE FROM tbl_pelanggan WHERE ID_PELANGGAN='" .$ID_PELANGGAN ."'";

		$this->db->query($sql);

		return $this->db->affected_rows();
	}

	public function update_pelanggan($data)
	{
		$this->db->where('ID_PELANGGAN', $data['ID_PELANGGAN']);
		return $this->db->update('tbl_pelanggan', $data);
	


		// $sql = "UPDATE tbl_pegawai SET ID_CABANG='" .$data['ID_CABANG'] ."' ,
		// 							NAMA_PEGAWAI='" .$data['NAMA_PEGAWAI'] ."' ,
		// 							EMAIL_PEGAWAI='" .$data['EMAIL_PEGAWAI'] ."' ,
		// 							PASSWORD='" .$data['PASSWORD'] ."' ,
		// 							ALAMAT='" .$data['ALAMAT'] ."' ,
		// 							STATUS_PEGAWAI='" .$data['STATUS_PEGAWAI'] ."' ,
		// 							JABATAN='" .$data['JABATAN'] ."' ,
		// 							ID_PERAN='" .$data['ID_PERAN'] ."'
		// 							WHERE ID_PEGAWAI='" .$data['ID_PEGAWAI'] ."'";
		// $this->db->query($sql);

		// return $this->db->affected_rows();
	}
}


?>