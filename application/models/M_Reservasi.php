<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class M_Reservasi extends CI_Model
{

	public function select_all_reservasi()
	{
		//$this->db->select('*');
		 $data = " SELECT tbl_reservasi.ID_RESERVASI, tbl_reservasi.ID_BOOKING, tbl_reservasi.ID_PELANGGAN, tbl_reservasi.ID_CABANG, tbl_reservasi.TANGGAL_RESERVASI, tbl_reservasi.JUMLAH_DEWASA, tbl_reservasi.JUMLAH_ANAK, tbl_reservasi.TOTAL_BIAYA, tbl_reservasi.STATUS_RESERVASI, tbl_reservasi.UANG_JAMINAN, tbl_reservasi.JENIS_TAMU, detail_reservasi.TANGGAL_MASUK, detail_reservasi.TANGGAL_SELESAI"; 
		$this->db->from('tbl_reservasi');
		///$this->db->join('tbl_pegawai' , 'tbl_reservasi.ID_PEGAWAI = tbl_pegawai.ID_PEGAWAI');
		$this->db->join('tbl_pelanggan' , 'tbl_reservasi.ID_PELANGGAN = tbl_pelanggan.ID_PELANGGAN');
		$this->db->join('tbl_cabang' , 'tbl_reservasi.ID_CABANG = tbl_cabang.ID_CABANG');
		$this->db->join('detail_reservasi' , 'tbl_reservasi.ID_RESERVASI = detail_reservasi.ID_RESERVASI');
		$this->db->join('tbl_kamar', 'detail_reservasi.ID_KAMAR = tbl_kamar.ID_KAMAR');
		$this->db->join('tbl_jenis_kamar', 'tbl_kamar.ID_JENIS_KAMAR = tbl_jenis_kamar.ID_JENIS_KAMAR');
		$data = $this->db->get();
		return $data->result();
	}

	public function get_id_booking($ID_RESERVASI)
	{
		$data = $this->db->get_where('tbl_reservasi', array('ID_RESERVASI' => $ID_RESERVASI));
		return $data->result();
	}

	public function get_reservasi($ID_RESERVASI)
	{
		$data = $this->db->get_where('tbl_reservasi', array('ID_RESERVASI' => $ID_RESERVASI));
		return $data->result();
	}

	public function get_all_reservasi()
	{
		$data = $this->db->get('tbl_reservasi');
		return $data->result();
	}

	public function tambah_reservasi($data)
	{
		$data = array(
			'ID_RESERVASI' => '',
			'ID_BOOKING' => $data['ID_BOOKING'],
			'ID_PELANGGAN' => $data['ID_PELANGGAN'],
			'ID_CABANG' => $data['ID_CABANG'],
			'TANGGAL_RESERVASI' => $data['TANGGAL_RESERVASI'],
			'JUMLAH_DEWASA' => $data['JUMLAH_DEWASA'],
			'JUMLAH_ANAK' => $data['JUMLAH_ANAK'],
			// 'TANGGAL_BAYAR' => $data['TANGGAL_BAYAR'],
			'TOTAL_BIAYA' => $data['TOTAL_BIAYA'],
			'STATUS_RESERVASI' => $data['STATUS_RESERVASI'],
			//'NAMA_INSTITUSI' => $data['NAMA_INSTITUSI'],
			// 'DEPOSIT' => $data['DEPOSIT'],
			'UANG_JAMINAN' => $data['UANG_JAMINAN'],
			'JENIS_TAMU' => $data['JENIS_TAMU'],
		);


		$this->db->insert('tbl_reservasi',$data);
		$last_id = $this->db->insert_id();
		return $last_id;
		
	}

	public function tambah_detail($data1)
	{
		$data1 = array(
					'ID_KAMAR'=>$data1['ID_KAMAR'],
					'ID_RESERVASI'=>$data1['ID_RESERVASI'],
					'TANGGAL_MASUK' => $data1['TANGGAL_MASUK'],
					'TANGGAL_SELESAI' => $data1['TANGGAL_SELESAI']
				);

		$this->db->insert('detail_reservasi',$data1);
		return TRUE;
	}

	public function tambah_jenis_kamar($data2)
	{
		$data2 = array(
			'ID_JENIS_KAMAR' => $data2['ID_JENIS_KAMAR']
		);

		$this->db->insert('tbl_jenis_kamar',$data2);
		return TRUE;
	}

	public function insert_batch()
	{
		$this->db->insert_batch('tbl_reservasi', $data);

		return $this->db->affected_rows();
	}

	public function delete_reservasi($ID_RESERVASI) 
	{
		$sql = "DELETE FROM tbl_reservasi WHERE ID_RESERVASI='" .$ID_RESERVASI ."'";

		$this->db->query($sql);

		return $this->db->affected_rows();
	}

	public function detail_reservasi($ID_RESERVASI)
	{
		$this->db->select('tbl_reservasi.ID_RESERVASI, tbl_reservasi.ID_BOOKING, tbl_reservasi.ID_PELANGGAN, tbl_pelanggan.NAMA_PELANGGAN, tbl_pelanggan.EMAIL_PELANGGAN, tbl_pelanggan.ALAMAT, tbl_pelanggan.NO_TELEPON_PELANGGAN, tbl_pelanggan.NO_IDENTITAS , tbl_cabang.ID_CABANG, tbl_cabang.NAMA_KOTA , tbl_kamar.ID_KAMAR, tbl_kamar.KODE_KAMAR, tbl_jenis_kamar.ID_JENIS_KAMAR, tbl_jenis_kamar.JENIS_KAMAR ,tbl_reservasi.TANGGAL_RESERVASI, tbl_reservasi.JUMLAH_DEWASA as jdewasa, tbl_reservasi.JUMLAH_ANAK as janak, tbl_reservasi.TOTAL_BIAYA, tbl_reservasi.STATUS_RESERVASI, tbl_reservasi.UANG_JAMINAN, tbl_reservasi.JENIS_TAMU, detail_reservasi.TANGGAL_MASUK, detail_reservasi.TANGGAL_SELESAI');
		$this->db->from("tbl_reservasi");
		$this->db->join('tbl_pelanggan' , 'tbl_reservasi.ID_PELANGGAN = tbl_pelanggan.ID_PELANGGAN');
		$this->db->join('tbl_cabang' , 'tbl_reservasi.ID_CABANG = tbl_cabang.ID_CABANG');
		$this->db->join('detail_reservasi' , 'tbl_reservasi.ID_RESERVASI = detail_reservasi.ID_RESERVASI');
		$this->db->join('tbl_kamar', 'detail_reservasi.ID_KAMAR = tbl_kamar.ID_KAMAR');
		$this->db->join('tbl_jenis_kamar', 'tbl_kamar.ID_JENIS_KAMAR = tbl_jenis_kamar.ID_JENIS_KAMAR');
		$this->db->where("tbl_reservasi.ID_RESERVASI", $ID_RESERVASI);
		$query = $this->db->get();
		return $query;

	}


	public function view_data_trasnfer()
	{
		$this->db->select('tbl_reservasi.ID_RESERVASI, tbl_reservasi.ID_BOOKING, tbl_reservasi.STATUS_RESERVASI, tbl_reservasi.UANG_JAMINAN, tbl_reservasi.TOTAL_BIAYA, tbl_reservasi.ID_PELANGGAN, tbl_pelanggan.NAMA_PELANGGAN, detail_reservasi.ID_RESERVASI, detail_reservasi.ID_DETAIL_RESERVASI, detail_reservasi.TANGGAL_MASUK, detail_reservasi.TANGGAL_SELESAI ');
		$this->db->from('tbl_reservasi');
		$this->db->join('tbl_pelanggan' , 'tbl_reservasi.ID_PELANGGAN = tbl_pelanggan.ID_PELANGGAN');
		$this->db->join('detail_reservasi' , 'tbl_reservasi.ID_RESERVASI = detail_reservasi.ID_RESERVASI');
		//$this->db->order_by("ID_RESERVASI", "desc");
		$query = $this->db->get();
		return $query;
	}

	public function view_data_trasnferID($ID_RESERVASI)
	{
		$this->db->select('*');
		$this->db->from('tbl_reservasi');
		$this->db->where("ID_RESERVASI", $ID_RESERVASI);
		$this->db->order_by("ID_RESERVASI", "desc");
		$query = $this->db->get();
		return $query;
	}

	// public function detail_data_nota($ID_RESERVASI)
	// {
	// 	$this->db->select('tbl_reservasi.ID_RESERVASI, tbl_reservasi.ID_BOOKING, tbl_reservasi.ID_PELANGGAN, tbl_pelanggan.ID_PELANGGAN, tbl_pelanggan.NAMA_PELANGGAN, tbl_pelanggan.ALAMAT');
	// 	$this->db->from('tbl_reservasi');
	// 	$this->db->join('tbl_pelanggan', 'tbl_reservasi.ID_PELANGGAN = tbl_pelanggan.ID_PELANGGAN');
	// 	$query = $this->db->get();
	// 	return $query;
	// }




}
?>