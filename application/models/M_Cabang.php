<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class M_Cabang extends CI_Model 
{

	public function select_all() 
	{
		$this->db->select('*');
		$this->db->from('tbl_cabang');

		$data = $this->db->get();

		return $data->result();
	}	

	public function get_cabang($ID_CABANG)
	{
		$data = $this->db->get_where('tbl_cabang', array('ID_CABANG' => $ID_CABANG));
		return $data->result();
	}

	public function get_all_cabang()
	{
		$data = $this->db->get('tbl_cabang');
		return $data->result();
	}

	public function insert($data) {
		$sql = "INSERT INTO tbl_cabang VALUES('','" .$data['NAMA_KOTA'] ."','" .$data['ALAMAT'] ."'," .$data['NO_TELP_CABANG'] .")";

		$this->db->query($sql);

		return $this->db->affected_rows();
	}

	public function insert_batch($data) {
		$this->db->insert_batch('tbl_cabang', $data);
		
		return $this->db->affected_rows();
	}

	
	public function update($data) {
		$sql = "UPDATE tbl_cabang SET NAMA_KOTA='" .$data['NAMA_KOTA'] ."' , ALAMAT='" .$data['ALAMAT'] ."', NO_TELP_CABANG=" .$data['NO_TELP_CABANG'] ." WHERE ID_CABANG='" .$data['ID_CABANG'] ."'";

		$this->db->query($sql);

		return $this->db->affected_rows();
	}

	public function delete($ID_CABANG) {
		$sql = "DELETE FROM tbl_cabang WHERE ID_CABANG='" .$ID_CABANG ."'";

		$this->db->query($sql);

		return $this->db->affected_rows();
	}
}

/* End of file Cabang.php */
/* Location: ./application/models/Cabang.php */