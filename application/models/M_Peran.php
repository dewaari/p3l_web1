<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

/**
* 
*/
class M_Peran extends CI_Model
{
	
	public function select_all()
	{
		$this->db->select('*');
		$this->db->from('tbl_peran');
		$data = $this->db->get();
		return $data->result();
	}

	public function get_all_peran()
	{
		$data = $this->db->get('tbl_peran');
		return $data->result();
	}

	public function get_peran($ID_PERAN)
	{
		$data = $this->db->get_where('tbl_peran', array('ID_PERAN' => $ID_PERAN));
		return $data->result();
	}
}
?>