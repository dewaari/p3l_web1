<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

	/**
	* 
	*/
class M_Sesion extends CI_Model
{
	
	public function select_all()
	{
		$this->db->select('*');
		$this->db->from('tbl_sesion');
		$this->db->join('tbl_jenis_kamar', 'tbl_sesion.ID_JENIS_KAMAR = tbl_jenis_kamar.ID_JENIS_KAMAR');
		$data = $this->db->get();
		return $data->result();
	}

	public function get_sesion($ID_SESION)
	{
		$data = $this->db->get_where('tbl_sesion', array('ID_SESION' => $ID_SESION));
		return $data->result();
	}


	public function tambah_sesion($data)
	{
		$data = array(
				'ID_SESION' => '',
				'ID_JENIS_KAMAR' => $data['ID_JENIS_KAMAR'],
				'NAMA_SESION' => $data['NAMA_SESION'],
				'TANGGAL_MULAI' => $data['TANGGAL_MULAI'],
				'TANGGAL_SELESAI' => $data['TANGGAL_SELESAI'],
				'HARGA_SESION' => $data['HARGA_SESION'],
				'STATUS_SESION' => $data['STATUS_SESION']
			);
		// $sql = "INSERT INTO tbl_sesion VALUES('','" .$data['ID_CABANG'] ."','" .$data['NAMA_SESION'] ."','" .$data['TANGGAL_MULAI'] ."','" .$data['TANGGAL_SELESAI'] ."','" .$data['HARGA_SESION'] ."','" .$data['STATUS_SESION'] .")";

		$this->db->insert('tbl_sesion',$data);
		return TRUE;
		//$sql = $this->db->set($data)->get_compiled_insert('tbl_sesion');
		//echo $sql;
	}

	public function insert_batch($data)
	{
		$this->db->insert_batch('tbl_sesion', $data);

		return $this->db->affected_rows();
	}

	public function delete($ID_SESION) {
		$sql = "DELETE FROM tbl_sesion WHERE ID_SESION='" .$ID_SESION ."'";

		$this->db->query($sql);

		return $this->db->affected_rows();
	}

	public function update_sesion($data)
	{
		$this->db->where('ID_SESION', $data['ID_SESION']);
		return $this->db->update('tbl_sesion', $data);
	}

}

?>