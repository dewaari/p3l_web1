<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class M_Tempat_Tidur extends CI_Model
{
	public function select_all()
	{
		$this->db->select('*');
		$this->db->from('tbl_tempat_tidur');
		$data = $this->db->get();
		return $data->result();
	}

	public function get_tempat_tidur($ID_TEMPAT_TIDUR)
	{
		$data = $this->db->get_where('tbl_tempat_tidur', array('ID_TEMPAT_TIDUR' => $ID_TEMPAT_TIDUR));
		return $data->result();
	}

	public function get_all_tempat_tidur()
	{
		$data = $this->db->get('tbl_tempat_tidur');
		return $data->result();
	}

	public function tambah_tempat_tidur($data) 
	{
		$data = array(
			'ID_TEMPAT_TIDUR' => '',
			'JENIS_TEMPAT_TIDUR'	=> $data['JENIS_TEMPAT_TIDUR'],
			'HARGA' => $data['HARGA'],
		);

		$this->db->insert('tbl_tempat_tidur', $data);
		return TRUE;
	}

	public function insert_batch($data)
	{
		$this->db->insert_batch('tbl_tempat_tidur', $data);

		return $this->db->affected_rows();
	}

	public function delete($ID_TEMPAT_TIDUR) 
	{
		$sql = "DELETE FROM tbl_tempat_tidur WHERE ID_TEMPAT_TIDUR='" .$ID_TEMPAT_TIDUR ."'";

		$this->db->query($sql);

		return $this->db->affected_rows();
	}

	public function update_tempat_tidur($data)
	{
		$this->db->where('ID_TEMPAT_TIDUR', $data['ID_TEMPAT_TIDUR']);
		return $this->db->update('tbl_tempat_tidur', $data);


		// $sql = "UPDATE tbl_pegawai SET ID_CABANG='" .$data['ID_CABANG'] ."' ,
		// 							NAMA_PEGAWAI='" .$data['NAMA_PEGAWAI'] ."' ,
		// 							EMAIL_PEGAWAI='" .$data['EMAIL_PEGAWAI'] ."' ,
		// 							PASSWORD='" .$data['PASSWORD'] ."' ,
		// 							ALAMAT='" .$data['ALAMAT'] ."' ,
		// 							STATUS_PEGAWAI='" .$data['STATUS_PEGAWAI'] ."' ,
		// 							JABATAN='" .$data['JABATAN'] ."' ,
		// 							ID_PERAN='" .$data['ID_PERAN'] ."'
		// 							WHERE ID_PEGAWAI='" .$data['ID_PEGAWAI'] ."'";
		// $this->db->query($sql);

		// return $this->db->affected_rows();
	}
}

?>