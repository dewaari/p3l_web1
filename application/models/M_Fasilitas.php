<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

/**
* 
*/
class M_Fasilitas extends CI_Model
{
	
	public function select_all()
	{
		$this->db->select('*');
		$this->db->from('tbl_fasilitas');
		$this->db->join('tbl_cabang','tbl_fasilitas.ID_CABANG = tbl_cabang.ID_CABANG');
		$data = $this->db->get();
		return $data->result();
	}

	public function get_fasilitas($ID_FASILITAS)
	{
		$data = $this->db->get_where('tbl_fasilitas', array('ID_FASILITAS' => $ID_FASILITAS));
		return $data->result();
	}

	public function get_all_fasilitas()
	{
		$data = $this->db->get('tbl_fasilitas');
		return $data->result();
	}

	public function insert($data) 
	{
		$data = array(
			'ID_FASILITAS' => '',
			'ID_CABANG'	=> $data['ID_CABANG'],
			'NAMA_FASILITAS' => $data['NAMA_FASILITAS'],
			'HARGA_FASILITAS' => $data['HARGA_FASILITAS']

		);

		$this->db->insert('tbl_fasilitas', $data);
		return TRUE;
	}

	public function insert_batch($data)
	{
		$this->db->insert_batch('tbl_fasilitas', $data);

		return $this->db->affected_rows();
	}

	public function delete($ID_FASILITAS) 
	{
		$sql = "DELETE FROM tbl_fasilitas WHERE ID_FASILITAS='" .$ID_FASILITAS ."'";

		$this->db->query($sql);

		return $this->db->affected_rows();
	}

	public function update_fasilitas($data)
	{
		$this->db->where('ID_FASILITAS', $data['ID_FASILITAS']);
		return $this->db->update('tbl_fasilitas', $data);

		// $sql = "UPDATE tbl_fasilitas SET ID_CABANG='" .$data['ID_CABANG'] ."' , 
		// 				NAMA_FASILITAS='" .$data['NAMA_FASILITAS'] ."' , 
		// 				HARGA_FASILITAS='" .$data['HARGA_FASILITAS'] ."' 
		// 				WHERE ID_FASILITAS='" .$data['ID_FASILITAS'] ."'";

		// $this->db->query($sql);

		// return $this->db->affected_rows();
	}
}

?>