<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

/**
* 
*/
class M_Login extends CI_Model
{
	public function login($e,$p)
	{
		$EMAIL=$e;
		$PASSWORD=sha1($p);

		$login = $this->db->get_where('tbl_pegawai' , array('EMAIL_PEGAWAI' => $EMAIL, 'PASSWORD'=>$PASSWORD ));
		if (count($login->result())>0)
		{
			foreach ($login->result() as $key) {
				$per['NAMA_PEGAWAI'] = $key->NAMA_PEGAWAI;
				$per['EMAIL_PEGAWAI'] = $key->EMAIL_PEGAWAI;
				if ($key->ID_PERAN == 3)
					$per['ADMIN'] = TRUE;
				else
					$per['ADMIN'] = FALSE;
				$this->session->set_userdata($per);
			}
			header('location:'.base_url().'Fasilitas');

		}else
		{
			header('location:'.base_url().'Login');
		}
	}

	public function change_password($data)
	{
		$data['PASSWORD_LAMA'] = sha1($data['PASSWORD_LAMA']);

		$data2 = array(
			'PASSWORD' => sha1($data['PASSWORD_BARU'])
		);

		$query = $this->db->get_where('tbl_pegawai', array('EMAIL_PEGAWAI' => $data['EMAIL_PEGAWAI'], 'PASSWORD' => $data['PASSWORD_LAMA']));

		if ($query->result_id->num_rows > 0) {
			$this->db->reset_query();
			$this->db->where(array('EMAIL_PEGAWAI' => $data['EMAIL_PEGAWAI'], 'PASSWORD' => $data['PASSWORD_LAMA']));
			$this->db->update('tbl_pegawai', $data2);
			return TRUE;
		}
		return FALSE;

	}
}