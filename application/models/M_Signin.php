<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

/**
* 
*/
class M_Signin extends CI_Model
{
	public function signin($EMAIL_PELANGGAN,$PASSWORD)
	{
		$this->db->where('EMAIL_PELANGGAN', $EMAIL_PELANGGAN);
		$this->db->where('PASSWORD', $PASSWORD);
		$query = $this->db->get('tbl_pelanggan');
		if ($query->num_rows()>0)
		{
			foreach ($query->result() as $row) {
				$sess = array ('EMAIL_PELANGGAN' => $row->EMAIL_PELANGGAN,
								'PASSWORD' => $row->PASSWORD
				);
			}
			$this->session->get_userdata($sess);
			redirect('Welcome');
		}
		else
		{
			$this->session->set_flashdata('info' , 'Maaf Username dan Password Salah Mohon Diperiksa Kembali');
			redirect('Signin');
		}
		
	}

	public function keamanan()
	{
		$username = $this->session->sess_destroy('EMAIL_PELANGGAN');
		if(!empty($EMAIL_PELANGGAN))
		{
			$this->session->sess_destroy();
			redirect('signin');
		}
	}

	// public function change_password($data)
	// {
	// 	$data['PASSWORD_LAMA'] = sha1($data['PASSWORD_LAMA']);

	// 	$data2 = array(
	// 		'PASSWORD' => sha1($data['PASSWORD_BARU'])
	// 	);

	// 	$query = $this->db->get_where('tbl_pegawai', array('EMAIL_PEGAWAI' => $data['EMAIL_PEGAWAI'], 'PASSWORD' => $data['PASSWORD_LAMA']));

	// 	if ($query->result_id->num_rows > 0) {
	// 		$this->db->reset_query();
	// 		$this->db->where(array('EMAIL_PEGAWAI' => $data['EMAIL_PEGAWAI'], 'PASSWORD' => $data['PASSWORD_LAMA']));
	// 		$this->db->update('tbl_pegawai', $data2);
	// 		return TRUE;
	// 	}
	// 	return FALSE;

	// }
}