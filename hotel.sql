-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 14 Apr 2018 pada 22.47
-- Versi server: 10.1.30-MariaDB
-- Versi PHP: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hotel`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_reservasi`
--

CREATE TABLE `detail_reservasi` (
  `ID_DETAIL` int(11) NOT NULL,
  `ID_KAMAR` int(11) NOT NULL,
  `ID_RESERVASI` int(11) NOT NULL,
  `TANGGAL_MASUK` date NOT NULL,
  `TANGGAL_SELESAI` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `detail_reservasi`
--

INSERT INTO `detail_reservasi` (`ID_DETAIL`, `ID_KAMAR`, `ID_RESERVASI`, `TANGGAL_MASUK`, `TANGGAL_SELESAI`) VALUES
(0, 1, 35, '2018-04-03', '2018-04-06'),
(1, 1, 1, '2018-04-11', '2018-04-12');

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_sesion`
--

CREATE TABLE `detail_sesion` (
  `ID_SESION` int(11) NOT NULL,
  `ID_JENIS_KAMAR` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `setting`
--

CREATE TABLE `setting` (
  `id_setting` int(11) NOT NULL,
  `about` text NOT NULL,
  `about_header` text NOT NULL,
  `about_footer` text NOT NULL,
  `phone` varchar(13) NOT NULL,
  `email` varchar(200) NOT NULL,
  `address` text NOT NULL,
  `fb` text NOT NULL,
  `tw` text NOT NULL,
  `yt` text NOT NULL,
  `ig` text NOT NULL,
  `google` text NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `setting`
--

INSERT INTO `setting` (`id_setting`, `about`, `about_header`, `about_footer`, `phone`, `email`, `address`, `fb`, `tw`, `yt`, `ig`, `google`, `status`) VALUES
(1, 'Rumah retret atau Rumah Pembinaan Carolus Borromeus (RPCB) Syantikara didirikan dengan maksud dan tujuan untuk melayani pengembangan spriritual awam, khususnya melalui pelayanan retret bagi kelompok remaja kaum muda (siswa atau mahasiswa), kelompok dewasa (guru – guru, dosen, suster/bruder/imam), dan lembaga pemerintahan, lembaga swadaya, lembaga gereja, serta masyarakat umum. Sejarah berdirinya Rumah Pembinaan Carolus Borromeus ( RPCB ) Syantikara tidak bisa dilepaskan dengan Yayasan Syantikara yang didirikan pada tanggal 21 November 1959.', 'Rumah retret atau Rumah Pembinaan Carolus Borromeus (RPCB) Syantikara didirikan dengan maksud dan tujuan untuk melayani pengembangan spriritual awam, khususnya melalui pelayanan retret bagi kelompok remaja kaum muda (siswa atau mahasiswa), kelompok dewasa (guru – guru, dosen, suster/bruder/imam), dan lembaga pemerintahan, lembaga swadaya, lembaga gereja, serta masyarakat umum. Sejarah berdirinya Rumah Pembinaan Carolus Borromeus ( RPCB ) Syantikara tidak bisa dilepaskan dengan Yayasan Syantikara yang didirikan pada tanggal 21 November 1959', 'Rumah retret atau Rumah Pembinaan Carolus Borromeus Syantikara didirikan dengan maksud dan tujuan untuk melayani pengembangan spriritual awam, khususnya melalui pelayanan retret bagi kelompok remaja kaum muda, kelompok dewasa, dan lembaga pemerintahan, lembaga swadaya, lembaga gereja, serta masyarakat umum.', '0215836988', 'info@syantikara.com', 'Jl. Colombo No.001, Caturtunggal, Kec. Depok, Kabupaten Sleman, Daerah Istimewa Yogyakarta', 'https://www.facebook.com/', 'https://twitter.com/', 'https://www.youtube.com/', 'https://www.instagram.com/', 'https://plus.google.com/about?hl=id', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_cabang`
--

CREATE TABLE `tbl_cabang` (
  `ID_CABANG` int(11) NOT NULL,
  `NAMA_KOTA` varchar(100) NOT NULL,
  `ALAMAT` varchar(100) NOT NULL,
  `NO_TELP_CABANG` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_cabang`
--

INSERT INTO `tbl_cabang` (`ID_CABANG`, `NAMA_KOTA`, `ALAMAT`, `NO_TELP_CABANG`) VALUES
(12, 'Bandung', 'Asia Fasifik', '8190786457'),
(13, 'Yogyakarta', 'Jalan Mangkubumi', '897867876');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_detail_fasilitas`
--

CREATE TABLE `tbl_detail_fasilitas` (
  `ID_DETAIL` int(11) NOT NULL,
  `ID_RESERVASI` int(11) NOT NULL,
  `ID_FASILITAS` int(11) NOT NULL,
  `JUMLAH` int(20) NOT NULL,
  `TOTAL` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_detail_pembayaran`
--

CREATE TABLE `tbl_detail_pembayaran` (
  `ID_PEMBAYARAN` int(11) NOT NULL,
  `ID_METODE` int(11) NOT NULL,
  `ID_RESERVASI` int(11) NOT NULL,
  `NO_NOTA` varchar(20) NOT NULL,
  `TANGGAL_PEMBAYARAN` date NOT NULL,
  `TOTAL_PEMBAYARAN_AWAL` float NOT NULL,
  `PAJAK` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_detail_pembayaran`
--

INSERT INTO `tbl_detail_pembayaran` (`ID_PEMBAYARAN`, `ID_METODE`, `ID_RESERVASI`, `NO_NOTA`, `TANGGAL_PEMBAYARAN`, `TOTAL_PEMBAYARAN_AWAL`, `PAJAK`) VALUES
(1, 1, 5, 'p09876', '2018-04-16', 30000000, 23000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_fasilitas`
--

CREATE TABLE `tbl_fasilitas` (
  `ID_FASILITAS` int(11) NOT NULL,
  `ID_CABANG` int(11) NOT NULL,
  `NAMA_FASILITAS` varchar(50) NOT NULL,
  `HARGA_FASILITAS` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_fasilitas`
--

INSERT INTO `tbl_fasilitas` (`ID_FASILITAS`, `ID_CABANG`, `NAMA_FASILITAS`, `HARGA_FASILITAS`) VALUES
(12, 12, 'Landry Manual', 8000000),
(15, 12, 'Laundry', 100000),
(16, 13, 'Laundry Manual', 100000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_jenis_kamar`
--

CREATE TABLE `tbl_jenis_kamar` (
  `ID_JENIS_KAMAR` int(11) NOT NULL,
  `JENIS_KAMAR` varchar(100) NOT NULL,
  `KAPASITAS` varchar(50) NOT NULL,
  `DESKRIPSI_KAMAR` varchar(200) NOT NULL,
  `STATUS_KAMAR` varchar(50) NOT NULL,
  `HARGA` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_jenis_kamar`
--

INSERT INTO `tbl_jenis_kamar` (`ID_JENIS_KAMAR`, `JENIS_KAMAR`, `KAPASITAS`, `DESKRIPSI_KAMAR`, `STATUS_KAMAR`, `HARGA`) VALUES
(2, 'Deluxe', '5 Orang', 'Sangat Memuasakan dan enak', 'Tidak Aktive', 6778980),
(4, 'Superior', '2 Orang', 'Sangat Memuaskan', 'Aktive', 800000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_kamar`
--

CREATE TABLE `tbl_kamar` (
  `ID_KAMAR` int(11) NOT NULL,
  `KODE_KAMAR` varchar(50) NOT NULL,
  `ID_CABANG` int(11) NOT NULL,
  `ID_TEMPAT_TIDUR` int(11) NOT NULL,
  `ID_JENIS_KAMAR` int(11) NOT NULL,
  `LANTAI_KAMAR` int(11) NOT NULL,
  `NOMOR_KAMAR` int(11) NOT NULL,
  `STATUS_KAMAR` varchar(50) NOT NULL,
  `DESKRIPSI_KAMAR` varchar(200) NOT NULL,
  `BEBAS_ROKO` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_kamar`
--

INSERT INTO `tbl_kamar` (`ID_KAMAR`, `KODE_KAMAR`, `ID_CABANG`, `ID_TEMPAT_TIDUR`, `ID_JENIS_KAMAR`, `LANTAI_KAMAR`, `NOMOR_KAMAR`, `STATUS_KAMAR`, `DESKRIPSI_KAMAR`, `BEBAS_ROKO`) VALUES
(1, 'P0987', 12, 1, 2, 4, 35, 'Aktive', 'Sangat Bagus', 'Bebas'),
(5, 'P089', 13, 2, 4, 3, 4, 'Aktive', 'sa', 'Bebas');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_metode_pembayaran`
--

CREATE TABLE `tbl_metode_pembayaran` (
  `ID_METODE` int(11) NOT NULL,
  `NAMA_PEMILIK` varchar(100) NOT NULL,
  `NO_KARTU` int(11) NOT NULL,
  `NAMA_BANK` varchar(100) NOT NULL,
  `CVV` int(4) NOT NULL,
  `BUKTI_PEMBAYARAN` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_metode_pembayaran`
--

INSERT INTO `tbl_metode_pembayaran` (`ID_METODE`, `NAMA_PEMILIK`, `NO_KARTU`, `NAMA_BANK`, `CVV`, `BUKTI_PEMBAYARAN`) VALUES
(1, 'Dewa', 1234567890, 'BRI', 123, '‰PNG\r\n\Z\r\n\0\0\0\r\nIHDR\0\0f\0\0\0ð\0\0\0B9b¾\0\0\0sRGB\0®Îé\0\0\0gAMA\0\0±üa\0\0\0	pHYs\0\0Ã\0\0ÃÇo¨d\0\0JÇIDATx^íÝ¿‹$');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_pegawai`
--

CREATE TABLE `tbl_pegawai` (
  `ID_PEGAWAI` int(11) NOT NULL,
  `ID_CABANG` int(11) NOT NULL,
  `NAMA_PEGAWAI` varchar(100) NOT NULL,
  `EMAIL_PEGAWAI` varchar(100) NOT NULL,
  `PASSWORD` varchar(100) NOT NULL,
  `ALAMAT` varchar(200) NOT NULL,
  `STATUS_PEGAWAI` varchar(100) NOT NULL,
  `JABATAN` varchar(100) NOT NULL,
  `ID_PERAN` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_pegawai`
--

INSERT INTO `tbl_pegawai` (`ID_PEGAWAI`, `ID_CABANG`, `NAMA_PEGAWAI`, `EMAIL_PEGAWAI`, `PASSWORD`, `ALAMAT`, `STATUS_PEGAWAI`, `JABATAN`, `ID_PERAN`) VALUES
(1, 13, 'ari dedi', 'dewa@gmail.com', 'e499eddcde8da81019b1f7d4b60f050653766037', 'Bandung', 'Tidak Aktive', 'Manajer', 4),
(2, 12, 'danu', 'danu@gmail.com', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'Yogyakarta', 'Aktive', 'Owner', 3),
(5, 13, 'Dedi', 'dedi@gmail.com', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'Yogyakarta', 'Tidak Aktive', 'Owner', 5);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_pelanggan`
--

CREATE TABLE `tbl_pelanggan` (
  `ID_PELANGGAN` int(11) NOT NULL,
  `NAMA_PELANGGAN` varchar(100) NOT NULL,
  `EMAIL_PELANGGAN` varchar(100) NOT NULL,
  `ALAMAT` varchar(200) NOT NULL,
  `NO_TELEPON_PELANGGAN` int(20) NOT NULL,
  `NO_IDENTITAS` int(20) NOT NULL,
  `PASSWORD` varchar(50) NOT NULL,
  `TANGGAL_RESERVASI` date NOT NULL,
  `ID_PERAN` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_pelanggan`
--

INSERT INTO `tbl_pelanggan` (`ID_PELANGGAN`, `NAMA_PELANGGAN`, `EMAIL_PELANGGAN`, `ALAMAT`, `NO_TELEPON_PELANGGAN`, `NO_IDENTITAS`, `PASSWORD`, `TANGGAL_RESERVASI`, `ID_PERAN`) VALUES
(8, 'Dewa', 'dewa@gmail.com', 'Yogyakarta', 897678655, 2147483647, '40bd001563085fc35165329ea1ff5c5ecbdbbeef', '2018-04-20', 4),
(9, 'Dedi', 'dewa@gmail.com', 'Yogyakarta', 897678655, 2147483647, '51eac6b471a284d3341d8c0c63d0f1a286262a18', '2018-04-10', 4),
(11, 'Dewa', 'dewa@gmail.com', 'Yogyakarta', 2147483647, 2147483647, '8cb2237d0679ca88db6464eac60da96345513964', '2018-04-09', 6),
(14, 'Dewa', 'dewa@gmail.com', 'Yogyakarta', 2147483647, 2147483647, '321', '2018-04-12', 6),
(15, 'Dedi', 'ardian@gmail.com', 'Yogyakarta', 8976767, 8978453, '40bd001563085fc35165329ea1ff5c5ecbdbbeef', '2018-04-12', 4),
(16, 'bagasrahadian', 'Bagas123@gmail.com', 'Bandung', 895674567, 2147483647, '40bd001563085fc35165329ea1ff5c5ecbdbbeef', '2018-04-12', 6);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_peran`
--

CREATE TABLE `tbl_peran` (
  `ID_PERAN` int(11) NOT NULL,
  `NAMA_PERAN` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_peran`
--

INSERT INTO `tbl_peran` (`ID_PERAN`, `NAMA_PERAN`) VALUES
(3, 'Admin'),
(4, 'Owner'),
(5, 'Manajer'),
(6, 'PIC');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_permintaan_khusus`
--

CREATE TABLE `tbl_permintaan_khusus` (
  `ID_PERMINTAAN` int(11) NOT NULL,
  `ID_RESERVASI` int(11) DEFAULT NULL,
  `NAMA_PERMINTAAN` varchar(100) NOT NULL,
  `JUMLAH_PERMINTAAN` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_reservasi`
--

CREATE TABLE `tbl_reservasi` (
  `ID_RESERVASI` int(11) NOT NULL,
  `ID_BOOKING` varchar(50) NOT NULL,
  `ID_PEGAWAI` int(11) DEFAULT NULL,
  `ID_PELANGGAN` int(11) DEFAULT NULL,
  `ID_CABANG` int(11) NOT NULL,
  `TANGGAL_RESERVASI` date NOT NULL,
  `JUMLAH_DEWASA` int(11) NOT NULL,
  `JUMLAH_ANAK` int(11) NOT NULL,
  `TANGGAL_BAYAR` date NOT NULL,
  `TOTAL_BIAYA` float NOT NULL,
  `STATUS_RESERVASI` varchar(50) NOT NULL,
  `NAMA_INSTITUSI` varchar(100) NOT NULL,
  `DEPOSIT` float NOT NULL,
  `UANG_JAMINAN` float NOT NULL,
  `JENIS_TAMU` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_reservasi`
--

INSERT INTO `tbl_reservasi` (`ID_RESERVASI`, `ID_BOOKING`, `ID_PEGAWAI`, `ID_PELANGGAN`, `ID_CABANG`, `TANGGAL_RESERVASI`, `JUMLAH_DEWASA`, `JUMLAH_ANAK`, `TANGGAL_BAYAR`, `TOTAL_BIAYA`, `STATUS_RESERVASI`, `NAMA_INSTITUSI`, `DEPOSIT`, `UANG_JAMINAN`, `JENIS_TAMU`) VALUES
(1, 'P0987', 1, 8, 13, '2018-04-17', 3, 2, '2018-04-23', 20000000, 'aktive', 'Universitas Atmajaya', 5000000, 10000, 'personal'),
(5, 'p0987', 1, 8, 12, '2018-04-19', 2, 1, '2018-04-03', 100000, 'Aktive', 'uajy', 500000, 500000, 'Personal'),
(10, 'P0987', NULL, 9, 13, '2018-04-16', 9, 3, '2018-04-25', 100000, 'Aktive', '', 900000, 90000, 'Personal'),
(11, 'P0987', NULL, 9, 13, '2018-04-16', 9, 3, '2018-04-25', 100000, 'Aktive', '', 900000, 90000, 'Personal'),
(12, 'P09876', NULL, 9, 12, '2018-04-17', 5, 9, '2018-04-17', 9000000000, 'Aktive', '', 900000000, 30000000, 'Personal'),
(13, 'p9876', NULL, 8, 12, '2018-04-19', 4, 3, '2018-04-20', 600000000, 'Aktive', '', 4000000, 300000, 'Personal'),
(14, 'p9876', NULL, 8, 12, '2018-04-19', 4, 3, '2018-04-20', 600000000, 'Aktive', '', 4000000, 300000, 'Personal'),
(15, 'p9876', NULL, 11, 12, '2018-04-19', 5, 3, '2018-04-19', 30000000000, 'Aktive', '', 30000000, 3000000, 'Personal'),
(16, 'p9876', NULL, 11, 12, '2018-04-19', 5, 3, '2018-04-19', 30000000000, 'Aktive', '', 30000000, 3000000, 'Personal'),
(17, 'p9876', NULL, 11, 12, '2018-04-19', 5, 3, '2018-04-19', 30000000000, 'Aktive', '', 30000000, 3000000, 'Personal'),
(18, 'p988', NULL, 9, 13, '2018-04-20', 8, 5, '2018-04-16', 908991000, 'Aktive', '', 890891000, 890891000, 'Personal'),
(19, 'p988', NULL, 9, 13, '2018-04-20', 8, 5, '2018-04-16', 908991000, 'Aktive', '', 890891000, 890891000, 'Personal'),
(20, 'p0987', NULL, 9, 12, '2018-04-14', 5, 4, '2018-04-25', 10000000, 'Aktive', '', 1000000, 1000000, 'Personal'),
(21, 'p0987', NULL, 9, 12, '2018-04-14', 5, 4, '2018-04-25', 10000000, 'Aktive', '', 1000000, 1000000, 'Personal'),
(22, 'p0987', NULL, 9, 12, '2018-04-14', 5, 4, '2018-04-25', 10000000, 'Aktive', '', 1000000, 1000000, 'Personal'),
(23, 'p0987', NULL, 9, 12, '2018-04-12', 6, 3, '2018-04-09', 4575680000, 'Aktive', '', 9000000, 1000000, 'Personal'),
(24, 'p988', NULL, 9, 13, '2018-04-18', 6, 3, '2018-04-25', 678798000000, 'Aktive', '', 30000, 300000, 'Personal'),
(25, 'p0987', NULL, 8, 13, '2018-04-11', 78, 7, '2018-04-18', 789789, 'Aktive', '', 879789, 789789, 'Personal'),
(26, 'P-00090', NULL, 8, 12, '2018-04-11', 12, 12, '2018-04-12', 8000000, 'Tidak Aktive', '', 300000, 80000000, 'Personal'),
(27, 'p0987', NULL, 9, 12, '2018-04-18', 4, 2, '2018-04-19', 3242430000, 'Aktive', '', 45546500, 345435000, 'Personal'),
(28, 'p0987', NULL, 9, 12, '2018-04-18', 4, 2, '2018-04-19', 3242430000, 'Aktive', '', 45546500, 345435000, 'Personal'),
(29, 'p0987', NULL, 9, 12, '2018-04-18', 4, 2, '2018-04-19', 3242430000, 'Aktive', '', 45546500, 345435000, 'Personal'),
(30, 'p0987', NULL, 9, 12, '2018-04-18', 4, 2, '2018-04-19', 3242430000, 'Aktive', '', 45546500, 345435000, 'Personal'),
(31, 'p988', NULL, 9, 12, '2018-04-19', 5, 3, '2018-04-18', 90000000000000, 'Aktive', '', 2000000000, 10000000, 'Personal'),
(32, 'p988', NULL, 9, 12, '2018-04-19', 5, 3, '2018-04-18', 90000000000000, 'Aktive', '', 2000000000, 10000000, 'Personal'),
(33, 'p988', NULL, 9, 12, '2018-04-19', 5, 3, '2018-04-18', 90000000000000, 'Aktive', '', 2000000000, 10000000, 'Personal'),
(34, 'p988', NULL, 9, 12, '2018-04-19', 5, 3, '2018-04-18', 90000000000000, 'Aktive', '', 2000000000, 10000000, 'Personal'),
(35, '123', NULL, 8, 12, '2018-04-02', 1, 2, '2018-04-06', 12345, 'Aktive', '', 21, 123, 'Personal');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_sesion`
--

CREATE TABLE `tbl_sesion` (
  `ID_SESION` int(11) NOT NULL,
  `ID_CABANG` int(11) NOT NULL,
  `NAMA_SESION` varchar(100) NOT NULL,
  `TANGGAL_MULAI` date NOT NULL,
  `TANGGAL_SELESAI` date NOT NULL,
  `HARGA_SESION` float NOT NULL,
  `STATUS_SESION` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_sesion`
--

INSERT INTO `tbl_sesion` (`ID_SESION`, `ID_CABANG`, `NAMA_SESION`, `TANGGAL_MULAI`, `TANGGAL_SELESAI`, `HARGA_SESION`, `STATUS_SESION`) VALUES
(1, 13, 'Promo', '2018-03-14', '2018-03-19', 30000000, 'Aktive'),
(2, 13, 'sadsd', '2018-03-14', '2018-03-21', 546456, 'rety'),
(3, 13, '213', '2018-04-05', '2018-04-13', 123, 'Aktive'),
(4, 13, '123', '2018-04-09', '2018-04-18', 123, 'Aktive'),
(5, 12, 'High Sesion', '2018-04-19', '2018-04-16', 6000000, 'Aktive'),
(6, 13, 'fdhgfh', '2018-04-12', '2018-04-12', 546547000, 'Aktive');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_tempat_tidur`
--

CREATE TABLE `tbl_tempat_tidur` (
  `ID_TEMPAT_TIDUR` int(11) NOT NULL,
  `JENIS_TEMPAT_TIDUR` varchar(50) NOT NULL,
  `HARGA` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_tempat_tidur`
--

INSERT INTO `tbl_tempat_tidur` (`ID_TEMPAT_TIDUR`, `JENIS_TEMPAT_TIDUR`, `HARGA`) VALUES
(1, 'Twin Super', 300000),
(2, 'Twin', 45436500);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `detail_reservasi`
--
ALTER TABLE `detail_reservasi`
  ADD PRIMARY KEY (`ID_DETAIL`),
  ADD KEY `ID_RESERVASI` (`ID_RESERVASI`),
  ADD KEY `INDEX` (`ID_KAMAR`) USING BTREE;

--
-- Indeks untuk tabel `detail_sesion`
--
ALTER TABLE `detail_sesion`
  ADD KEY `ID_JENIS_KAMAR` (`ID_JENIS_KAMAR`),
  ADD KEY `ID_SESION` (`ID_SESION`);

--
-- Indeks untuk tabel `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id_setting`);

--
-- Indeks untuk tabel `tbl_cabang`
--
ALTER TABLE `tbl_cabang`
  ADD PRIMARY KEY (`ID_CABANG`);

--
-- Indeks untuk tabel `tbl_detail_fasilitas`
--
ALTER TABLE `tbl_detail_fasilitas`
  ADD PRIMARY KEY (`ID_DETAIL`),
  ADD KEY `ID_RESERVASI` (`ID_RESERVASI`),
  ADD KEY `ID_FASILITAS` (`ID_FASILITAS`) USING BTREE;

--
-- Indeks untuk tabel `tbl_detail_pembayaran`
--
ALTER TABLE `tbl_detail_pembayaran`
  ADD PRIMARY KEY (`ID_PEMBAYARAN`),
  ADD KEY `ID_RESERVASI` (`ID_RESERVASI`),
  ADD KEY `ID_METODE` (`ID_METODE`);

--
-- Indeks untuk tabel `tbl_fasilitas`
--
ALTER TABLE `tbl_fasilitas`
  ADD PRIMARY KEY (`ID_FASILITAS`),
  ADD KEY `ID_CABANG` (`ID_CABANG`);

--
-- Indeks untuk tabel `tbl_jenis_kamar`
--
ALTER TABLE `tbl_jenis_kamar`
  ADD PRIMARY KEY (`ID_JENIS_KAMAR`);

--
-- Indeks untuk tabel `tbl_kamar`
--
ALTER TABLE `tbl_kamar`
  ADD PRIMARY KEY (`ID_KAMAR`),
  ADD KEY `ID_CABANG` (`ID_CABANG`),
  ADD KEY `ID_JENIS_KAMAR` (`ID_JENIS_KAMAR`),
  ADD KEY `ID_TEMPAT_TIDUR` (`ID_TEMPAT_TIDUR`);

--
-- Indeks untuk tabel `tbl_metode_pembayaran`
--
ALTER TABLE `tbl_metode_pembayaran`
  ADD PRIMARY KEY (`ID_METODE`);

--
-- Indeks untuk tabel `tbl_pegawai`
--
ALTER TABLE `tbl_pegawai`
  ADD PRIMARY KEY (`ID_PEGAWAI`),
  ADD KEY `ID_PERAN` (`ID_PERAN`),
  ADD KEY `ID_CABANG` (`ID_CABANG`);

--
-- Indeks untuk tabel `tbl_pelanggan`
--
ALTER TABLE `tbl_pelanggan`
  ADD PRIMARY KEY (`ID_PELANGGAN`),
  ADD KEY `ID_PERAN` (`ID_PERAN`);

--
-- Indeks untuk tabel `tbl_peran`
--
ALTER TABLE `tbl_peran`
  ADD PRIMARY KEY (`ID_PERAN`);

--
-- Indeks untuk tabel `tbl_permintaan_khusus`
--
ALTER TABLE `tbl_permintaan_khusus`
  ADD PRIMARY KEY (`ID_PERMINTAAN`),
  ADD KEY `ID_RESERVASI` (`ID_RESERVASI`);

--
-- Indeks untuk tabel `tbl_reservasi`
--
ALTER TABLE `tbl_reservasi`
  ADD PRIMARY KEY (`ID_RESERVASI`),
  ADD KEY `ID_CABANG` (`ID_CABANG`),
  ADD KEY `ID_PEGAWAI` (`ID_PEGAWAI`),
  ADD KEY `ID_PELANGGAN` (`ID_PELANGGAN`);

--
-- Indeks untuk tabel `tbl_sesion`
--
ALTER TABLE `tbl_sesion`
  ADD PRIMARY KEY (`ID_SESION`),
  ADD KEY `ID_CABANG` (`ID_CABANG`);

--
-- Indeks untuk tabel `tbl_tempat_tidur`
--
ALTER TABLE `tbl_tempat_tidur`
  ADD PRIMARY KEY (`ID_TEMPAT_TIDUR`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `setting`
--
ALTER TABLE `setting`
  MODIFY `id_setting` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tbl_cabang`
--
ALTER TABLE `tbl_cabang`
  MODIFY `ID_CABANG` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT untuk tabel `tbl_detail_fasilitas`
--
ALTER TABLE `tbl_detail_fasilitas`
  MODIFY `ID_DETAIL` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tbl_detail_pembayaran`
--
ALTER TABLE `tbl_detail_pembayaran`
  MODIFY `ID_PEMBAYARAN` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tbl_fasilitas`
--
ALTER TABLE `tbl_fasilitas`
  MODIFY `ID_FASILITAS` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT untuk tabel `tbl_jenis_kamar`
--
ALTER TABLE `tbl_jenis_kamar`
  MODIFY `ID_JENIS_KAMAR` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `tbl_kamar`
--
ALTER TABLE `tbl_kamar`
  MODIFY `ID_KAMAR` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `tbl_metode_pembayaran`
--
ALTER TABLE `tbl_metode_pembayaran`
  MODIFY `ID_METODE` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tbl_pegawai`
--
ALTER TABLE `tbl_pegawai`
  MODIFY `ID_PEGAWAI` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `tbl_pelanggan`
--
ALTER TABLE `tbl_pelanggan`
  MODIFY `ID_PELANGGAN` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT untuk tabel `tbl_peran`
--
ALTER TABLE `tbl_peran`
  MODIFY `ID_PERAN` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `tbl_permintaan_khusus`
--
ALTER TABLE `tbl_permintaan_khusus`
  MODIFY `ID_PERMINTAAN` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tbl_reservasi`
--
ALTER TABLE `tbl_reservasi`
  MODIFY `ID_RESERVASI` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT untuk tabel `tbl_sesion`
--
ALTER TABLE `tbl_sesion`
  MODIFY `ID_SESION` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `tbl_tempat_tidur`
--
ALTER TABLE `tbl_tempat_tidur`
  MODIFY `ID_TEMPAT_TIDUR` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `detail_reservasi`
--
ALTER TABLE `detail_reservasi`
  ADD CONSTRAINT `detail_reservasi_ibfk_1` FOREIGN KEY (`ID_RESERVASI`) REFERENCES `tbl_reservasi` (`ID_RESERVASI`),
  ADD CONSTRAINT `detail_reservasi_ibfk_2` FOREIGN KEY (`ID_KAMAR`) REFERENCES `tbl_kamar` (`ID_KAMAR`);

--
-- Ketidakleluasaan untuk tabel `detail_sesion`
--
ALTER TABLE `detail_sesion`
  ADD CONSTRAINT `detail_sesion_ibfk_1` FOREIGN KEY (`ID_JENIS_KAMAR`) REFERENCES `tbl_jenis_kamar` (`ID_JENIS_KAMAR`),
  ADD CONSTRAINT `detail_sesion_ibfk_2` FOREIGN KEY (`ID_SESION`) REFERENCES `tbl_sesion` (`ID_SESION`);

--
-- Ketidakleluasaan untuk tabel `tbl_detail_fasilitas`
--
ALTER TABLE `tbl_detail_fasilitas`
  ADD CONSTRAINT `tbl_detail_fasilitas_ibfk_1` FOREIGN KEY (`ID_RESERVASI`) REFERENCES `tbl_reservasi` (`ID_RESERVASI`),
  ADD CONSTRAINT `tbl_detail_fasilitas_ibfk_2` FOREIGN KEY (`ID_FASILITAS`) REFERENCES `tbl_fasilitas` (`ID_FASILITAS`);

--
-- Ketidakleluasaan untuk tabel `tbl_detail_pembayaran`
--
ALTER TABLE `tbl_detail_pembayaran`
  ADD CONSTRAINT `tbl_detail_pembayaran_ibfk_1` FOREIGN KEY (`ID_RESERVASI`) REFERENCES `tbl_reservasi` (`ID_RESERVASI`),
  ADD CONSTRAINT `tbl_detail_pembayaran_ibfk_2` FOREIGN KEY (`ID_METODE`) REFERENCES `tbl_metode_pembayaran` (`ID_METODE`);

--
-- Ketidakleluasaan untuk tabel `tbl_fasilitas`
--
ALTER TABLE `tbl_fasilitas`
  ADD CONSTRAINT `tbl_fasilitas_ibfk_1` FOREIGN KEY (`ID_CABANG`) REFERENCES `tbl_cabang` (`ID_CABANG`);

--
-- Ketidakleluasaan untuk tabel `tbl_kamar`
--
ALTER TABLE `tbl_kamar`
  ADD CONSTRAINT `tbl_kamar_ibfk_1` FOREIGN KEY (`ID_CABANG`) REFERENCES `tbl_cabang` (`ID_CABANG`),
  ADD CONSTRAINT `tbl_kamar_ibfk_2` FOREIGN KEY (`ID_JENIS_KAMAR`) REFERENCES `tbl_jenis_kamar` (`ID_JENIS_KAMAR`),
  ADD CONSTRAINT `tbl_kamar_ibfk_3` FOREIGN KEY (`ID_TEMPAT_TIDUR`) REFERENCES `tbl_tempat_tidur` (`ID_TEMPAT_TIDUR`);

--
-- Ketidakleluasaan untuk tabel `tbl_pegawai`
--
ALTER TABLE `tbl_pegawai`
  ADD CONSTRAINT `tbl_pegawai_ibfk_1` FOREIGN KEY (`ID_PERAN`) REFERENCES `tbl_peran` (`ID_PERAN`),
  ADD CONSTRAINT `tbl_pegawai_ibfk_2` FOREIGN KEY (`ID_CABANG`) REFERENCES `tbl_cabang` (`ID_CABANG`);

--
-- Ketidakleluasaan untuk tabel `tbl_pelanggan`
--
ALTER TABLE `tbl_pelanggan`
  ADD CONSTRAINT `tbl_pelanggan_ibfk_1` FOREIGN KEY (`ID_PERAN`) REFERENCES `tbl_peran` (`ID_PERAN`);

--
-- Ketidakleluasaan untuk tabel `tbl_permintaan_khusus`
--
ALTER TABLE `tbl_permintaan_khusus`
  ADD CONSTRAINT `tbl_permintaan_khusus_ibfk_1` FOREIGN KEY (`ID_RESERVASI`) REFERENCES `tbl_reservasi` (`ID_RESERVASI`);

--
-- Ketidakleluasaan untuk tabel `tbl_reservasi`
--
ALTER TABLE `tbl_reservasi`
  ADD CONSTRAINT `tbl_reservasi_ibfk_1` FOREIGN KEY (`ID_CABANG`) REFERENCES `tbl_cabang` (`ID_CABANG`),
  ADD CONSTRAINT `tbl_reservasi_ibfk_2` FOREIGN KEY (`ID_PEGAWAI`) REFERENCES `tbl_pegawai` (`ID_PEGAWAI`),
  ADD CONSTRAINT `tbl_reservasi_ibfk_3` FOREIGN KEY (`ID_PELANGGAN`) REFERENCES `tbl_pelanggan` (`ID_PELANGGAN`);

--
-- Ketidakleluasaan untuk tabel `tbl_sesion`
--
ALTER TABLE `tbl_sesion`
  ADD CONSTRAINT `tbl_sesion_ibfk_1` FOREIGN KEY (`ID_CABANG`) REFERENCES `tbl_cabang` (`ID_CABANG`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
